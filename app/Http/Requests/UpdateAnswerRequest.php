<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateAnswerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        return [
            'answer'  => 'required',
        ];
    }
    public function messages()
    {
        $messages = [ 
            'answer.required' => 'Câu trả lời là trường bắt buộc', 

        ];
        return $messages;
    }
    public function getData()
    {
        $data = $this->only([ 'answer','correct']);
        return $data;
    }
}
