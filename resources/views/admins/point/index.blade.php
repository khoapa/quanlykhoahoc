@extends('admin')
@section('content')

<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-car icon-gradient bg-mean-fruit">
                    </i>
                </div>
                <div>
                    Danh sách bảng điểm học sinh
                </div>
            </div>
        </div>
    </div>
    @if(Session::has('message'))
    <div class="alert alert-success">
        <span class="glyphicon glyphicon-ok"></span>
        {!! session('message') !!}
        <button type="button" class="close" data-dismiss="alert" aria-label="close">
            <span aria-hidden="true">&times;</span>
        </button>

    </div>
    @endif
    <div class="main-card card">
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">

                <table class="table table-striped ">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>Tên học sinh</th>
                            <th>Khóa học</th>
                            <th>Điểm trắc nghiệm</th>
                            <th>Điểm tự luận</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($userCourses as $userCourse)
                        <tr>
                            <td>{!! Helper::indexPaginate($userCourses,$loop) !!}</td>
                            <td>{{$userCourse->student->name}}</td>
                            <td>{{$userCourse->course->name}}</td>
                            <td>{{$userCourse->count_point_choose}}</td>
                            <td>{{$userCourse->total_point - $userCourse->count_point_choose}}</td>
                            <td>
                                <form method="POST" action="{{route('answer.answer.destroy',$userCourse->id)}}"
                                    accept-charset="UTF-8">
                                    <input name="_method" value="DELETE" type="hidden">
                                    {{ csrf_field() }}
                                    <div class=" w-100">
                                        <button type="submit"
                                            class="pull-right mb-2 mr-2 btn-icon btn-icon-only btn-shadow btn-dashed btn btn-outline-warning"
                                            title="Xóa" onclick="return confirm('Bạn muốn xóa ?')">
                                            <span class="pe-7s-trash" aria-hidden="true"></span>
                                        </button>
                                        <a href="{{route('answer.answer.edit',$userCourse->id)}}"
                                            class="pull-right mb-2 mr-2 btn-icon btn-icon-only btn-shadow btn-dashed btn btn-outline-primary"
                                            title="Sửa">
                                            <span class="pe-7s-tools" aria-hidden="true"></span>
                                        </a>
                                    </div>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
        </div>

        <div class="panel-footer">
            <div class="col-12">
                <div class="pagination pull-right">
                    {{ $userCourses->links()}}
                </div>
            </div>
        </div>

    </div>
</div>
@endsection