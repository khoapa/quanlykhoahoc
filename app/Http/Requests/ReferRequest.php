<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReferRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'  =>  'required|max:1024',
            'icon'  =>  'required|mimes:jpeg,png,jpg,gif,svg|max:5012',
            'link'  =>  'required|max:255',
        ];
    }
    public function messages()
    {
        return [
            'required'            =>  __(':attribute không được để trống.'),
            'min'            =>  __(':attribute tối thiểu 4 ký tự.'),
            'mimes'            =>  __('đây không phải file :attribute'),
            'max'            =>  __(':attribute quá dung lượng quy định.'),
        ];
    }
    public function attributes()
    {
        return [
            'title'     =>  __('Tiêu đề'),
            'icon'     =>  __('Icon'),
            'link'     =>  __('Link'),
        ];
    }
    public function getData()
    {
        $data = $this->only(['title','icon','link']);
        return $data;
    }
}
