<?php

use Illuminate\Database\Seeder;
use App\Models\Role;
use App\Models\Permission;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
class UserPermissionSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      // Create role for user
     $roleAdmin = Role::where('name','Admin')->first();

        Model::unguard();
        DB::table('permissions')->insert([
            //School
            [
                'name' => 'create_school', 
                'description' => 'Thêm', 
                'guard_name' => 'web',
                'subject' => 'Trường học'
            ],
            [
                'name' => 'edit_school', 
                'description' => 'Sửa', 
                'guard_name' => 'web',
                'subject' => 'Trường học'
            ],
            [
                'name' => 'view_school', 
                'description' => 'Xem', 
                'guard_name' => 'web',
                'subject' => 'Trường học'
            ],
            [
                'name' => 'delete_school', 
                'description' => 'Xóa',
                'guard_name' => 'web',
                'subject' => 'Trường học'
            ],
            //Grade
            [
                'name' => 'create_grade', 
                'description' => 'Thêm', 
                'guard_name' => 'web',
                'subject' => 'Khối học'
            ],
            [
                'name' => 'edit_grade', 
                'description' => 'Sửa', 
                'guard_name' => 'web',
                'subject' => 'Khối học'
            ],
            [
                'name' => 'view_grade', 
                'description' => 'Xem', 
                'guard_name' => 'web',
                'subject' => 'Khối học'
            ],
            [
                'name' => 'delete_grade', 
                'description' => 'Xóa', 
                'guard_name' => 'web',
                'subject' => 'Khối học'
            ],
            //Class
            [
                'name' => 'create_class', 
                'description' => 'Thêm', 
                'guard_name' => 'web',
                'subject' => 'Lớp học'
            ],
            [
                'name' => 'edit_class', 
                'description' => 'Sửa', 
                'guard_name' => 'web',
                'subject' => 'Lớp học'
            ],
            [
                'name' => 'view_class', 
                'description' => 'Xem', 
                'guard_name' => 'web',
                'subject' => 'Lớp học'
            ],
            [
                'name' => 'delete_class', 
                'description' => 'Xóa',
                'guard_name' => 'web', 
                'subject' => 'Lớp học'
            ],
            //Teacher
            [
                'name' => 'create_teacher', 
                'description' => 'Thêm', 
                'guard_name' => 'web',
                'subject' => 'Giáo viên'
            ],
            [
                'name' => 'edit_teacher', 
                'description' => 'Sửa',
                'guard_name' => 'web', 
                'subject' => 'Giáo viên'
            ],
            [
                'name' => 'view_teacher', 
                'description' => 'Xem',
                'guard_name' => 'web', 
                'subject' => 'Giáo viên'
            ],
            [
                'name' => 'delete_teacher', 
                'description' => 'Xóa', 
                'guard_name' => 'web',
                'subject' => 'Giáo viên'
            ],
             //Student
            [
                'name' => 'create_student', 
                'description' => 'Thêm', 
                'guard_name' => 'web',
                'subject' => 'Học sinh'
            ],
            [
                'name' => 'edit_student', 
                'description' => 'Sửa',
                'guard_name' => 'web', 
                'subject' => 'Học sinh'
            ],
            [
                'name' => 'view_student', 
                'description' => 'Xem',
                'guard_name' => 'web', 
                'subject' => 'Học sinh'
            ],
            [
                'name' => 'delete_student', 
                'description' => 'Xóa', 
                'guard_name' => 'web',
                'subject' => 'Học sinh'
            ],
            //Category course
            [
                'name' => 'create_category_course', 
                'description' => 'Thêm', 
                'guard_name' => 'web',
                'subject' => 'Danh mục khóa học'
            ],
            [
                'name' => 'edit_category_course', 
                'description' => 'Sửa',
                'guard_name' => 'web', 
                'subject' => 'Danh mục khóa học'
            ],
            [
                'name' => 'view_category_course', 
                'description' => 'Xem',
                'guard_name' => 'web', 
                'subject' => 'Danh mục khóa học'
            ],
            [
                'name' => 'delete_category_course', 
                'description' => 'Xóa', 
                'guard_name' => 'web',
                'subject' => 'Danh mục khóa học'
            ],
            //Mark
            [
                'name' => 'create_mark', 
                'description' => 'Chấm điểm', 
                'guard_name' => 'web',
                'subject' => 'Chấm điểm'
            ],
            [
                'name' => 'delete_mark', 
                'description' => 'Xóa', 
                'guard_name' => 'web',
                'subject' => 'Chấm điểm'
            ],
            //Course
            [
                'name' => 'create_course', 
                'description' => 'Thêm', 
                'guard_name' => 'web',
                'subject' => 'Khóa học'
            ],
            [
                'name' => 'edit_course', 
                'description' => 'Sửa',
                'guard_name' => 'web', 
                'subject' => 'Khóa học'
            ],
            [
                'name' => 'view_course', 
                'description' => 'Xem',
                'guard_name' => 'web', 
                'subject' => 'Khóa học'
            ],
            [
                'name' => 'delete_course', 
                'description' => 'Xóa', 
                'guard_name' => 'web',
                'subject' => 'Khóa học'
            ],
            //Category new
            [
                'name' => 'create_category_new', 
                'description' => 'Thêm', 
                'guard_name' => 'web',
                'subject' => 'Danh mục tin tức'
            ],
            [
                'name' => 'edit_category_new', 
                'description' => 'Sửa',
                'guard_name' => 'web', 
                'subject' => 'Danh mục tin tức'
            ],
            [
                'name' => 'view_category_new', 
                'description' => 'Xem',
                'guard_name' => 'web', 
                'subject' => 'Danh mục tin tức'
            ],
            [
                'name' => 'delete_category_new', 
                'description' => 'Xóa', 
                'guard_name' => 'web',
                'subject' => 'Danh mục tin tức'
            ], 
            //new
            [
                'name' => 'create_new', 
                'description' => 'Thêm', 
                'guard_name' => 'web',
                'subject' => 'Tin tức'
            ],
            [
                'name' => 'edit_new', 
                'description' => 'Sửa',
                'guard_name' => 'web', 
                'subject' => 'Tin tức'
            ],
            [
                'name' => 'view_new', 
                'description' => 'Xem',
                'guard_name' => 'web', 
                'subject' => 'Tin tức'
            ],
            [
                'name' => 'delete_new', 
                'description' => 'Xóa', 
                'guard_name' => 'web',
                'subject' => 'Tin tức'
            ],
            //Category map
            [
                'name' => 'create_category_map', 
                'description' => 'Thêm', 
                'guard_name' => 'web',
                'subject' => 'Danh mục map'
            ],
            [
                'name' => 'edit_category_map', 
                'description' => 'Sửa',
                'guard_name' => 'web', 
                'subject' => 'Danh mục map'
            ],
            [
                'name' => 'view_category_map', 
                'description' => 'Xem',
                'guard_name' => 'web', 
                'subject' => 'Danh mục map'
            ],
            [
                'name' => 'delete_category_map', 
                'description' => 'Xóa', 
                'guard_name' => 'web',
                'subject' => 'Danh mục map'
            ], 
            //new
            [
                'name' => 'create_map', 
                'description' => 'Thêm', 
                'guard_name' => 'web',
                'subject' => 'Map'
            ],
            [
                'name' => 'edit_map', 
                'description' => 'Sửa',
                'guard_name' => 'web', 
                'subject' => 'Map'
            ],
            [
                'name' => 'view_map', 
                'description' => 'Xem',
                'guard_name' => 'web', 
                'subject' => 'Map'
            ],
            [
                'name' => 'delete_map', 
                'description' => 'Xóa', 
                'guard_name' => 'web',
                'subject' => 'Map'
            ], 
            //Permission           
            [
                'name' => 'view_permission', 
                'description' => 'Xem',
                'guard_name' => 'web', 
                'subject' => 'Phân quyền'
            ],
            [
                'name' => 'edit_permission', 
                'description' => 'Sửa', 
                'guard_name' => 'web',
                'subject' => 'Phân quyền'
            ],            
              
            ]);
    $permissions = Permission::all();
    foreach($permissions as $permission){
        $roleAdmin->givePermissionTo($permission->name);
    }        
    }
}