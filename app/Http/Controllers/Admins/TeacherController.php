<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use App\Models\Classes;
use App\Models\School;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Services\UploadService;
use App\Helpers\Helper;
use App\Http\Requests\StudentUpdateRequest;
use App\Http\Requests\TeacherUpdateRequest;
use App\Enums\UserTypeEnum;
use App\Http\Requests\TeacherCreateRequest;
use App\Models\Role;

class TeacherController extends Controller
{
    public function index(Request $request)
    {
        $name = $request->get('search');
        $schoolID = $request->get('school_id');
        $classID = $request->get('class_id');
        $query=User::where('user_type',UserTypeEnum::TEACHER)->orderBy('id', 'DESC');
        $classes = null;
        if (!is_null($schoolID)) {
            $query = $query->where('school_id',$schoolID);
            $classes = Classes::where('school_id',$schoolID)->get();   
        }
        if (!is_null($classID)) {
            $query = $query->where('class_id',$classID);
        }
        if (!is_null($name)) {
            $query = $query->where('name', 'LIKE', '%' . $name . '%');
        }
        $teacher = $query->paginate(config('settings.countPaginate'));
        $schools = School::get();
        return view('admins.teacher.index', compact('teacher','schools','classes'));
    }
    public function create()
    {
        $schools = School::get();
        $classes = Classes::get();
        $roles = Role::where('name','!=','Admin')->get();
        return view('admins.teacher.create', compact('schools', 'classes','roles'));
    }
    public function store(TeacherCreateRequest $request)
    {
        $data = $request->getData();
        $data['birthday'] = Helper::formatSqlDate($data['birthday']);
        $data['password'] = Hash::make(config('settings.password'));
        $data['user_type'] = UserTypeEnum::TEACHER;
        $data['email'] = str_replace(' ', '', $data['email']);
        $data_image = config('settings.image');
        if ($request->hasFile('avatar')) {
            $file = $request->file('avatar');
            $data_image = UploadService::uploadImage('teacher', $file);
        }
        $data['avatar'] = $data_image;
        try {
            $userCreate = User::create($data);
            $roleId = $data['role_id'];
            if(!is_null($roleId)){
                $userCreate->syncRoles([$roleId]);
            }
            return redirect()->route('teacher.index')->with('message',  'Thêm mới giáo viên thành công');
        } catch (\Exception $e) {
            return redirect()->back()->with('message', 'Thêm mới giáo viên không thành công');
        }
    }
    public function edit($id)
    {
        $teacher = User::findOrFail($id);
        $schools = School::get();
        $classes = Classes::get();
        $roles = Role::where('name','!=','Admin')->get();
        return view('admins.teacher.update', compact('teacher', 'schools', 'classes','roles'));
    }
    public function update(TeacherUpdateRequest $request, $id)
    {

        $teacher = User::findOrFail($id);
        $data = $request->getData();
        $data['birthday'] = Helper::formatSqlDate($data['birthday']);
        $data['user_type'] = UserTypeEnum::TEACHER;
        $data['email'] = str_replace(' ', '', $data['email']);
        if ($request->has('avatar')) {
            $file = $request->file('avatar');
            $data_image = UploadService::uploadImage('teacher', $file);
            $data['avatar'] = $data_image;
        } else {
            $data['avatar'] = $teacher->avatar;
        }
        $roleId = $data['role_id'];
        $roleOld = $teacher->role()->first();
        if(!is_null($roleId)){
            $teacher->syncRoles([$roleId]);
        }else{
            if($roleOld){
                $teacher->removeRole($roleOld->role_id);   
            }
        }
        try {
            $teacher->update($data);
            return redirect()->route('teacher.index')->with('message', 'Đã cập nhật giáo viên thành công.');
        } catch (\Exception $e) {
            return redirect()->back()->with('message', 'Đã có lỗi xảy ra trong quá trình cập nhật. Vui lòng thử lại.');
        }
    }
    public function destroy($id)
    {
        $teacher = User::findOrFail($id);
        try {
            $teacher->delete();
            return redirect()->route('teacher.index')->with('message', 'Đã xóa giao viên thành công.');
        } catch (\Exception $e) {
            return redirect()->back()->with('message', 'Đã có lỗi xảy ra trong quá trình xóa. Vui lòng thử lại');
        }
    }
    public function listTeacher($id)
    {
        $class = User::join('model_has_roles', function($join) {
            $join->on('users.id', '=', 'model_has_roles.model_id');
          })
          ->where('school_id', $id)->where('user_type', UserTypeEnum::TEACHER)
          ->where('model_has_roles.role_id',3)->get();
        return response()->json($class);
    }
}
