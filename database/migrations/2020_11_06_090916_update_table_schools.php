<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTableSchools extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('schools', 'representative')){
            Schema::table('schools', function (Blueprint $table){
                $table->dropColumn('representative');
                $table->string('email')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('schools', 'email')){
            Schema::table('schools', function (Blueprint $table){
                $table->dropColumn('email');
                $table->string('representative');
            });
        }
    }
}
