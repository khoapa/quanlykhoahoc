function addStatusFunction(status, id) {
    var url = 'admin/news/update-status';
    $.ajax({
        url: url,
        type: 'get',
        dataType: 'json',
        data: {
            'status': status,
            'id': id
        },
        success: function(data) {
            alert(data);
        }
    });
}