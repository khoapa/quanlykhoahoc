<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use App\Models\SchoolCourse;

$factory->define(App\Models\SchoolCourse::class, function (Faker $faker) {
    return [
        'school_id' => rand(1, 10),
        'course_id' => rand(1, 10),
    ];
});
