<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserCourseResult extends Model
{
    protected $table = 'user_courses_results';
    protected $fillable = [
        'user_course_id','question_id','answer','point'
    ];
    public function question(){
        return $this->belongsTo(Question::class,'question_id','id');
    }
    public function userCourse(){
        return $this->belongsTo(UserCourse::class,'user_course_id','id');
    }
}
