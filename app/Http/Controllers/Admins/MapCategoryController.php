<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryMapCreateRequest;
use App\Http\Requests\CategoryMapUpdateRequest;
use App\Models\MapCategory;
use App\Services\UploadService;
use Illuminate\Http\Request;

class MapCategoryController extends Controller
{
    public function index(Request $request)
    {
        $name = $request->get('search');
        $query = MapCategory::orderBy('id','DESC');
        if(!is_null($name)){
            $query = $query->where('name','LIKE','%'.$name.'%');
        }
        $categories = $query->paginate(config('settings.countPaginate'));
        return view('admins.map_category.index',compact('categories'));
    }
    public function create()
    {
        return view('admins.map_category.create');
    }
    public function store(CategoryMapCreateRequest $request)   
    {
        $data = $request->getData();
        if ($request->hasFile('icon')) {
           $file = $request->file('icon');
           $data_image = UploadService::uploadImage('map_category', $file);
           $data['icon'] = $data_image;
       }
        MapCategory::create($data);
        return redirect()->route('map_category_index')
            ->with('message', 'Tạo thể loại điểm map thành công');
    }
    public function edit($id)
    {
        $category = MapCategory::find($id);
        return view('admins.map_category.edit',compact('category'));
    }
    public function update(CategoryMapUpdateRequest $request, $id)
    {   
        $category = MapCategory::find($id);
        $data = $request->getData();
        $data_image = $category->icon;
        if ($request->hasFile('icon')) {
            $file = $request->file('icon');
            $data_image = UploadService::uploadImage('map_category', $file);
        }
        $data['icon'] = $data_image;
        $category->update($data);
        return redirect()->route('map_category_index')->with('message', 'Cập nhật thể loại điểm map thành công');
    }
    public function delete($id)
    {
        $category = MapCategory::find($id);
        $category->delete();
        return redirect()->route('map_category_index')->with('message', 'Xóa thể loại điểm map thành công');
    }
    
}
