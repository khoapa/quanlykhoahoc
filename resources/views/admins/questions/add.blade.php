@extends('admin')
@section('content')
<div class="app-page-title app-page-title-custom">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-plus icon-gradient bg-happy-itmeo">
                </i>
            </div>
            <div>
                <h5>Thêm Mới Câu Hỏi</h5>
            </div>
        </div>
    </div>
</div>
<div class="main-card card main-card-custom">
    <div class="card-body">
        <form enctype="multipart/form-data" method="POST" novalidate="" action="{{route('question.question.store') }}" accept-charset="UTF-8" id="create_exhibition_form" name="create_exhibition_form" class="form-horizontal {{ $errors->any() ? 'was-validated' : '' }}">
            {{ csrf_field() }}

            @include ('admins.questions.form', [
                'question' => null,
                'courses' => $courses
                ])
                <div class="form-group">
                <div class="col-md-offset-2">
                <a href="{{ route('question.question.index',$id) }}" class="mb-2 mr-2 btn-icon btn-pill btn btn-outline-secondary"><i class="pe-7s-back btn-icon-wrapper"> </i>Quay lại</a>
                    <button type="submit" class="mb-2 mr-2 btn-icon btn-pill btn btn-outline-success"><i
                            class="mr-2 pe-7s-check btn-icon-wrapper"> </i>Lưu</button>
                    <button id="rediret-answer" class="mb-2 mr-2 btn-icon btn-pill btn btn-outline-success"><i
                            class="mr-2 pe-7s-check btn-icon-wrapper"> </i>Thêm câu trả lời</button>
                </div>

                </div>
        </form>

    </div>
</div>
@endsection