<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Hash;
use Illuminate\Http\Request;
class ChangePassRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $getOldPass = Request::get('old_password');       
        return [
        'old_password' => [
                'required',
                function ($attribute, $value, $fail) use($getOldPass) {
                    $passwordOld = auth('api')->user()->password;
                    if (!Hash::check($getOldPass, $passwordOld)){
                        return $fail("Mật khẩu cũ không đúng.");
                    }
                }
            ],  
          'password' => 'required|confirmed',
          'password_confirmation' => 'required',
        ];
    }
    public function messages()
    {
        $messages = [ 
            'old_password.required' => 'Mật khẩu cũ là trường bắt buộc', 
            'password.required' => 'Mật khẩu là trường bắt buộc', 
            'password.confirmed' =>'Mật khẩu xác nhận không khớp với mật khẩu đã nhập',
            'password_confirmation.required' => 'Mật khẩu xác nhận là trường bắt buộc',
        ];
        return $messages;
    }
}
