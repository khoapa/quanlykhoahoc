<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Project;
use App\User;

class Notification extends Model
{
    protected $table = "notifications";
    protected $fillable = ['sender_id', 'receiver_id', 'title', 'content', 'status'];

    public function sender(){
      return $this->belongsTo(User::class, 'sender_id', 'id');
    }

    public function receiver(){
      return $this->belongsTo(User::class, 'receiver_id', 'id');
    }

    public function scopeUnread($query, $user){
      return $query->where(['receiver_id' => $user->id, 'status' => 'false'])->latest();
    }

    public function scopeOfUser($query, $user){
      return $query->where(['receiver_id' => $user->id])->latest();
    }

    public function getReadStatusAttribute(){
      return $this->status == 0 ? false : true;
    }
}
