<?php

namespace App\Http\Controllers\Admins;

use App\Enums\UserTypeEnum;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\CreateCourseRequest;
use App\Models\CourseCategory;
use App\Models\Course;
use App\Http\Requests\UpdateCourseRequest;
use App\Services\UploadService;
use App\Helpers\Helper;
use App\User;

class CourseController extends Controller
{
    use \App\Traits\NotificationTrait;
    public function index(Request $request)
    {
        $user = auth()->user();
        if ($user->user_type == UserTypeEnum::STUDENT) {
            return view('errors.403');
        } else {
            $courses = Course::orderBy('id', 'DESC');
            $name = $request->get('search');
            if (!is_null($name)) {
                $courses = $courses->where('name', 'LIKE', '%' . $name . '%');
            }
            if ($user->admin_flg != 1) {
                $courses = $courses->whereHas('school', function ($q) use ($user) {
                    $q = $q->where('school_id', $user->school_id);
                });
            }
            $courses = $courses->paginate(config('settings.countPaginate'));
            return view('admins.course.index', compact('courses'));
        }
    }
    public function create()
    {
        $categoryCourses = CourseCategory::get();
        return view('admins.course.add', compact('categoryCourses'));
    }
    public function store(CreateCourseRequest $request)
    {

        $data = $request->getData();
        $creator = auth()->user();
        $users = [];
        if ($data['status'] == 1)
            if ($creator->admin_flg == 1) {
                $users = User::where('id', "!=", $creator->id)->get();
            } else {
                $users = User::where('id', "!=",  $creator->id)->where('school_id', $creator->school_id)->get();
            }
        if ($request->hasFile('cover_image')) {
            $file = $request->file('cover_image');
            $data_image = UploadService::uploadImage('course', $file);
            $data['cover_image'] = $data_image;
        }
        if (!is_null($data['start_date'])) {
            $data['start_date'] = Helper::formatSqlDate($data['start_date']);
        }
        $data['author'] = $creator->id;
        $data['total_point'] = config('settings.totalPoint');
        $insertData = Course::create($data);
        $title_notification = '' . $creator->name . ' đã tạo một khoá học.';

        foreach ($users as $user)
            $this->notifcationSend($title_notification, "", $creator, $user);
        if ($insertData) {
            return redirect()->route('question.question.index', $insertData->id)->with('message', trans('course.create_success'));
        } else {
            return redirect()->back()->with('message', trans('course.create_fail'));
        }
    }
    public function edit($id)
    {
        $course = Course::findOrFail($id);
        $categoryCourses = CourseCategory::get();
        return view('admins.course.edit', compact('course', 'categoryCourses'));
    }
    public function update(UpdateCourseRequest $request, $id)
    {
        $course = Course::findOrFail($id);
        $data = $request->getData();
        $data_image = $course->cover_image;
        if ($request->hasFile('cover_image')) {
            $file = $request->file('cover_image');
            $data_image = UploadService::uploadImage('course', $file);
        }
        $data['cover_image'] = $data_image;
        if (!is_null($data['start_date'])) {
            $data['start_date'] = Helper::formatSqlDate($data['start_date']);
        }
        $update = $course->update($data);
        $countQuestionChoose = $course->question->where('type', 1)->count();
        $countQuestionEssay = $course->question->where('type', 2)->count();
        $totalPointChoice = $course->total_point_choice;
        $totalPoint = $course->total_point;
        if ($countQuestionChoose >= 1) {
            $point = $totalPointChoice / $countQuestionChoose;
            $course->question()->where('type', 1)->update(['point' => $point]);
        }
        if ($countQuestionEssay >= 1) {
            $point = ($totalPoint - $totalPointChoice) / $countQuestionEssay;
            $course->question()->where('type', 2)->update(['point' => $point]);
        }
        if ($update) {
            return redirect()->route('question.question.index', $course->id)->with('message', trans('course.update_success'));
        } else {
            return redirect()->back()->with('message', trans('course.update_fail'));
        }
    }
    public function destroy($id)
    {
        $course = Course::findOrFail($id);
        $delCourse = $course->delete();
        if ($delCourse) {
            return redirect()->route('course.index')->with('message', trans('course.delete_success'));
        } else {
            return redirect()->back()->with('message', trans('course.delete_fail'));
        }
    }
}
