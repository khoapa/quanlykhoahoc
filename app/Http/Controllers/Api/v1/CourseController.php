<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Models\CourseCategory;
use App\Models\Course;
use App\Http\Resources\CourseCollection;
use App\Enums\StatusCourseEnum;
use App\Http\Resources\CourseResource;
use App\Http\Resources\CourseDetailResource;
use App\Models\Question;
use App\Http\Resources\CourseAllCollection;
use Facade\FlareClient\Api;
use Carbon\Carbon;
use App\Helpers\Helper;
use App\Models\UserCourse;
use App\Http\Requests\ReplyRequest;
use App\Models\UserCourseResult;
use App\Http\Requests\UpdateUserApiRequest;
use App\Http\Resources\CourseResultResource;

class CourseController extends ApiController
{
    public function index(Request $request){
        $categoryId = $request->get('category_id');
        $searchText = $request->get('search_text');
        $perPage = $request->get('per_page');
        $query = Course::with('category');
        if(!is_null($categoryId)){
          $query = $query->where('course_category_id','=',$categoryId);  
        }
        if(!is_null($searchText)){
          $query = $query->where('name','LIKE','%'.$searchText.'%');  
        }
        $perPage = $perPage ? $perPage : config('settings.countPaginate');
        $query = $query->where('status',1)->latest()->paginate($perPage);
        return $this->formatJson(CourseCollection::class, $query);
    }
    public function listOffer(Request $request){
      $searchText = $request->get('search_text');
      $perPage = $request->get('per_page');
      $query = Course::orderBy('id','DESC');
      if(!is_null($searchText)){
        $query = $query->where('name','LIKE','%'.$searchText.'%');  
      }
      $perPage = $perPage ? $perPage : config('settings.countPaginate');
      $query = $query->where('offer',1)->where('status',StatusCourseEnum::SHOW)->paginate($perPage);
      return $this->formatJson(CourseCollection::class, $query); 
    }
    public function listNew(Request $request){
      $searchText = $request->get('search_text');
      $perPage = $request->get('per_page');
      $query = Course::orderBy('id','DESC');
      if(!is_null($searchText)){
        $query = $query->where('name','LIKE','%'.$searchText.'%');  
      }
      $perPage = $perPage ? $perPage : config('settings.countPaginate');
      $query = $query->where('new',1)->where('status',StatusCourseEnum::SHOW)->paginate($perPage);
      return $this->formatJson(CourseCollection::class, $query); 
    }
    public function show($id){
      $user = auth()->user();
      $course = Course::findOrFail($id);
      $user_course = UserCourse::where('user_id',$user->id)->where('course_id',$id)->first();
      if($user_course){
        $number_of_visits = !is_null($user_course->number_of_visits) ? $user_course->number_of_visits : 0;
        $user_course = $user_course->update(['number_of_visits'=> $number_of_visits+1]);
      }
      return $this->formatJson(CourseDetailResource::class,$course);
    }
    public function listAll(Request $request){
      $searchText = $request->get('search_text');
      $perPage = $request->get('per_page');
      $query = CourseCategory::orderBy('id','DESC');
      if(!is_null($searchText)){
        $query = $query->where('name','LIKE','%'.$searchText.'%');  
      }
      $perPage = $perPage ? $perPage : config('settings.countPaginate');
      $query = $query->paginate($perPage);
      return $this->formatJson(CourseAllCollection::class, $query);
    }
    public function join($id){
      $user = auth()->user();
      $data  = [
          'join_date' => Helper::formatSqlDate(Carbon::now()), 
          'course_id' => $id,
          'user_id' => $user->id,
      ];
      $course = UserCourse::where('user_id',$user->id)->where('course_id',$id)->first();
      if($course){
        $course->delete();
        return $this->sendMessage('Bạn đã rời khóa học.');
      }
      UserCourse::create($data);
      return $this->sendMessage('Tham gia khóa học thành công.');
    }
    public function favourite($id){
      $course = Course::findOrFail($id);
      $user = auth()->user();
      $favorite = $course->userFavourite()->firstOrNew(['user_id' => $user->id]);
      $status = is_null($favorite->id);
      $status ? $user->userFavourite()->attach($id) : $user->userFavourite()->detach($id);
      if($status == 1){
        return $this->sendMessage('Khóa học đã được thêm vào danh sách yêu thích.'); 
      }else{
        return $this->sendMessage('Bỏ yêu thích khóa học thành công.'); 
      }
    }
    public function history(Request $request){
      $status = $request->get('status');
      $user = auth()->user();
      $perPage = $request->get('per_page');
      $perPage = $perPage ? $perPage : config('settings.countPaginate');
      $query = $user->userCourse();
      if(!is_null($status)){
        $query = $query->wherePivot('status',$status);
      }
      $courses = $query->paginate($perPage);
      return $this->formatJson(CourseCollection::class,$courses);
    }
    public function listFavourite(Request $request){
      $user = auth()->user();
      $perPage = $request->get('per_page');
      $perPage = $perPage ? $perPage : config('settings.countPaginate');
      $courses = $user->userFavourite()->paginate($perPage);
      return $this->formatJson(CourseCollection::class,$courses);
    }
    public function reply(ReplyRequest $request, $id){
      $course = Course::findOrFail($id);
      $user = auth()->user();
      $userCourse = UserCourse::where('user_id',$user->id)->where('course_id',$id)->first();
      $data = $request->all();
      $point = 0;
      foreach($data as $d){
        $question = Question::findOrFail($d['id_question']);
        if($d['type'] == 1 && $question->multiselect == 0){
        $answerCorrect = $question->answer()->where('correct',1)->first();
         if($d['answer'] == $answerCorrect->id){
           $point = $question->point;
         } 
        }elseif($d['type'] == 1 && $question->multiselect == 1){
          $answerCorrect = $question->answer()->where('correct',1)->pluck('id')->toArray();
          $arrAnswer = explode(',',$d['answer']);
          $diffArr = array_diff($answerCorrect,$arrAnswer);
          if(empty($diffArr) && count($answerCorrect) == count($arrAnswer)){
            $point = $question->point;
          }
        }else{
          $point = 0;
        }
       $arr = [
         'user_course_id' => $userCourse->id,
         'question_id' => $d['id_question'],
         'answer' => $d['answer'],
         'point' => $point
       ];
       $create = UserCourseResult::create($arr);
      }

     return $this->formatJson(CourseResultResource::class,$userCourse);
    }
    public function result($id){
      $user = auth()->user();
      $userCourse = UserCourse::where('user_id',$user->id)->where('course_id',$id)->first();
      return $this->formatJson(CourseResultResource::class,$userCourse);
    }

}