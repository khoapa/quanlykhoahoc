<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $table = 'questions';
    protected $fillable = [
        'course_id','type','subject','multiselect','point'
    ];
    public function course(){
        return $this->belongsTo(Course::class,'course_id','id');
     }
    public function getTypeQuestionAttribute()
    {
        $value = $this->type;
        if($value == 1){
            $typeQuestion = "Trắc nghiệm";
        }else{
            $typeQuestion = "Tự luận";
        }
        return $typeQuestion;
    }
    public function answer(){
        return $this->hasMany(Answer::class,'question_id','id');
    }
}
