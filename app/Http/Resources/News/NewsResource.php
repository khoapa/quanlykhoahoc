<?php

namespace App\Http\Resources\News;

use App\Models\Tag;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Str;
use App\Helpers\Helper;

class NewsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {   
        $tags = Tag::whereIn("id",is_null($this->tag_ids) ? [0] : json_decode($this->tag_ids))->select("id","name")->get();
        return [
            'id' => $this->id,
            'title' => $this->title,
            'thumbnail' => $this->thumbnail,
            'news_category_id' => $this->news_category_id,
            'content' => $this->content,
            'priority' => $this->priority,
            'link' => $this->link,
            'status' => $this->status,
            'tags' => $tags,
            'description' =>Str::limit(preg_replace( "/\r|\n/", "", strip_tags(html_entity_decode($this->content))),150) ,
            'created_at' => Helper::formatDateDiffForHumans($this->created_at)
        ];
    }
}
