<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Permission;
use App\Models\Role;
use App\Models\RoleHasPermission;
use Spatie\Permission\Models\Role as SpatieRole;
use Illuminate\Support\Facades\Auth;

class PermisionController extends Controller
{
    public function index(){
        $roles = Role::where('name','!=','Admin')->paginate(config('settings.countPaginate'));
        return view('admins.permissions.index',compact('roles'));
    }
    public function show($id){
        $role = Role::findOrFail($id);
        $roles = $role->permissionRole()->get();
        $permissions = Permission::select('id','subject','name','description')->get()->groupBy('subject');                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
        return view('admins.permissions.add',compact('permissions','roles','id','role'));
    }
    public function update($id,Request $request){
        $role = Role::findOrFail($id);
        $dataPermission = $request->get('permision');
        if(!empty($dataPermission)){
            foreach ($dataPermission as $key => $value) {
                $rolePermission = RoleHasPermission::where('role_id', $id)
                ->where('permission_id', $value)->first();
                if(empty($rolePermission)){
                    $role->permissionRole()->attach($value);
                }
            }
            RoleHasPermission::where('role_id', $id)->whereNotIn('permission_id', $dataPermission)->delete();
        }else{
            RoleHasPermission::where('role_id', $id)->delete();
        }
        return redirect()->route('permission.permission.index')->with('message','Cập nhập phân quyền thành công.');
    }
}
