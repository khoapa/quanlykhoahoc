<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Models\CategoryNews;
use App\Http\Resources\CategoryNewResource;

class CategoryNewController extends ApiController
{
    public function index(){
        $categories = CategoryNews::all();
        return $this->formatCollectionJson(CategoryNewResource::class,$categories);
    }
}
