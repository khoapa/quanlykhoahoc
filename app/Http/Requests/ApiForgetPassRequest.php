<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ApiForgetPassRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'data_verify' => [
                'required', function ($attribute, $value, $fail) {
                    if (!is_null($value)) {
                        if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
                            if (!preg_match('/^[0-9]{10}+$/', $value))
                            return $fail("Email hoặc số điện thoại không đúng định dạng.");
                        }
                    }
                }
            ],
        ];
    }
    public function messages()
    {
        $messages = [ 
            'data_verify.required' => 'Email hoặc số điện thoại là trường bắt buộc', 
        ];
        return $messages;
    }
    public function getData()
    {
        $data = $this->only(['data_verify']);
        return $data;
    }
}
