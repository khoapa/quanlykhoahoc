<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryNews extends Model
{
    protected $table = 'category_news';
    protected $fillable = [
        'name',
    ];
    public function news()
    {
        return $this->hasMany(News::class,"news_category_id",'id');
    }
}
