@extends('admin')
@section('content')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-car icon-gradient bg-mean-fruit">
                    </i>
                </div>
                <div>
                    KẾT QUẢ HỌC TẬP
                </div>
            </div>

        </div>
    </div>
    @if(Session::has('message'))
    <div class="alert alert-success">
        <span class="glyphicon glyphicon-ok"></span>
        {!! session('message') !!}
        <button type="button" class="close" data-dismiss="alert" aria-label="close">
            <span aria-hidden="true">&times;</span>
        </button>

    </div>
    @endif
    <div class="col-md-12 pl-0">
        <div class="mb-3 card">
            <div class="card-body">
                <div class="tab-content">
                    <form enctype="multipart/form-data" method="get" novalidate="" action="{{route('transcript.user')}}" accept-charset="UTF-8" id="create_exhibition_form" name="create_exhibition_form" class="form-horizontal">
                        {{ csrf_field() }}
                        <div class="form-row">
                            <div class="col mb-3">
                                <h6 class="timeline-title">Chọn lớp :</h6>
                            </div>
                            <div class="col mb-3">
                                <div class="form-group">
                                    <select name="school" id="school_id" required class="custom-select {{ $errors->has('school') ? 'is-invalid' : '' }}">
                                        <option value="0">--Trường--</option>                                       
                                        @foreach($schools as $school)    
                                        <option @if($school->id == $school_id)
                                            selected = "selected"
                                            @endif
                                            value="{{$school->id}}">{{$school->name}}</option>
                                        @endforeach
                                    </select>
                                    {!! $errors->first('school', '<p class="invalid-feedback">:message</p>') !!}
                                </div>
                            </div>
                            <div class="col mb-3">
                                <div class="form-group">
                                    <select name="class" id="class_id" required class="custom-select {{ $errors->has('class') ? 'is-invalid' : '' }}">
                                        @foreach($classes as $value)
                                        <option @if($value->id == $class->id)
                                            selected="selected"
                                            @endif
                                            value="{{$value->id}}">{{$value->name}}</option>
                                        @endforeach
                                    </select>
                                    {!! $errors->first('class', '<p class="invalid-feedback">:message</p>') !!}
                                </div>
                            </div>
                            <div class="col mb-3">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col mb-3">
                                <h6 class="timeline-title">Và chọn khóa học cần xem :</h6>
                            </div>
                            <div class="col mb-3">
                                <div class="form-group">
                                    <select name="category" id="category_id" required class="custom-select {{ $errors->has('category') ? 'is-invalid' : '' }}">
                                        <option>--Danh mục khóa học--</option>
                                        @foreach($categoryCourses as $category)   
                                        <option @if($category->id == $category_id)
                                            selected = "selected"
                                            @endif
                                            value="{{$category->id}}">{{$category->name}}</option>
                                        @endforeach
                                    </select>
                                    {!! $errors->first('category', '<p class="invalid-feedback">:message</p>') !!}
                                </div>
                            </div>
                            <div class="col mb-3">
                                <div class="form-group">
                                    <select name="course" id="course_id" required class="custom-select {{ $errors->has('course') ? 'is-invalid' : '' }}">
                                        @foreach($courses as $value)course
                                        <option @if($value->id == $course->id)
                                            selected="selected"
                                            @endif
                                            value="{{$value->id}}">{{$value->name}}</option>
                                        @endforeach
                                    </select>
                                    {!! $errors->first('course', '<p class="invalid-feedback">:message</p>') !!}
                                </div>
                            </div>
                            <div class="col mb-3">
                                <button class="mb-2 mr-2 btn-icon btn-pill btn btn-outline-success"><i class="pe-7s-check btn-icon-wrapper"> </i>Xem bảng điểm</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="main-card card">
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">
                <table class="table table-striped ">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>Tên học sinh</th>
                            <th>Khóa học</th>
                            <th>Điểm trắc nghiệm</th>
                            <th>Điểm tự luận</th>
                            <th>Tổng điểm</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(!is_null($userCourses))
                        @foreach($userCourses as $user)
                        <tr>
                            <td></td>
                            <td>{{$user->student->name}}</td>
                            <td>{{$course->name}}</td>
                            <td>{{round($user->count_point_choose,2)}}</td>
                            <td>
                                @if($user->count_point_essay == 0)
                                --
                                @else
                                {{ $user->count_point_essay}}
                                @endif
                            </td>
                            <td>{{($user->count_point_choose*$user->course->total_point_choice+
                                $user->count_point_essay*($user->course->total_point - $user->course->total_point_choice))/100
                                }}</td>
                            <td>
                                <div class=" w-100">
                                    <a href="{{route('transcript.user.delete',['id'=>$user->id])}}" class="pull-right mb-2 mr-2 btn-icon btn-icon-only btn-shadow btn-dashed btn btn-outline-warning" title="Xóa" onclick="return confirm('Bạn muốn xóa ?')">
                                        <span class="pe-7s-trash" aria-hidden="true"></span>
                                    </a>
                                    @if(!is_null($user->courseResult->first()))
                                    <a href="{{route('transcript.mark',['user_id'=>$user->student->id,'course_id'=>$course->id])}}" class="pull-right mb-2 mr-2 btn-icon btn-icon-only btn-shadow btn-dashed btn btn-outline-primary">
                                        Chấm điểm
                                    </a>
                                    @endif
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
                
            </div>
        </div>
        <div class="panel-footer">
            <div class="col-12">
                <div class="pagination pull-right">

                </div>
            </div>
        </div>

    </div>
</div>
@endsection