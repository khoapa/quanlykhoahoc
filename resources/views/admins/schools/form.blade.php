<div class="form-row">
    <div class="col mb-6">
        <div class="form-group">
            <label for="name">Tên trường</label>
            <input type="text" class="form-control" maxlength="255" id="name" name="name" value="{{ old('name', optional(optional($school))->name) }}" placeholder="Tên trường" required>
            {!! $errors->first('name', '<p class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group">
            <label for="address">Địa chỉ</label>
            <input type="text" class="form-control" id="address" name="address" value="{{ old('address', optional(optional($school))->address) }}" placeholder="Địa chỉ" required>
            {!! $errors->first('address', '<p class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group">
            <label for="grade_ids">Khối :</label>
            <select id="grade_ids" class="grade_ids form-control" name="grade_ids[]" multiple="multiple">
                @foreach($grades as $grade)
                    @php
                    $check= null;
                    @endphp
                    @if(!is_null($gradesOfSchool))
                    @foreach($gradesOfSchool as $gradeOfSchool)
                    @if($grade->id == old('grade_ids', $gradeOfSchool ))
                    @php
                    $check= $grade->id;
                    @endphp
                    <option selected="selected" value="{{$grade->id}}">{{$grade->name}}</option>
                    @endif
                @endforeach
                @if($check==null)
                <option value="{{$grade->id}}">{{$grade->name}}</option>
                @endif
                @else
                <option value="{{$grade->id}}">{{$grade->name}}</option>
                @endif
                @endforeach
            </select>
        </div>
    </div>
    <div class="col mb-6">
        <div class="form-group">
            <label for="phone">Số điện thoại</label>
            <input type="number" class="form-control" id="phone" name="phone" value="{{ old('phone', optional(optional($school))->phone) }}" placeholder="Số điện thoại" required>
            {!! $errors->first('phone', '<p class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group {{ $errors->has('email') ? 'was-validated-custom' : 'was-validated-custom-valid'}}">
            <label for="email">Email</label>
            <input type="email" class="form-control" id="email" name="email" value="{{ old('email', optional(optional($school))->email) }}" placeholder="Email" required>
            {!! $errors->first('email', '<p class="invalid-feedback">:message</p>') !!}
        </div>
    </div>
</div>
<div class="form-group">
    <label for="description">Mô tả</label>
    <div>
        <textarea type="text" class="form-control" id="description" name="description" placeholder="Mô tả">{{ old('description', optional(optional($school))->description) }}</textarea>
    </div>
</div>
<div class="form-group {{ $errors->has('logo') ? 'was-validated-custom' : 'was-validated-custom-valid'}}">
    <label for="logo">Logo trường học</label>
    <div class="col-md-3 block-single-upload">
        {!! Helper::uploadSingleImage( old('logo', optional($school)->logo), 'logo') !!}
    </div>
    {!! $errors->first('logo', '<p class="invalid-feedback">:message</p>') !!}
</div>
<br>
<script type="text/javascript">
    $(document).ready(function() {
        $('.grade_ids').select2({
            placeholder: "Chọn khối",
        });
    });
</script>