<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserCategoryMap extends Model
{
    protected $table = 'user_category_map';
    protected $fillable = [
        'user_id','category_map_id','status'
    ];
   
}
