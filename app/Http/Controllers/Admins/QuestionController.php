<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Course;
use App\Http\Requests\CreateQuestionRequest;
use App\Models\Question;
use App\Http\Requests\UpdateQuestionRequest;

class QuestionController extends Controller
{
    public function index($id = null,Request $request){
        $searchCrouse = $request->get('course_name');
        $query = Question::orderBy('id','DESC');
        $course = Course::findOrFail($id);
        $questions = $query->where('course_id',$id)->paginate(config('settings.countPaginate'));
        return view('admins.questions.index',compact('questions','id','course'));
    }
    public function create($id = null){
        $query = Course::orderBy('id','DESC');
        if(!is_null($id)){
            $query = $query->where('id',$id);
        }
        $courses = $query->get();
        return view('admins.questions.add',compact('courses','id'));
    }
    public function store(CreateQuestionRequest $request){
        $data = $request->getData();
        $createQuestion = Question::create($data);
        $course = Course::findOrFail($createQuestion->course_id);
        $countQuestionChoose = $course->question->where('type',1)->count();
        $countQuestionEssay = $course->question->where('type',2)->count();
        $totalPointChoice = $course->total_point_choice;
        $totalPoint = $course->total_point;
        if($countQuestionChoose >= 1){
            $point = $totalPointChoice/$countQuestionChoose;
            $course->question()->where('type',1)->update(['point'=>$point]);
        }
        if($countQuestionEssay >= 1){
            $point = ($totalPoint-$totalPointChoice)/$countQuestionEssay;
            $course->question()->where('type',2)->update(['point'=>$point]);
        }
        if($data['rediret_answer'] == 0){
            return redirect()->route('question.question.index',$createQuestion->course_id)->with('message','Tạo câu hỏi thành công.');
        }else{
            return redirect()->route('answer.answer.create',$createQuestion->id);
        }
    }
    public function edit($id){
        $question = Question::findOrFail($id);
        $courses = Course::all();
        return view('admins.questions.edit',compact('courses','question'));
    }
    public function update(UpdateQuestionRequest $request,$id){
        $question = Question::findOrFail($id);
        $data = $request->getData();
        $updateQuestion = $question->update($data);
        $course = Course::findOrFail($question->course_id);
        $countQuestionChoose = $course->question->where('type',1)->count();
        $countQuestionEssay = $course->question->where('type',2)->count();
        $totalPointChoice = $course->total_point_choice;
        $totalPoint = $course->total_point;
        if($countQuestionChoose >= 1){
            $point = $totalPointChoice/$countQuestionChoose;
            $course->question()->where('type',1)->update(['point'=>$point]);
        }
        if($countQuestionEssay >= 1){
            $point = ($totalPoint-$totalPointChoice)/$countQuestionEssay;
            $course->question()->where('type',2)->update(['point'=>$point]);
        }
        if($data['rediret_answer'] == 0){
            return redirect()->route('question.question.index',$question->course_id)->with('message','Cập nhập câu hỏi thành công.');
        }else{
            return redirect()->route('answer.answer.create',$updateQuestion->id);
        }
    }
    public function destroy($id){
        $question = Question::findOrFail($id);
        $answer = $question->answer()->delete();
        $delQuestion = $question->delete();
        $course = Course::findOrFail($question->course_id);
        $countQuestionChoose = $course->question->where('type',1)->count();
        $countQuestionEssay = $course->question->where('type',2)->count();
        $totalPointChoice = $course->total_point_choice;
        $totalPoint = $course->total_point;
        if($countQuestionChoose >= 1){
            $point = $totalPointChoice/$countQuestionChoose;
            $course->question()->where('type',1)->update(['point'=>$point]);
        }
        if($countQuestionEssay >= 1){
            $point = ($totalPoint-$totalPointChoice)/$countQuestionEssay;
            $course->question()->where('type',2)->update(['point'=>$point]);
        }
        if ($delQuestion) {
            return redirect()->route('question.question.index',$question->course_id)->with('message','Xóa câu hỏi thành công.');
	    }else {                        
            return redirect()->back()->with('message','Xóa câu hỏi thất bại.');
	    }
    }
}
