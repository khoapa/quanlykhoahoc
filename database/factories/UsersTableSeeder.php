<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\User;
use Faker\Generator as Faker;

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->email,
        'avatar' => 'https://picsum.photos/200/300',
        'phone' => rand(1000000000, 9999999999),
        'password' => Hash::make(12345678),
        'birthday' => $faker->dateTimeBetween('1990-01-01', '2012-12-31')->format('Y/m/d'),
        'address' => $faker->address,
        'gender' => rand(1, 2),
        'school_id' => rand(1, 10),
        'class_id' => rand(1, 10),
        'position_id' => rand(1, 4),
        'user_type' => rand(1, 2),
    ];
});
