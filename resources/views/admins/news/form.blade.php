<div class="form-row">
    <div class="col mb-6">
        <div class="form-group {{ $errors->has('title') ? 'was-validated-custom' : 'was-validated-custom-valid' }}">
            <label for="title">Tiêu đề :</label>
            <input type="text" class="form-control" id="title" name="title" value="{{ old('title', optional(optional($news))->title) }}" placeholder="Tiêu đề" required>
            {!! $errors->first('title', '<p class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group">
            <label for="link">Link :</label>
            <input type="text" class="form-control" name="link" value="{{ old('link', optional(optional($news))->link)}}" placeholder="Link">
            {!! $errors->first('link', '<p class="invalid-feedback">:message</p>') !!}
        </div>
    </div>
    <div class="col mb-6">
        <div class="form-group">
            <label for="news_category_id">Thể loại :</label>
            <select required class="custom-select {{ $errors->has('news_category_id') ? 'is-invalid' : '' }}" name="news_category_id">
                <option value="">--Chọn thể loại tin tức--</option>
                @foreach($categories as $category)
                <option @if($category->id == old('news_category_id', optional(optional($news))->news_category_id))
                    selected = "selected"
                    @endif
                    value="{{$category->id}}">{{$category->name}}</option>
                @endforeach
            </select>
            {!! $errors->first('news_category_id', '<p class="invalid-feedback">:message</p>') !!}
        </div>
    </div>
</div>
<div class="form-group">
    <label for="content">Nội dung :</label>
    <textarea class="form-control" name="content" type="text" id="editor" required>{{ old('content', optional(optional($news))->content)}}</textarea>
    {!! $errors->first('content', '<p class="invalid-feedback">:message</p>') !!}
</div>
<div class="form-group">
    <label for="tag_ids">Hastag :</label>
    <select id="tag_ids" class="tag_id form-control" name="tag_ids[]" multiple="multiple">
        @foreach($tags as $tag)
            @php
                $check= null;
            @endphp
            @if(!is_null($tagsNews))
                @foreach($tagsNews as $tagNews)
                    @if($tag->id == old('tag_ids', $tagNews ))
                        @php
                            $check= $tag->id;
                        @endphp
                    <option selected="selected" value="{{$tag->id}}">{{$tag->name}}</option>
                    @endif
                @endforeach
                @if($check==null)
                <option value="{{$tag->id}}">{{$tag->name}}</option>
                @endif          
            @else
            <option value="{{$tag->id}}">{{$tag->name}}</option>
            @endif
        @endforeach       
    </select>
</div>
<div class="form-group {{ $errors->has('thumbnail') ? 'was-validated-custom' : 'was-validated-custom-valid'}}">
    <label for="thumbnail">Hình ảnh</label>
    <div class="col-md-3 block-single-upload">
        {!! Helper::uploadSingleImage( old('thumbnail', optional($news)->thumbnail), 'thumbnail') !!}
    </div>
    {!! $errors->first('thumbnail', '<p class="invalid-feedback">:message</p>') !!}
</div>
<br>
<div class="form-group">

</div>
@section('script')
<script src="https://cdn.ckeditor.com/4.12.1/standard/ckeditor.js"></script>

<script>
    $(document).ready(function() {
        CKEDITOR.replace('content');
    })
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.tag_id').select2({
            placeholder: "Chọn hastag",
            tags:true
        });
    });
</script>

@endsection