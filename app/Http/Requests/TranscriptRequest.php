<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TranscriptRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'class'=> 'required',
            'course'=> 'required',
          
        ];
    }
    public function messages(){
        $messages = [
            'class.required' => 'Bạn chưa chọn lớp',
            'course.required' => 'Bạn chưa chọn khóa học', 
        ];
        return $messages;
    } 
}
