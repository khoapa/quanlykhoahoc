<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use App\Http\Requests\SchoolCreateRequest;
use App\Http\Requests\SchoolUpdateRequest;
use App\Models\Classes;
use App\Models\School;
use Illuminate\Http\Request;
use App\Services\UploadService;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Enums\UserTypeEnum;
use App\Http\Requests\ClassCreateRequest;
use App\Http\Requests\ClassUpdateRequest;
use App\Models\Grade;
use App\Models\Role;

class SchoolController extends Controller
{
    public function index(Request $request)
    {
        $user = auth()->user();
        if ($user->admin_flg == 1) {
            $name = $request->get('search');
            $schools = School::orderBy('id', 'DESC');
            if (!is_null($name)) {
                $schools = $schools->where('name', 'LIKE', '%' . $name . '%');
            }
            $schools = $schools->paginate(config('settings.countPaginate'));
            return view('admins.schools.index', compact('schools'));
        }
        return view('errors.403');
    }
    public function create()
    {
        $roles = Role::where('name', '!=', 'Admin')->get();
        $grades = Grade::get();
        return view('admins.schools.create', compact('roles', 'grades'));
    }

    public function store(SchoolCreateRequest $request)
    {
        $data = $request->getData();
        $data['logo'] = config('settings.image');
        if ($request->hasFile('logo')) {
            $file = $request->file('logo');
            $dataImage = UploadService::uploadImage('school', $file);
            $data['logo'] = $dataImage;
        }
        $grade_ids  = $request->grade_ids;
        $data['grade_ids'] = json_encode($grade_ids);
        $data['email'] = str_replace(' ', '', $data['email']);
        $insertData = School::create($data);
        $dataUser = [
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make(config('settings.password')),
            'phone' => $data['phone'],
            'user_type' => UserTypeEnum::STAFF,
            'school_id' => $insertData->id,
            'avatar' => $data['logo']
        ];
        $user = User::create($dataUser);
        $roleId = 3;
        $user->syncRoles([$roleId]);
        $updateSchool = $insertData->update(['user_id' => $user->id]);
        if ($insertData) {
            return redirect()->route('school_index')->with('message', 'Tạo trường học thành công');
        } else {
            return redirect()->back()->with('message', 'Tạo trường học thất bại');
        }
    }
    public function delete($id)
    {
        $school = School::findOrFail($id);
        $school->delete();
        return redirect()->route('school_index')->with('message', 'Xóa trường học thành công');
    }
    public function edit($id)
    {
        $grades = Grade::get();
        $school = School::find($id);
        $gradesOfSchool = json_decode($school->grade_ids);
        $roles = Role::where('name', '!=', 'Admin')->get();
        return view('admins.schools.edit', compact('school', 'roles', 'grades', 'gradesOfSchool'));
    }

    public function update(SchoolUpdateRequest $request, $id)
    {
        $school = School::find($id);
        $data = $request->getData();
        $dataLogo = $school->logo;
        if ($request->hasFile('logo')) {
            $file = $request->file('logo');
            $dataLogo = UploadService::uploadImage('school', $file);
        }
        $data['logo'] = $dataLogo;
        $user = $school->staff;
        $data['email'] = str_replace(' ', '', $data['email']);
        // $roleId = $data['role_id'];
        // $roleOld = $user->role()->first();
        // if(!is_null($roleId)){
        //     $user->syncRoles([$roleId]);
        // }else{
        //     if($roleOld){
        //         $user->removeRole($roleOld->role_id);   
        //     }
        // }
        $grade_ids  = $request->grade_ids;
        $data['grade_ids'] = json_encode($grade_ids);
        $update = $school->update($data);
        if ($update) {
            return redirect()->route('school_index')->with('message', 'Đã cập nhật trường học thành công.');
        } else {
            return redirect()->route('school_index')->with('message', 'Đã có lỗi xảy ra trong quá trình cập nhật. Vui lòng thử lại.');
        }
    }
    public function createClass($id)
    {
        $school = School::find($id);
        $schools = School::get();
        $grades = Grade::get();
        $teachers = User::where('school_id', $id)->where('user_type', UserTypeEnum::TEACHER)->get();
        return view('admins.schools.create_class', compact('school', 'schools', 'grades', 'teachers'));
    }
    public function storeClass(ClassCreateRequest $request)
    {
        $data = $request->getData();
        if ($request->hasFile('thumbnail')) {
            $file = $request->file('thumbnail');
            $data_image = UploadService::uploadImage('class', $file);
            $data['thumbnail'] = $data_image;
        }
        $insertData = Classes::create($data);
        if ($insertData) {
            return redirect()->route('list_class', ['id' => $data['school_id']])->with('message', 'Tạo lớp học thành công');
        } else {
            return redirect()->back()->with('message', 'Tạo lớp học thất bại');
        }
    }
    public function editClass($id)
    {
        $class = Classes::find($id);
        $schools = School::get();
        $grades = Grade::get();
        $teachers = User::where('school_id', $class->school_id)->where('user_type', UserTypeEnum::TEACHER)->get();
        return view('admins.schools.edit_class', compact('class', 'schools', 'grades', 'teachers'));
    }
    public function updateClass(ClassUpdateRequest $request, $id)
    {
        $class = Classes::find($id);
        $schoolId = $class->school_id;
        $data = $request->getData();
        $dataImage = $class->thumbnail;
        if ($request->hasFile('thumbnail')) {
            $file = $request->file('thumbnail');
            $dataImage = UploadService::uploadImage('class', $file);
        }
        $data['thumbnail'] = $dataImage;
        $update = $class->update($data);
        if ($update) {
            return redirect()->route('list_class', ['id' => $schoolId])->with('message', 'Đã cập nhật lớp học thành công.');
        } else {
            return redirect()->route('class_index')->with('message', 'Đã có lỗi xảy ra trong quá trình cập nhật. Vui lòng thử lại.');
        }
    }
}
