<?php

namespace App\Enums;

use Spatie\Enum\Enum;
/**
 * @method static self admin()
 * @method static self visitor()
 * @method static self exhibitor()
 */
final class UserTypeEnum extends Enum
{
    const STUDENT = 1;
    const TEACHER = 2;
    const STAFF = 3;
    public static function getDescription($value): string
    {
        if ($value === self::STUDENT) {
            return 'Học sinh';
        }
        if ($value === self::TEACHER) {
            return 'Giáo viên';
        }
        if ($value === self::STAFF) {
            return 'Nhân viên quản lý';
        }
        return parent::getDescription($value);
    }
}