<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\School;
use App\Models\Course;
use App\Models\SchoolCourse;
use App\Http\Requests\SchoolCourseRequest;
use App\Models\ClassCourse;
use App\Models\Classes;
use App\Models\UserCourse;

class SchoolCourseController extends Controller
{
    public function index(Request $request)
    {
        $school_id = null;
        $schools = null;
        $classes = null;
        $listClasses = null;
        $class_id = null;
        $userCourses = null;
        if (!is_null($request->school_id) && is_null($request->class_id)) {
            $school_id = $request->school_id;
            session()->forget('classId');
            $listClasses = Classes::where('school_id', '=', $school_id)->get();
            $userCourses = UserCourse::whereHas('student', function ($query) use ($school_id) {
                $query = $query->where('school_id', $school_id);
            })->get();
            $schools = School::with('course')->where('id', $school_id)->paginate(config('settings.countPaginate'));
        }
        if (!is_null($school_id)) {
            Session()->put('schoolId', $school_id);
        }
        if (Session()->has('schoolId') && is_null($request->school_id) && is_null($request->class_id)) {
            $school_id = Session('schoolId');
            $listClasses = Classes::where('school_id', '=', $school_id)->get();
            $userCourses = UserCourse::whereHas('student', function ($query) use ($school_id) {
                $query = $query->where('school_id', $school_id);
            })->get();
            $schools = School::with('course')->where('id', $school_id)->paginate(config('settings.countPaginate'));
        }
        $listSchools = School::get();
        if (!is_null($request->class_id)) {
            $class_id = $request->class_id;
            $school_id = $request->school_id;
            $listClasses = Classes::where('school_id', '=', $school_id)->get();

            $userCourses = UserCourse::whereHas('student', function ($query) use ($class_id) {
                $query = $query->where('class_id', $class_id);
            })->get();
            $classes = Classes::with("course")->where('id', $class_id)->paginate(config('settings.countPaginate'));
        }
        if (!is_null($school_id) && !is_null($class_id)) {
            Session()->put('schoolId', $school_id);
            Session()->put('classId', $class_id);
        }
        if (Session()->has('schoolId') && Session()->has('classId') && is_null($request->school_id) && is_null($request->class_id)) {
            $class_id = Session('classId');
            $school_id = Session('schoolId');
            $listClasses = Classes::where('school_id', '=', $school_id)->get();

            $userCourses = UserCourse::whereHas('student', function ($query) use ($class_id) {
                $query = $query->where('class_id', $class_id);
            })->get();
            $classes = Classes::with("course")->where('id', $class_id)->paginate(config('settings.countPaginate'));
        }

        return view('admins.school_course.index', compact('schools', 'listSchools', 'school_id', 'classes', 'listClasses', 'class_id', 'userCourses'));
    }
    public function create($id)
    {
        $schools = School::with("course")->where('id', $id)->get();
        $Courses_id  = [];
        $school =  School::find($id);
        foreach ($schools as $valueSchool) {
            foreach ($valueSchool->course as $value) {
                $Courses_id[] = $value->pivot->course_id;
            }
        }
        $courses = Course::whereNotIn('id', $Courses_id)->get();
        return view('admins.school_course.create', compact('school', 'courses'));
    }
    public function store(SchoolCourseRequest $request, $id)
    {
        $course_id = $request->course_id;
        $data['school_id'] = $id;
        $school =  School::find($id);
        try {
            foreach ($course_id as $valueCourse) {
                $data['course_id'] = $valueCourse;
                SchoolCourse::create($data);
                $dataClass['course_id'] = $valueCourse;
                foreach ($school->classes as $class) {
                    $dataClass['classes_id'] = $class->id;
                    ClassCourse::create($dataClass);
                }
            }
            return redirect()->route('school_course.index')->with('message',  'Thêm mới khóa học trường ' . '"' . $school->name . '"' . ' thành công');
        } catch (\Exception $e) {
            return redirect()->back()->with('message', 'Đã có lỗi trong quá trình thực hiện vui long thử lại');
        }
    }
    public function destroy($id)
    {
        $schoolCourses = SchoolCourse::find($id);
        $course_id = $schoolCourses->course_id;
        $schools =  School::find($schoolCourses->school_id);
        try {
            $schoolCourses->delete();
            foreach ($schools->classes as $value) {
                ClassCourse::where([
                    ['classes_id', $value->id],
                    ['course_id', $course_id],
                ])->delete();
            }
            return redirect()->route('school_course.index')->with('message', 'Đã xóa Khóa Học trường  ' . '"' . $schools->name . '"' . ' thành công');
        } catch (\Exception $e) {
            return redirect()->back()->with('message', 'Đã có lỗi xảy ra trong quá trình xóa. Vui lòng thử lại.');
        }
    }
    public function createClass($id)
    {
        $classCourse_id = [];
        $schoolCourse_id = [];
        $classes = Classes::find($id);
        $classCourse = ClassCourse::where('classes_id', $id)->get();
        foreach ($classCourse as $value) {
            $classCourse_id[] = $value->course_id;
        }
        $schools = School::with("course")->where('id', $classes->school_id)->get();
        foreach ($schools as $valueSchool) {
            foreach ($valueSchool->course as $valueCourse) {
                $schoolCourse_id[] = $valueCourse->id;
            }
        }
        $course = array_diff($schoolCourse_id, $classCourse_id);
        $courses = Course::whereIn('id', $course)->get();
        return view('admins.class_course.create', compact('classes', 'courses'));
    }
    public function storeClass(SchoolCourseRequest $request, $id)
    {
        $data['classes_id'] = $id;
        $class = Classes::find($id);
        $course_id = $request->course_id;
        try {
            foreach ($course_id as $value) {
                $data['course_id'] = $value;
                ClassCourse::create($data);
            }
            return redirect()->route('school_course.index')->with('message',  'Thêm mới khóa học lớp ' . '"' . $class->name . '"' . ' thành công');
        } catch (\Exception $e) {
            return redirect()->back()->with('message', 'Đã có lỗi trong quá trình thực hiện vui long thử lại');
        }
    }
    public function destroyClass($id)
    {
        $classCourse = ClassCourse::find($id);
        $class = Classes::find($classCourse->classes_id);
        try {
            $classCourse->delete();
            return redirect()->route('school_course.index')->with('message', 'Đã xóa khóa học lớp ' . '"' . $class->name . '"' . ' thành công');
        } catch (\Exception $e) {
            return redirect()->back()->with('message', 'Đã có lỗi xảy ra trong quá trình xóa. Vui lòng thử lại.');
        }
    }
    // public function edit($id)
    // {
    //     $schoolCourses = SchoolCourse::find($id);
    //     $schools = School::with("course")->where('id', $id)->get();
    //     $Courses_id  = [];
    //     $school =  School::find($schoolCourses->school_id);
    //     foreach($schools as $valueSchool){
    //         foreach ($valueSchool->course as $value) {
    //             if($value->pivot->id == $id ){
    //                 continue;
    //             }
    //             $Courses_id[] = $value->pivot->course_id;
    //         }
    //     }
    //     $courses = Course::whereNotIn('id', $Courses_id)->get();
    //     return view('admins.school_course.update', compact('schoolCourses', 'courses','school'));
    // }
    // public function update(SchoolCourseRequest $request,$id)
    // {
    //     $schoolCourses = SchoolCourse::find($id);
    //     $data = $request->getData();
    //     $schools =  School::find($schoolCourses->school_id);
    //     $data['school_id'] = $schools->id;
    //     try {
    //         $schoolCourses->update($data);
    //         return redirect()->route('school_course.index')->with('message',  'Cập Nhập khóa học trường '."/ $schools->name /".' thành công');
    //     } catch (\Exception $e) {
    //         return redirect()->back()->with('message', 'Đã có lỗi trong quá trình thực hiện vui long thử lại');
    //     }
    // }
}
