<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryNewsCreateRequest;
use App\Http\Requests\CategoryNewsUpdateRequest;
use App\Http\Requests\NewsCreateRequest;
use App\Http\Requests\NewsUpdateRequest;
use App\Models\CategoryNews;
use App\Models\News;
use App\Models\Tag;
use Illuminate\Http\Request;
use App\Services\UploadService;
use App\User;

class NewsController extends Controller
{
    use \App\Traits\NotificationTrait;
    public function index(Request $request)
    {
        $title = $request->get('search');
        $query = News::orderBy('id', 'DESC');
        if (!is_null($title)) {
            $query = $query->where('title', 'LIKE', '%' . $title . '%');
        }
        $news = $query->paginate(config('settings.countPaginate'));
        return view('admins.news.index', compact('news'));
    }
    public function create()
    {
        $categories = CategoryNews::get();
        $tags = Tag::get();
        return view('admins.news.create', compact('categories', 'tags'));
    }
    public function store(NewsCreateRequest $request)
    {
        $data = $request->getData();
        $tags = $request->input('tag_ids');
        $creator = auth()->user();
        $arrayTag = [];
        if (!is_null($tags)) {
            foreach ($tags as $value) {
                if (Tag::find($value)) {
                    $arrayTag[] = $value;
                    continue;
                } else {
                    $tag = new Tag();
                    $tag->name = $value;
                    $tag->save();
                    $arrayTag[] = $tag->id;
                }
            };
        }
        $data['tag_ids'] = json_encode($arrayTag);
        if ($request->hasFile('thumbnail')) {
            $file = $request->file('thumbnail');
            $data_image = UploadService::uploadImage('news', $file);
            $data['thumbnail'] = $data_image;
        }
        $insertData = News::create($data);
        $users = User::where('id', "!=", $creator->id)->get();
        $title_notification = 'Bạn có một tin tức mới.';
        foreach ($users as $user)
            $this->notifcationSend($title_notification, "", $creator, $user);
        if ($insertData) {
            return redirect()->route('news_index')->with('message', 'Tạo tin tức thành công');
        } else {
            return redirect()->back()->with('message', 'Tạo tin tức thất bại');
        }
    }
    public function delete($id)
    {
        $news = News::findOrFail($id);
        $news->delete();
        return redirect()->route('news_index')->with('message', 'Xóa tin tức thành công');
    }
    public function edit($id)
    {
        $news = News::find($id);
        $tagsNews = json_decode($news->tag_ids);
        $categories = CategoryNews::get();
        $tags = Tag::get();
        return view('admins.news.edit', compact('news', 'categories', 'tags', 'tagsNews'));
    }
    public function update(NewsUpdateRequest $request, $id)
    {
        $news = News::find($id);
        $updater = auth()->user();
        $data = $request->getData();
        $tags = $request->input('tag_ids');
        $arrayTag = [];
        if (!is_null($tags)) {
            foreach ($tags as $value) {
                if (Tag::find($value)) {
                    $arrayTag[] = $value;
                    continue;
                } else {
                    $tag = new Tag();
                    $tag->name = $value;
                    $tag->save();
                    $arrayTag[] = $tag->id;
                }
            };
        }
        $data['tag_ids'] = json_encode($arrayTag);
        $data_image = $news->thumbnail;
        if ($request->hasFile('thumbnail')) {
            $file = $request->file('thumbnail');
            $data_image = UploadService::uploadImage('news', $file);
        }
        $data['thumbnail'] = $data_image;
        $update = $news->update($data);
        $users = User::where('id', "!=", $updater->id)->get();
        $title_notification = 'Có một tin tức được cập nhập.';
        foreach ($users as $user)
            $this->notifcationSend($title_notification, "", $updater, $user);
        if ($update) {
            return redirect()->route('news_index')->with('message', 'Đã cập nhật tin tức thành công.');
        } else {
            return redirect()->route('news_index')->with('message', 'Đã có lỗi xảy ra trong quá trình cập nhật. Vui lòng thử lại.');
        }
    }
    public function updateStatusAjax(Request $request)
    {
        if ($request->ajax()) {
            $news = News::findOrFail($request->id);
            $news->status = $request->status;
            if ($news->save()) {
                $message = 'chuyển trạng thái thành công';
            } else {
                $message = 'chuyển trạng thái thất bại';
            }
            return response()->json($message);
        }
    }
}
