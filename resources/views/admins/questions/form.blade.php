<input type="hidden" value="0" id="inp-rediret-answer" name="rediret_answer">

<div class="form-row mb-2">
    
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('course_id') ? 'was-validated ' : '' }}">
            <label for="sel1">Khóa học</label>
            <select class="custom-select {{ $errors->has('course_id') ? 'is-invalid' : '' }}" id="course_id_disable"
                name="course_id">
                @foreach($courses as $course)
                <option value="{{$course->id}}" @if($course->id == old('course_id',
                    optional(optional($question))->course_id))
                    selected = "selected"
                    @endif
                    >{{$course->name}}</option>
                @endforeach
            </select>
            {!! $errors->first('course_id', '<p class="invalid-feedback">:message</p>') !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('type') ? 'was-validated ' : '' }}">
            <label for="sel1">Loại câu hỏi</label>
            <select class="custom-select {{ $errors->has('type') ? 'is-invalid' : '' }}" id="type" name="type">
                @php
                $type = old('type', optional(optional($question))->type)
                @endphp
                <option value="1" {!! Helper::setSelected($type, 1) !!}>Trắc nghiệm</option>
                <option value="2" {!! Helper::setSelected($type, 2) !!}>Tự luận</option>
            </select>
            {!! $errors->first('type', '<p class="invalid-feedback">:message</p>') !!}
        </div>
    </div>
</div>
<div class="form-group">
    <div class="form-row">
        <div class="col-md-6">
            <label for="firstname">Nội dung câu hỏi</label>
            <div>
                <input type="text" value="{{ old('subject', optional(optional($question))->subject) }}"
                    class="form-control" id="subject" name="subject" required>
                {!! $errors->first('subject', '<p class="invalid-feedback">:message</p>') !!}
            </div>
        </div>
        <div class="col-md-6" id="form-select-multi">
            <div class="form-group {{ $errors->has('multiselect') ? 'was-validated ' : '' }}">
                <label for="sel1">Được chọn nhiều</label>
                <select class="custom-select {{ $errors->has('multiselect') ? 'is-invalid' : '' }}" id="multiselect"
                    name="multiselect">
                    @php
                    $multiselect = old('multiselect', optional(optional($question))->multiselect)
                    @endphp
                    <option value="0" {!! Helper::setSelected($multiselect, 1) !!}>Không chọn nhiều đáp án</option>
                    <option value="1" {!! Helper::setSelected($multiselect, 0) !!}>Chọn nhiều đáp án</option>
                </select>
                {!! $errors->first('multiselect', '<p class="invalid-feedback">:message</p>') !!}
            </div>
        </div>
    </div>
</div>