<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UserCourse;

class PointController extends Controller
{
    public function index(){
        $userCourses = UserCourse::where('total_point','!=',0)->paginate(config('settings.countPaginate'));
        return view('admins.point.index',compact('userCourses'));
    }
}
