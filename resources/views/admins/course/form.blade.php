<div class="form-group {{ $errors->has('name') ? 'was-validated-custom' : 'was-validated-custom-valid' }}">
    <div class="form-row mb-2">
        <div class="col-md-6">
            <label for="firstname">Tên Khóa Học</label>
            <div>

                <input type="text" value="{{ old('name', optional(optional($course))->name) }}" class="form-control"
                    id="name" name="name" required>
                {!! $errors->first('name', '<p class="invalid-feedback">:message</p>') !!}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group {{ $errors->has('course_category_id') ? 'was-validated ' : '' }}">
                <label for="sel1">Danh mục khóa học</label>
                <select class="custom-select {{ $errors->has('course_category_id') ? 'is-invalid' : '' }}"
                    id="course_category_id" name="course_category_id">
                    @foreach($categoryCourses as $cate)
                    <option value="{{$cate->id}}" @if($cate->id == old('course_category_id',
                        optional(optional($course))->course_category_id))
                        selected = "selected"
                        @endif
                        >{{$cate->name}}</option>
                    @endforeach
                </select>
            </div>
            @if( $errors->has('course_category_id') )
            <div class="alert alert-danger">{{ $errors->first('course_category_id') }}</div>
            @endif
        </div>
    </div>
    <div class="form-row mb-2">
        <div class="col-md-6">
            <label for="birthday">Ngày Khai Giảng</label>
            <div>
                @php
                $date = old('start_date', optional(optional($course))->start_date);
                @endphp
                <input type="text" value="{!! Helper::formatDate1($date) !!}" class="form-control" id="datepicker"
                    name="start_date" placeholder="25-10-2004" required>
                {!! $errors->first('start_date', '<p class="invalid-feedback">:message</p>') !!}
            </div>
        </div>
        <div class="col-md-6">
            <div
                class="form-group {{ $errors->has('longtime') ? 'was-validated-custom' : 'was-validated-custom-valid' }}">
                <label for="firstname">Số ngày học</label>
                <div>
                    <input type="number" value="{{ old('longtime', optional(optional($course))->longtime) }}"
                        class="form-control" id="longtime" name="longtime" required>
                    {!! $errors->first('longtime', '<p class="invalid-feedback">:message</p>') !!}
                </div>
            </div>
        </div>
    </div>
    <div class="form-row mb-2">
        <div class="col-md-6">
            <div
                class="form-group {{ $errors->has('total_point_choice') ? 'was-validated-custom' : 'was-validated-custom-valid' }}">
                <label for="firstname">Tỉ lệ điểm trắc nghiệm(%)</label>
                <div>
                    <input type="number"
                        value="{{ old('total_point_choice', optional(optional($course))->total_point_choice) }}"
                        class="form-control" id="total_point_choice" name="total_point_choice" required>
                    {!! $errors->first('total_point_choice', '<p class="invalid-feedback">:message</p>') !!}
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div
                class="form-group">
                <label for="firstname">Tỉ lệ điểm tự luận(%)</label>
                <div>
                    <input type="text" disabled
                        class="form-control" id="total_point_essay">
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="avatar" class="control-label">Hình ảnh(*)</label>
        <div class="col-md-3 block-single-upload">
            {!! Helper::uploadSingleImage( old('cover_image', optional($course)->cover_image), 'cover_image') !!}
            {!! $errors->first('cover_image', '<p class="invalid-feedback">:message</p>') !!}
        </div>
    </div>
    <div class="form-group">
        <label>Hiển thị </label>
        @php
        $new = old('new', optional(optional($course))->new);
        $offer = old('offer', optional(optional($course))->offer);
        $status = old('offer', optional(optional($course))->status);
        @endphp
        <label class="customcheck">Hiển thị
            <input type="hidden" name="status" value="0">
            <input class="form-check-input" {!! Helper::setChecked($status, 1) !!} name="status" type="checkbox" value="1"
                id="defaultCheck0">
            <span class="checkmark"></span>
        </label>
        <label class="customcheck">Khóa học mới
            <input type="hidden" name="new" value="0">
            <input class="form-check-input" {!! Helper::setChecked($new, 1) !!} name="new" type="checkbox" value="1"
                id="defaultCheck1">
            <span class="checkmark"></span>
        </label>

        <label class="customcheck">Khóa học đề xuất
            <input type="hidden" name="offer" value="0">
            <input class="form-check-input" {!! Helper::setChecked($offer, 1) !!} name="offer" type="checkbox" value="1"
                id="defaultCheck2">
            <span class="checkmark"></span>
        </label>

    </div>

</div>