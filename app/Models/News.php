<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = 'news';
    protected $fillable = [
        'title', 'thumbnail','news_category_id','content','priority','link','tag_ids','status'
    ];
    public function category()
    {
        return $this->belongsTo(CategoryNews::class,'news_category_id');
    }
    public function tag()
    {
        return $this->belongsToMany('App\Models\Tag','tag_ids');
    }
}
