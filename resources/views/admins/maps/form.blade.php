<div class="form-row">
  <div class="col mb-6">
    <div class="form-group {{ $errors->has('title') ? 'was-validated-custom' : 'was-validated-custom-valid' }}">
      <label for="title" class="control-label">Tiêu đề</label>
      <input type="text" class="form-control" id="title" name="title" value="{{ old('title', optional(optional($map))->title) }}" placeholder="Tiêu đề" required>
      {!! $errors->first('title', '<p class="invalid-feedback">:message</p>') !!}
    </div>
  </div>
  <div class="col mb-6">
    <div class="form-group">
      <label for="map_category_id" class="control-label">Thể loại:</label>
      <select required class="custom-select {{ $errors->has('map_category_id') ? 'is-invalid' : '' }}" name="map_category_id">
        <option value="">--Chọn thể loại điểm map--</option>
        @foreach($categories as $category)
        <option @if($category->id == old('map_category_id', optional(optional($map))->map_category_id))
          selected = "selected"
          @endif
          value="{{$category->id}}">{{$category->name}}</option>
        @endforeach
      </select>
      {!! $errors->first('map_category_id', '<p class="invalid-feedback">:message</p>') !!}
    </div>
  </div>
</div>
<div class="form-group">
  <div class="form-row">
    <div class="col">
      <label for="phone">Phone</label>
      <input type="number" class="form-control" id="phone" name="phone" value="{{ old('phone', optional(optional($map))->phone)}}" placeholder="Phone" required>
      {!! $errors->first('phone', '<p class="invalid-feedback">:message</p>') !!}
    </div>
    <div class="col">
      <label for="hotline">Hotline</label>
      <input type="number" class="form-control" id="hotline" name="hotline" value="{{ old('hotline', optional(optional($map))->hotline)}}" placeholder="Hotline">
      {!! $errors->first('hotline', '<p class="invalid-feedback">:message</p>') !!}
    </div>
  </div>
</div>
<div class="form-group {{ $errors->has('icon') ? 'was-validated-custom' : 'was-validated-custom-valid'}}">
  <label for="logo">Icon</label>
  <div class="col-md-3 block-single-upload">
    {!! Helper::uploadSingleImage( old('icon', optional($map)->icon), 'icon') !!}
    {!! $errors->first('icon', '<p class="invalid-feedback">:message</p>') !!}
  </div>

</div>
<br>
<div class="form-group">
  <label for="description">Mô tả</label>
  <textarea type="text" rows="3" class="form-control" id="description" name="description" placeholder="Mô tả" required>{{ old('description', optional(optional($map))->description)}}</textarea>
  {!! $errors->first('description', '<p class="invalid-feedback">:message</p>') !!}
</div>
<div class="form-group">
  <label for="location">Nhập vị trí</label>
  <input type="text" class="form-control col-md-7" id="searchTextField" name="location" value="{{ old('location', optional(optional($map))->location)}}" required>
  {!! $errors->first('location', '<p class="invalid-feedback">:message</p>') !!}
</div>
<div class="">
  <div class="">
    <div id="map"></div>
  </div>
  <input type="text" hidden class="form-control double" id="lat" name="latitude" value="{{ old('latitude', optional(optional($map))->latitude)}}">
  <input type="text" hidden class="form-control" id="long" name="longitude" value="{{ old('longitude', optional(optional($map))->longitude)}}">
</div>
<br>
@section('script')
<script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAZlHcoPe33iRtvrqFr1o_ZU50pV6JRVJs&callback=initMap&libraries=places&callback=initMap" defer></script>
<script type="text/javascript" src="{{ asset('admins/assets/scripts/location.js') }}"></script>
@endsection