<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\ApiController;
use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateStatusMapRequest;
use App\Http\Requests\User\AddLastPositionUserRequest;
use App\Http\Resources\Map\MapCollection;
use App\Http\Resources\Map\MapResource;
use App\Models\Map;
use App\Models\UserCategoryMap;
use App\Models\UserMap;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class MapController extends ApiController
{
    public function index(Request $request)
    {
        $userId = auth()->user()->id;
        $perPage = $request->get('per_page');
        $query = Map::orderBy('id', 'DESC');
        $userCategoryMap = UserCategoryMap::where('user_id',$userId)->where('status',2)->pluck('category_map_id');
        $query = $query->whereNotIn("map_category_id",$userCategoryMap);
        $perPage = $perPage ? $perPage : config('settings.countPaginate');
        $query = $query->latest()->paginate($perPage);
        return $this->formatJson(MapCollection::class, $query);
    }

    public function show($id)
    {
        $map = Map::findOrFail($id);
        return $this->formatJson(MapResource::class, $map);
    }

    public function addTheLastPosition(AddLastPositionUserRequest $request){
        try{
            $data = $request->getData();
            DB::beginTransaction();
            $user = auth()->user();
            $user = $user->update(['longitude' => $data['long'],'latitude'=> $data['lat']]);
            DB::commit();
            return $this->sendMessage('Cập nhật vị trí thánh công.'); 
        }catch(Exception $e){
            DB::rollBack();
            throw $e;
        }
        
    }
}
