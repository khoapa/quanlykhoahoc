<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MarkRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'mark.*' => 'required',
            'mark.*' => 'numeric|max:100|min:0',
        ];
    }
    public function messages(){
        $messages = [
            'mark.*.required' => 'Bạn chưa nhập điểm',
            'mark.*.numeric' => 'Bạn chưa nhập điểm',   
            'mark.*.max' => 'Điểm không đúng thang 100' ,  
            'mark.*.min' => 'Điểm không đúng thang 100' , 
        ];
        return $messages;
    }

    public function getData()
    {
        $data = $this->only('mark');
        return $data;
    }

}
