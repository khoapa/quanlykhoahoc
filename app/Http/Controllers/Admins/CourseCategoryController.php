<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\CreateCategoryCourseRequest;
use App\Models\CourseCategory;
use App\Http\Requests\UpdateCategoryCourseRequest;
use App\Models\Course;
use App\Services\UploadService;

class CourseCategoryController extends Controller
{
    public function index(Request $request){
        $name = $request->get('search');
        $query = CourseCategory::orderBy('id', 'DESC');
        if (!is_null($name)) {
            $query = $query->where('name', 'LIKE', '%' . $name . '%');
        }
        $categoryCourses = $query->paginate(config('settings.countPaginate'));
        return view('admins.category_course.index',compact('categoryCourses'));
    }
    public function create(){
        return view('admins.category_course.add');
    }
    public function store(CreateCategoryCourseRequest $request){
        $data = $request->getData();
        if ($request->hasFile('thumbnail')) {
            $file = $request->file('thumbnail');
            $data_logo = UploadService::uploadImage('category', $file);
            $data['thumbnail'] = $data_logo;
        }
        $insertData = CourseCategory::create($data);
	    if ($insertData) {
            return redirect()->route('category_course.index')->with('message',trans('category_course.create_success'));
	    }else {                        
            return redirect()->back()->with('message',trans('category_course.create_fail'));
	    }
    }
    public function edit($id){
        $categoryCourse = CourseCategory::findOrFail($id);
        return view('admins.category_course.edit',compact('categoryCourse'));
    }
    public function update(UpdateCategoryCourseRequest $request,$id){
        $categoryCourse = CourseCategory::findOrFail($id);
        $data = $request->getData();
        $data_logo= $categoryCourse->thumbnail;
        if ($request->hasFile('thumbnail')) {
            $file = $request->file('thumbnail');
            $data_logo = UploadService::uploadImage('category', $file);
        }
        $data['thumbnail'] = $data_logo;
        $update = $categoryCourse->update($data);
	    if ($update) {
            return redirect()->route('category_course.index')->with('message',trans('category_course.update_success'));
	    }else {                        
            return redirect()->back()->with('message',trans('category_course.update_fail'));
	    }

    }
    public function destroy($id){
        $courseCate = CourseCategory::findOrFail($id);
        $course = Course::where('course_category_id',$id)->delete();
        $delCate = $courseCate->delete();
        if ($delCate) {
            return redirect()->route('category_course.index')->with('message',trans('category_course.delete_success'));
	    }else {                        
            return redirect()->back()->with('message',trans('category_course.delete_fail'));
	    }
    }
    
}
