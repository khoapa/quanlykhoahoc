<?php

namespace App\Http\Resources\Map;

use App\Models\Map;
use App\Models\UserMap;
use Illuminate\Http\Resources\Json\JsonResource;

class MapResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->name,
            'map_category_id' => $this->map_category_id,
            'icon' => $this->icon,
            'description' => $this->description,
            'phone' => $this->phone,
            'hotline' => $this->hotline,
            'location' => $this->location,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'distance' => $this->distance()
        ];
    }
    public function distance()
    {
        $user = auth('api')->user();
        if (
            is_null($user->latitude) ||
            is_null($user->longitude) ||
            is_null($this->latitude) ||
            is_null($this->longitude)
        )
            return null;
            
        $latitudeFrom = $user->latitude;
        $longitudeFrom = $user->longitude;
        $latitudeTo = $this->latitude;
        $longitudeTo = $this->longitude;
        $theta = $longitudeFrom - $longitudeTo;
        $miles = (sin(deg2rad($latitudeFrom)) * sin(deg2rad($latitudeTo))) + (cos(deg2rad($latitudeFrom)) * cos(deg2rad($latitudeTo)) * cos(deg2rad($theta)));
        $miles = acos($miles);
        $miles = rad2deg($miles);
        $miles = $miles * 60 * 1.1515;
        // $feet = $miles * 5280;
        // $yards = $feet / 3;
        $kilometers = $miles * 1.609344;
        // $meters = $kilometers * 1000;

        return number_format((float)$kilometers, 3, '.', '');
    }
}
