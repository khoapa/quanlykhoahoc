<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateConversationRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user2_id' => 'required',
            'text' => 'nullable',
            'images' => 'required_without:text',
        ];
    }
    public function messages()
    {
        $messages = [ 
            'user2_id.required' => 'Tên người nhận là trường bắt buộc',  
            'images.required_without' => 'Hình ảnh là trường bắt buộc', 
        ];
        return $messages;
    }
    public function getData()
    {
        $data = $this->only(['user2_id','text','images']);
        return $data;
    }
}
