<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class UserCourse extends Model
{
    protected $table = 'user_courses';
    protected $fillable = [
    	'user_id','course_id','join_date','status','total_point','number_of_visits'
    ];
    public function student()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }
    public function course()
    {
        return $this->belongsTo(Course::class,'course_id','id');
    }
    public function courseResult(){
        return $this->hasMany(UserCourseResult::class,'user_course_id','id');
    }
    public function getCountPointChooseAttribute(){

        $coursePoint = $this->course->total_point_choice;
        if($coursePoint != 0){
            $courseResults =  $this->courseResult()->get();
            $total = 0;
            foreach($courseResults as $rs){
                $question = $rs->question()->first();
                if($question->type == 1){
                    $total += $rs->point;
                }
            }
            $point = $total*100/$coursePoint;
        }else{
            $point = 0;
        }
        return $point; 
      }    
      public function getCountPointEssayAttribute(){
        $coursePoint =$this->course->total_point - $this->course->total_point_choice;
        if($coursePoint != 0){
            $courseResults =  $this->courseResult()->get();
            $total = 0;
            foreach($courseResults as $rs){
                $question = $rs->question()->first();
                if($question->type == 2){
                    $total += $rs->point;
                }
            }
            $point = $total*100/$coursePoint;
        }else{
            $point = 0;
        }
        return $point; 
      }   
}
