<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use App\User;

class School extends Model
{
    protected $table = 'schools';
    protected $fillable = [
        'name', 'address', 'phone','email','logo','description','user_id','grade_ids'
    ];

    public $timestamps = true;

    public function classes()
    {
        return $this->hasMany(Classes::class);
    }
    public function grade()
    {
        return $this->hasMany('App\Models\Grade');
    }
    public function Scholl_Student()
    {
        return $this->hasMany('App\User');
    }
    public function staff(){
        return $this->belongsTo(User::class,'user_id','id');
    }
    public function course()
    {
        return $this->belongsToMany('App\Models\Course', 'App\Models\SchoolCourse')->withPivot('id');
    }
}
