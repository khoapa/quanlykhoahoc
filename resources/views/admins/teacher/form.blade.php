<div class="form-row mb-2">
    <div class="col-md-6 {{ $errors->has('email') ? 'was-validated-custom' : '' }}">
        <label for="firstname">Họ Tên</label>
        <input class="form-control" name="name" type="text" id="name" value="{{ old('name', optional(optional($teacher))->name) }}" placeholder="Nguyễn Văn A" maxlength="255" placeholder="" required>
        {!! $errors->first('name', '<p class="invalid-feedback">:message</p>') !!}
    </div>
    <div class="col-md-6 {{ $errors->has('birthday') ? 'was-validated-custom' : '' }}">
        <label for="firstname">Ngày sinh</label>
        @php
        $date = old('birthday', optional(optional($teacher))->birthday);
        @endphp
        <input class="form-control" name="birthday" type="text" id="datepicker" value="{!! Helper::formatDate1($date) !!}" placeholder="25-10-2004" maxlength="255" placeholder="" required>
        {!! $errors->first('birthday', '<p class="invalid-feedback">:message</p>') !!}
    </div>
</div>
<div class="form-row mb-2">
    <div class="col-md-6 {{ $errors->has('email') ? 'was-validated-custom' : '' }}">
        <label for="birthday">Email</label>
        <input class="form-control" name="email" type="email" id="email" value="{{ old('email', optional($teacher)->email) }}" maxlength="255" placeholder="" required>
        {!! $errors->first('email', '<p class="invalid-feedback">:message</p>') !!}
    </div>
    <div class="col-md-6 {{ $errors->has('email') ? 'was-validated-custom' : '' }}">
        <label for="address">Địa Chỉ</label>
        <input class="form-control" name="address" type="text" id="address" value="{{ old('address', optional(optional($teacher))->address) }}" placeholder="Địa Chỉ" maxlength="255" placeholder="" required>
        {!! $errors->first('address', '<p class="invalid-feedback">:message</p>') !!}
    </div>
</div>
<div class="form-row mb-2">
    <div class="col-md-6">
        <div class="form-group">
            <label for="sel1">Giới Tính:</label>
            <select class="custom-select {{ $errors->has('gender') ? 'is-invalid' : '' }}" id="gender" name="gender">
                @php
                $gender = old('gender', optional(optional($teacher))->gender)
                @endphp
                <option value="">Giới Tính</option>
                <option value="1" {!! Helper::setSelected($gender, 1) !!}>Nam</option>
                <option value="2" {!! Helper::setSelected($gender, 2) !!}>Nữ</option>
            </select>
            {!! $errors->first('gender', '<p class="invalid-feedback">:message</p>') !!}
        </div>
    </div>
    <div class="col-md-6 {{ $errors->has('phone') ? 'was-validated-custom' : '' }}">
        <label for="phone">Số Điện thoại</label>
        <input class="form-control" name="phone" type="number" id="phone" value="{{ old('phone', optional(optional($teacher))->phone) }}" placeholder="09795000000" maxlength="255" placeholder="" required>
        {!! $errors->first('phone', '<p class="invalid-feedback">:message</p>') !!}
    </div>
</div>
<div class="form-row mb-2">
    <div class="col-md-6">
        <div class="form-group ">
            <label for="sel1">Trường:</label>
            <select class="custom-select {{ $errors->has('school_id') ? 'is-invalid' : '' }}" id="school_id1" name="school_id">
                <option value="">Trường</option>
                @foreach($schools as $valueSchool)
                <option value="{{$valueSchool->id}}" @if($valueSchool->id == old('school_id',
                    optional(optional($teacher))->school_id))
                    selected = "selected"
                    @endif>{{$valueSchool->name}}</option>
                @endforeach
            </select>
            {!! $errors->first('school_id', '<p class="invalid-feedback">:message</p>') !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="sel1">Lớp:</label>
            <select class="custom-select {{ $errors->has('class_id') ? 'is-invalid' : '' }}" id="class_id" name="class_id">
                <option value="">--Lớp--</option>
                @foreach($classes as $valueClass)
                <option value="{{$valueClass->id}}" @if($valueClass->id == old('class_id',
                    optional(optional($teacher))->class_id))
                    selected = "selected"
                    @endif>{{$valueClass->name}}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>
<div class="form-row mb-2">
<div class="col-md-6">
        <div class="form-group {{ $errors->has('role_id') ? 'was-validated ' : '' }}">
            <label for="sel1">Chức vụ</label>
            <select class="custom-select {{ $errors->has('role_id') ? 'is-invalid' : '' }}" id="role_id"
                name="role_id">
                @php
                $roleId = null;
                if($teacher){
                    $role =  optional(optional($teacher))->role()->first();
                    $roleId = $role ?  $role->role_id : null;
                }
                @endphp
                <option value="" >--Chức vụ--</option>
                @foreach($roles as $role)
                <option value="{{$role->id}}"
                    @if($role->id == old('role_id',$roleId))
                    selected = "selected"
                    @endif
                    >{{$role->description}}</option>
                @endforeach
            </select>
            {!! $errors->first('role_id', '<p class="invalid-feedback">:message</p>') !!}
        </div>
    </div>
</div>
<div class="form-group mb-2">
    <label for="avatar" class="control-label">Hình ảnh(*)</label>
    <div class="col-md-3 block-single-upload">
        {!! Helper::uploadSingleImage( old('avatar', optional($teacher)->avatar), 'avatar') !!}
        {!! $errors->first('avatar', '<p class="invalid-feedback">:message</p>') !!}
    </div>
</div>