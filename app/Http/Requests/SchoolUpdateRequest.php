<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SchoolUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=> 'required',
            'address' => 'required',
            'phone' => 'required',
            'logo' => 'nullable|mimes:jpeg,png,jpg,gif,svg|max:5000',
            'email' => 'required|email'
        ];
    }
    public function messages(){
        $messages = [
            'email.email'=> 'Email phải đúng định dạng',
            'email.required' => 'Email là trường bắt buộc',
            'logo.mimes'=> 'Logo không đúng định dạng',
            'logo.max' => 'Logo không vượt quá 5Mb',
            'name.required' => 'Name School là trường bắt buộc',
            'address.required' => 'Địa chỉ là trường bắt buộc',
            'phone.required' => 'Phone là trường bắt buộc',
            
        ];
        return $messages;
    }
    public function getData()
    {
        $data = $this->only(['role_id','name','address','logo','phone','email','description']);
        return $data;
    }
}
