<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NewsCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=> 'required|max:1024',
            'content'=> 'required',
            'thumbnail' => 'nullable|mimes:jpeg,png,jpg,gif,svg|max:5012',
            'news_category_id' => 'required',
        ];
    }
    public function messages(){
        $messages = [
            'title.max' => 'Tiêu đề không vượt quá 1024 kí tự',
            'news_category_id.required' => 'Thể loại là trường bắt buộc',
            'thumbnail.mimes'=>'Ảnh phải đúng định dạng',
            'thumbnail.max' => 'Ảnh không vượt quá 5Mb',
            'title.required' => 'Tiêu đề là trường bắt buộc',
            'content.required' => 'Nội dung là trường bắt buộc',       
        ];
        return $messages;
    }
    public function getData()
    {
        $data = $this->only(['title','thumbnail','news_category_id','content','priority','link','tag_ids']);
        return $data;
    }
}
