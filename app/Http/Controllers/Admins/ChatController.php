<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use LRedis;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\ApiController;
use function GuzzleHttp\json_decode;
use App\Models\Message;
use App\Models\Conversation;
use App\Http\Requests\UpdateConversationRequest;
use App\Http\Resources\MessageCollection;
use App\Http\Resources\ConversionCollection;
use App\Http\Resources\MessageResource;
use App\Http\Resources\ConversionResource;
use App\Services\UploadService;
use File;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Input;
use App\User;

class ChatController extends Controller
{
    public function index($id = null){
        $user = Auth::user();
        $listConversionsFirst = Conversation::where('user1_id',$user->id)->orderBy('id','DESC')->get();
        $listConversionsTwo = Conversation::where('user2_id',$user->id)->orderBy('id','DESC')->get();
        $messages = null;
        $user2Id = null;
        $conversonId = null;
        if(!is_null($id)){
            $converson = Conversation::findOrFail($id);
            if(is_null($converson)){
                $user2Id = $id;
            }else{
                $messages = $converson->message()->get();
                $user1Id = $converson->user1_id;
                $idUser2 = $converson->user2_id;
                $user2Id = ($user1Id == $user->id) ? $idUser2 : $user1Id; 
                $conversonId = $id;  
            }

        }
        $nameChat = "";
        if(!is_null($user2Id)){
            $nameChat  = User::findOrFail($user2Id)->name;
        }

        return view('admins.chat.index',compact('listConversionsFirst','listConversionsTwo','messages','conversonId','user2Id','nameChat'));
    }
    public function store(Request $request){
         $user = auth()->user();
         $text = $_POST['text'];
         $image = $_POST['image'];
         $conversonId = $_POST['conversion_id'];
         $user2Id = $_POST['user2_id'];
         $data = [
             'text' => $text,
             'user2_id' => $user2Id,
             'images' => $image,
         ];
         if(!is_null($conversonId)){
            $converson = Conversation::findOrFail($conversonId);
            $data['sender_id'] = $user->id;
            $data['conversation_id'] = $converson->id;
            $createMessage = Message::create($data);
        }else{
            $dataConversition['user1_id'] = $user->id;
            $dataConversition['user2_id'] = $data['user2_id'];
            $converson = Conversation::create($dataConversition);
            $data['sender_id'] = $user->id;
            $data['conversation_id'] = $converson->id;
            $createMessage = Message::create($data);
            $this->pushSocketListConversation($dataConversition['user1_id'], $converson);
            $this->pushSocketListConversation($dataConversition['user2_id'], $converson);
        }
        $this->pushSocket($converson->id, $createMessage);
        echo json_encode($data);
    }
    protected function pushSocket($id, $message) 
    {
        $redis = LRedis::connection();
        $redis->publish('conversation-'.$id, json_encode(new MessageResource($message)));
    }

    protected function pushSocketListConversation($id, $conversation) {
        $redis = LRedis::connection();
        $redis->publish('list-conversation-'.$id, json_encode(new ConversionResource($conversation)));
    }
    public function upload(Request $request){
        $file = $request->file('file');
        $imageUrl = UploadService::uploadImage('code',$file);
         echo $imageUrl;
    }
    public function deleteConversion($id){
        $converson = Conversation::findOrFail($id);
        $converson->delete();
        return redirect()->route('chat.chat.index')->with('message','Xóa hội thoại thành công.');
    }
    public function deleteMessage($id){
        $message = Message::findOrFail($id);
        $countMessage = Message::where('conversation_id',$message->conversation_id)->get()->count();
        if($countMessage == 0){
            $converson = Conversation::findOrFail($message->conversation_id);
            $converson->delete();
        }
        $message->delete();
        echo json_encode(array('success'=>true, 'message' => 'Xóa tin nhắn thành công.'));
    }
}
