<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Enums\StatusUserCourseEnum;
use App\Enums\UserRole;

class CourseDetailResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    public function toArray($request)
    {
        $questionChoice = $this->question()->where('type',1)->get();
        $questionEssay = $this->question()->where('type',2)->get();
        $idUser = auth()->user()->id;
        $statusUserCourse = $this->userCourse()->where('user_id',$idUser)->first();
        $statusFavourite = $this->userFavourite()->where('user_id',$idUser)->first();
        return [
            'id' => $this->id,
            'name'=> $this->name,
            'cover_image' => $this->cover_image,
            'question_choice' => QuestionResource::collection($questionChoice->shuffle()),
            'questionEssay' => QuestionResource::collection($questionEssay),
            'status_user' => is_null($statusUserCourse) ? null : $statusUserCourse->pivot->status,
            'status_user_lable' => is_null($statusUserCourse) ? 'Tham gia' : StatusUserCourseEnum::getDescription($statusUserCourse->pivot->status),
            'status_favourite' => is_null($statusFavourite) ? 0 : 1,
        ];
    }
    
}
