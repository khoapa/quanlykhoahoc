@extends('admin')
@section('title','Create News Category')
@section('content')
<div class="app-page-title app-page-title-custom">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-plus icon-gradient bg-happy-itmeo">
                </i>
            </div>
            <div>
                <h5>Thêm Mới Thể Loại Tin Tức</h5>
            </div>
        </div>
    </div>
</div>
<div class="main-card card main-card-custom">
    <div class="card-body">
        <form enctype="multipart/form-data" method="POST" novalidate="" action="{{route('news_category_create')}}" accept-charset="UTF-8" id="create_exhibition_form" name="create_exhibition_form" class="form-horizontal {{ $errors->any() ? 'was-validated' : '' }}">
            {{csrf_field()}}
            @include ('admins.news_category.form', [
            'category' => null,
            ])
            <div class="form-group">
                    <a href="{{ route('news_category_index') }}" class="mb-2 mr-2 btn-icon btn-pill btn btn-outline-secondary"><i class="pe-7s-back btn-icon-wrapper"> </i>Trở lại</a>
                    <button class="mb-2 mr-2 btn-icon btn-pill btn btn-outline-success"><i class="pe-7s-check btn-icon-wrapper"> </i>Tạo mới</button>
            </div>
        </form>
    </div>
</div>
</div>
@endsection