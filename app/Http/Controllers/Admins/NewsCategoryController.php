<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryNewsCreateRequest;
use App\Http\Requests\CategoryNewsUpdateRequest;
use App\Models\CategoryNews;
use Illuminate\Http\Request;

class NewsCategoryController extends Controller
{
    public function index(Request $request)
    {
        $name = $request->get('search');
        $query = CategoryNews::orderBy('id', 'DESC');
        if (!is_null($name)) {
            $query = $query->where('name', 'LIKE', '%' . $name . '%');
        }
        $categories = $query->paginate(config('settings.countPaginate'));
        return view('admins.news_category.index',compact('categories'));
    }
    public function create()
    {
        return view('admins.news_category.create');
    }
    public function store(CategoryNewsCreateRequest $request)   
    {
        $data = $request->getData();
        CategoryNews::create($data);
        return redirect()->route('news_category_index')
            ->with('message', 'Tạo thể loại tin tức thành công');
    }
    public function edit($id)
    {
        $category = CategoryNews::find($id);
        return view('admins.news_category.edit',compact('category'));
    }
    public function update(CategoryNewsUpdateRequest $request, $id)
    {   
        $category = CategoryNews::find($id);
        $data = $request->getData();
        $category->update($data);
        return redirect()->route('news_category_index')->with('message', 'Update thể loại tin tức thành công');
    }
    public function delete($id)
    {
        $category = CategoryNews::find($id);
        $category->delete();
        return redirect()->route('news_category_index')->with('message', 'Xóa thể loại tin tức thành công');
    }
}
