<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Models\School;
use App\Http\Resources\ListSchoolResource;

class SchoolController extends ApiController
{
    public function index(Request $request){
        $searchText = $request->get('search_text');
        $query = School::orderBy('id','ASC');
        if(!is_null($searchText)){
            $query = $query->where('name','LIKE','%'.$searchText.'%');
        }
        $schools = $query->get();
        return $this->formatCollectionJson(ListSchoolResource::class,$schools);
    }
}
