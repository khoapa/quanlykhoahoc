<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ChangePassVerifyRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function rules()
    {
        return [
            'data_verify' => [
                'required', function ($attribute, $value, $fail) {
                    if (!is_null($value)) {
                        if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
                            if (!preg_match('/^[0-9]{10}+$/', $value))
                            return $fail("Email hoặc số điện thoại không đúng định dạng.");
                        }
                    }
                }
            ],
            'verify_code' => 'required',
            'password' => 'required|confirmed',
            'password_confirmation' => 'required',
        ];
    }
    public function messages()
    {
        $messages = [
            'data_verify.required' => 'Email hoặc số điện thoại là trường bắt buộc', 
            'verify_code.required' => 'Mã xác thực là trường bắt buộc.',
            'password.required' => 'Mật khẩu là trường bắt buộc', 
            'password.confirmed' =>'Mật khẩu xác nhận không khớp với mật khẩu đã nhập',
            'password_confirmation.required' => 'Mật khẩu xác nhận là trường bắt buộc',
        ];
        return $messages;
    }
    public function getData()
    {
        $data = $this->only(['verify_code', 'phone','password']);
        return $data;
    }
}
