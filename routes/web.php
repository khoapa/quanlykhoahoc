<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group([
    'namespace' => 'Admins',
    'middleware' =>'auth'
], function () {
    Route::get('/', 'HomeController@index')->name('home.index');
});
Route::group(['prefix' => 'admin','middleware' =>'auth'], function () {

    Route::group([
        'prefix' => 'school',
        'namespace' => 'Admins',
    ], function () {
        Route::get('/', 'SchoolController@index')->name('school_index')->middleware('permission:view_school');
        Route::get('/create', 'SchoolController@create')->name('school_create')->middleware('permission:create_school');
        Route::post('/create', 'SchoolController@store');
        Route::get('/delete/{id}', 'SchoolController@delete')->name('school_delete')->middleware('permission:delete_school');
        Route::get('/edit/{id}', 'SchoolController@edit')->name('school_edit')->middleware('permission:edit_school');
        Route::post('/update/{id}', 'SchoolController@update')->name('school_update');
        Route::get('/class/{id}', 'ClassController@listClass')->name('list_class');
        Route::get('class/create/{id}', 'SchoolController@createClass')->name('school_create.class')->middleware('permission:create_class');
        Route::post('class/create', 'SchoolController@storeClass')->name('school_store.class');
        Route::get('class/edit/{id}', 'SchoolController@editClass')->name('school_edit.class');
        Route::post('class/update/{id}', 'SchoolController@updateClass')->name('school_update.class');
    });
    Route::group([
        'prefix' => 'class',
        'namespace' => 'Admins',
    ], function () {
        Route::get('/', 'ClassController@index')->name('class_index')->middleware('permission:view_class');
        Route::get('/create', 'ClassController@create')->name('class_create')->middleware('permission:create_class');
        Route::post('/create', 'ClassController@store');
        Route::get('/delete/{id}', 'ClassController@delete')->name('class_delete')->middleware('permission:delete_class');
        Route::get('/edit/{id}', 'ClassController@edit')->name('class_edit')->middleware('permission:edit_class');
        Route::post('/update/{id}', 'ClassController@update')->name('class_update');
        Route::get('/students/{id}/{school_id?}', 'ClassController@listStudent')->name('list_student');
        Route::get('/student/create/{id}', 'ClassController@createStudent')->name('class_create.student');
        Route::post('/student/create', 'ClassController@storeStudent')->name('class_store.student');
        Route::get('/student/edit/{id}', 'ClassController@editStudent')->name('class_edit.student');
        Route::post('/student/update/{id}', 'ClassController@updateStudent')->name('class_update.student');
    });
    Route::group([
        'prefix' => 'student',
        'namespace' => 'Admins',
    ], function () {
        // student
        Route::get('/', 'StudentController@index')->name('student.index')->middleware('permission:view_student');
        Route::get('/create', 'StudentController@create')->name('student.create')->middleware('permission:create_student');
        Route::post('student-store', 'StudentController@store')->name('student.store');
        Route::get('/edit/{id}', 'StudentController@edit')->name('student.edit')->middleware('permission:edit_student');
        Route::post('/update/{id}', 'StudentController@update')->name('student.update');
        Route::get('/destroy/{id}', 'StudentController@destroy')->name('student.destroy')->middleware('permission:delete_student');
        //ajax
        Route::get('/search-class', 'StudentController@searchClassAjax');
        Route::get('/update-status', 'StudentController@updateStatusAjax');
    });
    Route::group([
        'prefix' => 'teacher',
        'namespace' => 'Admins',
    ], function () {

        //teacher
        Route::get('/', 'TeacherController@index')->name('teacher.index')->middleware('permission:view_teacher');
        Route::get('/create', 'TeacherController@create')->name('teacher.create')->middleware('permission:create_teacher');
        Route::post('/store', 'TeacherController@store')->name('teacher.store');
        Route::get('/edit/{id}', 'TeacherController@edit')->name('teacher.edit')->middleware('permission:edit_teacher');
        Route::post('/update/{id}', 'TeacherController@update')->name('teacher.update');
        Route::get('/destroy/{id}', 'TeacherController@destroy')->name('teacher.destroy')->middleware('permission:delete_teacher');
        Route::get('/list-teacher/{id}', 'TeacherController@listTeacher');


    });
    Route::group([
        'prefix' => 'map',
        'namespace' => 'Admins',
    ], function () {
        Route::get('', 'MapController@index')->name('map_index')->middleware('permission:view_map');
        Route::get('/create', 'MapController@create')->name('map_create')->middleware('permission:create_map');
        Route::post('/create', 'MapController@store');
        Route::get('/delete/{id}', 'MapController@delete')->name('map_delete')->middleware('permission:delete_map');
        Route::get('/edit/{id}', 'MapController@edit')->name('map_edit')->middleware('permission:edit_map');
        Route::post('/update/{id}', 'MapController@update')->name('map_update');
    });
    Route::group([
        'prefix' => 'refer',
        'namespace' => 'Admins',
    ], function () {

        //refer
        Route::get('/', 'ReferController@index')->name('refer.index');
        Route::get('/create', 'ReferController@create')->name('refer.create');
        Route::post('/store', 'ReferController@store')->name('refer.store');
        Route::get('/edit/{id}', 'ReferController@edit')->name('refer.edit');
        Route::post('/update/{id}', 'ReferController@update')->name('refer.update');
        Route::get('/destroy/{id}', 'ReferController@destroy')->name('refer.destroy');
    });
    Route::group([
        'prefix' => 'grades',
        'namespace' => 'Admins',
    ], function () {
        Route::get('', 'GradeController@index')->name('grade_index')->middleware('permission:view_grade');
        Route::get('/create', 'GradeController@create')->name('grade_create')->middleware('permission:create_grade');
        Route::post('/create', 'GradeController@store');
        Route::get('/delete/{id}', 'GradeController@delete')->name('grade_delete')->middleware('permission:delete_grade');
        Route::get('/edit/{id}', 'GradeController@edit')->name('grade_edit')->middleware('permission:edit_grade');
        Route::post('/update/{id}', 'GradeController@update')->name('grade_update');
        Route::get('/list-grade/{id}', 'GradeController@listGradeOfSchool');
    });
    Route::group([
        'prefix' => 'map/category',
        'namespace' => 'Admins',
    ], function () {
        Route::get('', 'MapCategoryController@index')->name('map_category_index')->middleware('permission:view_category_map');
        Route::get('/create', 'MapCategoryController@create')->name('map_category_create')->middleware('permission:create_category_map');
        Route::post('/create', 'MapCategoryController@store');
        Route::get('/delete/{id}', 'MapCategoryController@delete')->name('map_category_delete')->middleware('permission:edit_category_map');
        Route::get('/edit/{id}', 'MapCategoryController@edit')->name('map_category_edit')->middleware('permission:delete_category_map');
        Route::post('/update/{id}', 'MapCategoryController@update')->name('map_category_update');
    });

    Route::group([
        'prefix' => 'news',
        'namespace' => 'Admins',
    ], function () {
        Route::get('','NewsController@index')->name('news_index')->middleware('permission:view_new');
        Route::get('/create','NewsController@create')->name('news_create')->middleware('permission:create_new');
        Route::post('/create','NewsController@store');
        Route::get('/delete/{id}','NewsController@delete')->name('news_delete')->middleware('permission:edit_new');
        Route::get('/edit/{id}','NewsController@edit')->name('news_edit')->middleware('permission:delete_new');
        Route::post('/update/{id}','NewsController@update')->name('news_update');
        Route::get('/update-status', 'NewsController@updateStatusAjax');
    });

    Route::group([
        'prefix' => 'news/category',
        'namespace' => 'Admins',
    ], function () {
        Route::get('', 'NewsCategoryController@index')->name('news_category_index')->middleware('permission:view_category_new');
        Route::get('/create', 'NewsCategoryController@create')->name('news_category_create')->middleware('permission:create_category_new');
        Route::post('/create', 'NewsCategoryController@store');
        Route::get('/delete/{id}', 'NewsCategoryController@delete')->name('news_category_delete')->middleware('permission:delete_category_new');
        Route::get('/edit/{id}', 'NewsCategoryController@edit')->name('news_category_edit')->middleware('permission:edit_category_new');
        Route::post('/update/{id}', 'NewsCategoryController@update')->name('news_category_update');
    });

    Route::group([
        'prefix' => 'supports',
        'namespace' => 'Admins',
    ], function () {

        Route::get('/', 'SupportsController@index')->name('supports.index');
        Route::get('/create', 'SupportsController@create')->name('supports.create');
        Route::post('/store', 'SupportsController@store')->name('supports.store');
        Route::get('/edit/{id}', 'SupportsController@edit')->name('supports.edit');
        Route::post('/update/{id}', 'SupportsController@update')->name('supports.update');
        Route::get('/destroy/{id}', 'SupportsController@destroy')->name('supports.destroy');
    });
    Route::group([
        'prefix' => 'transcript',
        'namespace' => 'Admins',
    ], function () {

        Route::get('/', 'TranscriptController@index')->name('transcript.index');
        Route::get('search-user','TranscriptController@searchUser')->name('transcript.user');
        Route::get('/delete-user/{id}','TranscriptController@deleteUser')->name('transcript.user.delete');
        Route::get('/mark/{user_id}/{course_id}', 'TranscriptController@mark')->name('transcript.mark');
        Route::post('/mark/store/{id}', 'TranscriptController@markStore')->name('transcript.mark.store');
        Route::get('/search-class', 'TranscriptController@searchClassAjax');
        Route::get('/search-course', 'TranscriptController@searchCourseAjax');
    });
    Route::group([
        'prefix' => 'school/course',
        'namespace' => 'Admins',
    ], function () {

        Route::get('/', 'SchoolCourseController@index')->name('school_course.index');
        Route::get('/create/{id}', 'SchoolCourseController@create')->name('school_course.create');
        Route::post('/store/{id}', 'SchoolCourseController@store')->name('school_course.store');
        Route::get('/edit/{id}', 'SchoolCourseController@edit')->name('school_course.edit');
        Route::post('/update/{id}', 'SchoolCourseController@update')->name('school_course.update');
        Route::get('/destroy/{id}', 'SchoolCourseController@destroy')->name('school_course.destroy');

        Route::get('/class-create/{id}', 'SchoolCourseController@createClass')->name('school_course.createClass');
        Route::post('/class-store/{id}', 'SchoolCourseController@storeClass')->name('school_course.storeClass');
        Route::get('/class-destroy/{id}', 'SchoolCourseController@destroyClass')->name('school_course.destroyClass');
    });
    Route::group([
        'prefix' => 'Web/service',
        'namespace' => 'Admins',
    ], function () {

        Route::get('/edit', 'ServiceController@edit')->name('service.edit');
        Route::post('/update/{id}', 'ServiceController@update')->name('service.update');
      
    });

    Route::group([
        'prefix' => 'user',
        'namespace' => 'Admins',
    ], function () {
        // user
        Route::get('/', 'UserController@index')->name('user.index');
        Route::get('/create', 'UserController@create')->name('user.create');
        Route::post('user-store', 'UserController@store')->name('user.store');
        Route::get('/edit/{id}', 'UserController@edit')->name('user.edit');
        Route::post('/update/{id}', 'UserController@update')->name('user.update');
        Route::get('/destroy/{id}', 'UserController@destroy')->name('user.destroy');
    });

});
