<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MapCategory extends Model
{
    protected $table = 'map_category';
    protected $fillable = [
        'icon', 'name'
    ];
    public function map()
    {
        return $this->hasMany(Map::class,"map_category_id","id");
    }
}
