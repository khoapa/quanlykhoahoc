<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Classes extends Model
{
    protected $table = 'class';
    protected $fillable = [
        'name', 'thumbnail', 'grade_id','school_id','user_id',
    ];
    public $timestamps = true;
    public function school()
    {
        return $this->belongsTo('App\Models\School');
    }
    public function grade()
    {
        return $this->belongsTo('App\Models\Grade');
    }
    public function teacher()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }
    public function course()
    {
        return $this->belongsToMany('App\Models\Course','App\Models\ClassCourse')->withPivot('id');
    }
}
