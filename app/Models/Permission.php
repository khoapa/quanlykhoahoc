<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Permission as SpatiePermission;

class Permission extends SpatiePermission
{
  public $guard_name = 'web';
  protected $table = "permissions";
  public function userRoles(){
    return $this->belongsToMany(Role::class, 'role_has_permissions', 'role_id','permission_id');
}
}
