<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use App\Http\Requests\MarkRequest;
use App\Http\Requests\TranscriptRequest;
use App\Http\Resources\CategoryCourseResource;
use App\Http\Resources\UserResource;
use App\Models\Classes;
use App\Models\Course;
use App\Models\CourseCategory;
use App\Models\Question;
use App\Models\School;
use App\Models\UserCourse;
use App\Models\UserCourseResult;
use App\User;
use Illuminate\Http\Request;

class TranscriptController extends Controller
{
    public function index()
    {
        $schools = School::get();
        $categoryCourses = CourseCategory::get();
        return view('admins.transcripts.index', compact('schools', 'categoryCourses'));
    }
    public function searchUser(TranscriptRequest $request)
    {
        $schools = School::get();
        $categoryCourses = CourseCategory::get();
        $school_id = $request->school;
        $class_id = $request->class;
        $course_id = $request->course;
        $category_id = $request->category;
        $class = Classes::findOrFail($class_id);
        $course = Course::findOrFail($course_id);
        $users = $course->userCourse()->where('class_id', $class_id)->get();
        $userCourses = UserCourse::where('course_id', $course_id)->whereHas('student', function ($query) use ($class_id) {
            $query = $query->where('class_id', $class_id);
        })->get();
        $classes = Classes::where('school_id', $school_id)->get();
        $courses = Course::where('course_category_id', $category_id)->get();


        return view('admins.transcripts.transcript', compact('users', 'schools', 'categoryCourses', 'school_id', 'class', 'course', 'category_id', 'classes', 'courses', 'userCourses'));
    }
    public function mark($user_id, $course_id)
    {
        $course = Course::findOrFail($course_id);
        $countQuestionEssay = $course->question->where('type', 2)->count();
        $percentageChoice = $course->total_point_choice;
        $percentageEssay = 100 - $percentageChoice;
        if ($countQuestionEssay >= 1) {
            $pointOfOneQuestionsEssay = $percentageEssay / $countQuestionEssay;
        };
        $userCourse = UserCourse::where([
            ['user_id', $user_id],
            ['course_id', $course_id]
        ])->first();
        $questionsChoose = $userCourse->courseResult()->whereHas('question', function ($query) {
            $query->where('type', 1);
        })->get();
        $sumPointChoose = 0;
        foreach ($questionsChoose as $value) {
            $sumPointChoose = $sumPointChoose + $value->point;
        };
        if ($percentageChoice != 0) {
            $sumPointChoose = round($sumPointChoose * 100 / $percentageChoice, 2);
        } else {
            $sumPointChoose = 0;
        }
        $questionsEssay = $userCourse->courseResult()->whereHas('question', function ($query) {
            $query->where('type', 2);
        })->get();
        foreach ($questionsEssay as $value) {
            $value->point100 = round($value->point * 100 / $pointOfOneQuestionsEssay, 0);
        };
        $sumPointEssay = 0;
        foreach ($questionsEssay as $value) {
            if (!is_null($value->point)) {
                $sumPointEssay = $sumPointEssay + $value->point;
            }
        };
        if ($sumPointEssay != 0) {
            $sumPointEssay = round($sumPointEssay * 100 / $percentageEssay, 2);
        } else {
            $sumPointEssay = 0;
        }
        $totalPoint = ($sumPointEssay * $percentageEssay + $sumPointChoose * $percentageChoice) / 100;
        return view('admins.transcripts.mark', compact('questionsChoose', 'questionsEssay', 'sumPointChoose', 'sumPointEssay', 'totalPoint', 'percentageChoice'));
    }
    public function markStore(MarkRequest $request, $id)
    {
        $course = Course::findOrFail($id);
        $countQuestionEssay = $course->question->where('type', 2)->count();
        $percentageChoice = $course->total_point_choice;
        $percentageEssay = 100 - $percentageChoice;

        if ($countQuestionEssay >= 1) {
            $pointOfOneQuestionsEssay = $percentageEssay / $countQuestionEssay;
        };
        $data = $request->getData();

        foreach ($data as $data) {
            foreach ($data as $id => $mark) {
                $pointEssay = round(($mark / 100) * $pointOfOneQuestionsEssay, 2);
                UserCourseResult::where('id', $id)->update(['point' => $pointEssay]);
            }
        };
        return redirect()->back()->with('message', 'Chấm điểm thành công');
    }
    public function deleteUser($id)
    {
        $userCourse = UserCourse::findOrFail($id);
        $userCourse->delete();
        return redirect()->route('transcript.index')->with('message', 'Xóa người học thành công');
    }
    public function searchClassAjax(Request $request)
    {
        if ($request->ajax()) {
            $classes = Classes::where('school_id', '=', $request->id_School)->get();
            return response()->json($classes);
        }
    }
    public function searchCourseAjax(Request $request)
    {
        if ($request->ajax()) {
            $courses = Course::where('course_category_id', '=', $request->id_category)->get();
            return response()->json($courses);
        }
    }
}
