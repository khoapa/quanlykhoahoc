<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Enums\StatusUserCourseEnum;

class CourseAllCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public $collects = CourseAllResource::class;
    public function toArray($request)
    {
        return $this->collection;
    }
}
