<?php

namespace App\Http\Controllers\Admins;

use App\Enums\UserTypeEnum;
use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\News;
use Illuminate\Http\Request;
use App\User;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        $students = User::where("user_type", '=', UserTypeEnum::STUDENT)->count();
        $news = News::count();
        $courses = Course::count();
        $teachers =User::with('school', 'classes')->where("user_type",UserTypeEnum::TEACHER)->whereNull('position_id')->orderBy('id', 'DESC')->paginate(config('settings.countPaginateHome'));
        $listNews = News::orderBy('id', 'DESC')->paginate(config('settings.countPaginateHome'));
        $listCourses = Course::orderBy('id','DESC')->where('new',1)->paginate(config('settings.countPaginateHome'));
        return view('admins.dashboard.index',compact('courses','students','news','teachers','listNews','listCourses'));
    }
}
