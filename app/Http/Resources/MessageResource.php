<?php

namespace App\Http\Resources;

use App\Helpers\Helper;
use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;

class MessageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'sender_id' => $this->sender_id,
            'avatar' => $this->sender->avatar,
            'sender' => $this->sender->name,
            'text' => $this->text,
            'images' => $this->images,
            'time' => Helper::formatDateDiffForHumans($this->created_at)
        ];
    }
}
