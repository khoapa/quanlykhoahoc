<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;
class Course extends Model
{
    use SoftDeletes;
    protected $table = 'courses';
    protected $fillable = [
        'name','course_category_id','cover_image','start_date','longtime','status','new','offer','total_point','total_point_choice','author'
    ];
    public $timestamps = true;

    public function category(){
        return $this->belongsTo(CourseCategory::class,'course_category_id','id');
    }
    public function question(){
        return $this->hasMany(Question::class,'course_id','id');
    }
    public function userCourse()
    {
        return $this->belongsToMany(User::class, 'user_courses','course_id','user_id')->withPivot('join_date','status','total_point');
    }
    public function userFavourite()
    {
        return $this->belongsToMany(User::class, 'course_favorites','course_id','user_id');
    }
    public function school()
    {
        return $this->belongsToMany(School::class, 'school_course', 'course_id','school_id');
    }
    public function class()
    {
        return $this->belongsToMany(Classes::class,'App\Models\ClassCourse');
    }
    public function authorUser(){
        return $this->belongsTo(User::class,'author','id');
    }
}
