@extends('admin')
@section('content')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-car icon-gradient bg-mean-fruit">
                    </i>
                </div>
                <div>
                    KẾT QUẢ HỌC TẬP<br>
                    <h8 class="text">Bảng điểm và chấm điểm</h8>
                </div>
            </div>

        </div>
    </div>
    @if(Session::has('message'))
    <div class="alert alert-success">
        <span class="glyphicon glyphicon-ok"></span>
        {!! session('message') !!}
        <button type="button" class="close" data-dismiss="alert" aria-label="close">
            <span aria-hidden="true">&times;</span>
        </button>

    </div>
    @endif
    <div class="col-md-12 pl-0">
        <div class="mb-3 card">
            <div class="card-body">
                <div class="tab-content">
                    <form enctype="multipart/form-data" method="get" novalidate="" action="{{route('transcript.user')}}" accept-charset="UTF-8" id="create_exhibition_form" name="create_exhibition_form" class="form-horizontal {{ $errors->any() ? 'was-validated' : '' }}">
                        {{ csrf_field() }}
                        <div class="form-row">
                            <div class="col mb-3">
                                <h6 class="timeline-title">Chọn lớp :</h6>
                            </div>
                            <div class="col mb-3">
                                <div class="form-group">
                                    <select name="school" id="school_id" required class="custom-select {{ $errors->has('school') ? 'is-invalid' : '' }}">
                                        <option value="">--Trường--</option>
                                        @foreach($schools as $school)    
                                        <option @if($school->id == old('school'))
                                            selected = "selected"
                                            @endif
                                            value="{{$school->id}}">{{$school->name}}</option>
                                        @endforeach
                                    </select>
                                    {!! $errors->first('school', '<p class="invalid-feedback">:message</p>') !!}
                                </div>
                            </div>
                            <div class="col mb-3">
                                <div class="form-group">
                                    <select name="class" id="class_id" required class="custom-select {{ $errors->has('class') ? 'is-invalid' : '' }}">
                                        <option value="">--Lớp--</option>
                                    </select>
                                    {!! $errors->first('class', '<p class="invalid-feedback">:message</p>') !!}
                                </div>
                            </div>
                            <div class="col mb-3">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col mb-3">
                                <h6 class="timeline-title">Và chọn khóa học cần xem :</h6>
                            </div>
                            <div class="col mb-3">
                                <div class="form-group">
                                    <select name="category" id="category_id" required class="custom-select {{ $errors->has('category') ? 'is-invalid' : '' }}">
                                        <option value="">--Danh mục khóa học--</option>
                                        @foreach($categoryCourses as $category)
                                        <option @if($category->id == old('category'))
                                            selected = "selected"
                                            @endif
                                            value="{{$category->id}}">{{$category->name}}</option>
                                        @endforeach
                                    </select>
                                    {!! $errors->first('category', '<p class="invalid-feedback">:message</p>') !!}
                                </div>
                            </div>
                            <div class="col mb-3">
                                <div class="form-group">
                                    <select name="course" id="course_id" required class="custom-select {{ $errors->has('course') ? 'is-invalid' : '' }}">
                                        <option value="">--Khóa học--</option>
                                    </select>
                                    {!! $errors->first('course', '<p class="invalid-feedback">:message</p>') !!}
                                </div>
                            </div>
                            <div class="col mb-3">
                                <button class="mb-2 mr-2 btn-icon btn-pill btn btn-outline-success"><i class="pe-7s-check btn-icon-wrapper"> </i>Xem bảng điểm</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection