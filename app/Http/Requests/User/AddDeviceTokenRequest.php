<?php

namespace App\Http\Requests\User;

use App\Http\Requests\ApiRequest;
use Illuminate\Foundation\Http\FormRequest;

class AddDeviceTokenRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'device_token' => 'required'
        ];
    }
    public function messages()
    {
        $messages = [ 
            'device_token.required' => 'Mã thông báo thiết bị là trường bắt buộc',  
        ];
        return $messages;
    }
    public function getData()
    {
        $data = $this->only(['device_token']);
        return $data;
    }
}
