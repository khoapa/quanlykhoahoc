var map;
var markers = [];

function initMap() {
    var locations = new Array();
    let students = document.getElementById('studentsMap').value;
    let arrstudent = JSON.parse(students);
    for (var i = 0; i < arrstudent.length; i++) {
        arr = [arrstudent[i]['name'], arrstudent[i]['school'], arrstudent[i]['long'], arrstudent[i]['lat'], i];
        if (arrstudent[i]['long'] != null || arrstudent[i]['lat'] != null) {
            locations.push(arr)
        }
    }
    console.log(locations);


    window.map = new google.maps.Map(document.getElementById('mapstudent'), {
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var bounds = new google.maps.LatLngBounds();

    for (i = 0; i < locations.length; i++) {
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[i][2], locations[i][3]),
            map: map
        });

        bounds.extend(marker.position);

        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                var html = "<div class='maps_popover-content'><h4>" + locations[i][0] + "</h4><span>" + locations[i][1] + "</span></div>";
                infowindow.setContent(html);
                infowindow.open(map, marker);
            }
        })(marker, i));
    }

    map.fitBounds(bounds);

    var listener = google.maps.event.addListener(map, "click", function() {
        map.setZoom(15);
        google.maps.event.removeListener(listener);
    });
    // var map = new google.maps.Map(document.getElementById('mapstudent'), {
    //     zoom: 15,
    //     center: new google.maps.LatLng(18.5248904, 73.7228789),
    //     mapTypeId: google.maps.MapTypeId.ROADMAP
    // });

    // var infowindow = new google.maps.InfoWindow();

    // var marker, i;

    // for (i = 0; i < locationArray.length; i++) {
    //     marker = new google.maps.Marker({
    //         position: new google.maps.LatLng(locationArray[i][1], locationArray[i][2]),
    //         map: map
    //     });

    //     google.maps.event.addListener(marker, 'click', (function(marker, i) {
    //         return function() {
    //             infowindow.setContent(locationArray[i][0]);
    //             infowindow.open(map, marker);
    //         }
    //     })(marker, i));
    // }



}