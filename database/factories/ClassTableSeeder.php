<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

use App\Models\Classes;

$factory->define(App\Models\Classes::class, function (Faker $faker) {
    return [
        'name'=>$faker->name,
        'grade_id' => rand(1,12),
        'school_id' => rand(1,10),
    ];
});
