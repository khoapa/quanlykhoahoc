<?php

namespace App\Http\Controllers\Api\v1;

use App\Enums\UserTypeEnum;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Http\Requests\Conversation\CreateNewConversationRequest;
use App\Http\Requests\CreateConversationRequest;
use App\Models\Conversation;
use App\Models\Message;
use App\Http\Requests\UpdateConversationRequest;
use App\Http\Resources\Conversation\CreateNewConversationResource;
use App\Http\Resources\MessageCollection;
use App\Http\Resources\ConversionCollection;
use App\Http\Resources\MessageResource;
use App\Http\Resources\ConversionResource;
use LRedis;
use App\Http\Resources\ListConversationCollection;
use App\User;
use Exception;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class ConversationController extends ApiController
{
    use \App\Traits\NotificationTrait;
    public function index(Request $request)
    {
        $user = auth('api')->user();
        $perPage = $request->get('per_page');
        $perPage = $perPage ? $perPage : config('settings.countPaginate');
        $users = User::where('id', '!=', $user->id);
        if ($user->admin_flg != 1) {            //if is't admin
            $users = $users->where('school_id', $user->school_id);
            $user_school_id = $user->school()->firstOrFail()->user_id;          //school
            if ($user->id == $user_school_id) {       //if is school
                $users = $users->whereIn('user_type', [UserTypeEnum::TEACHER, UserTypeEnum::STUDENT]);
            } else {
                if ($user->user_type == UserTypeEnum::STUDENT) { //if is student
                    $homeroom_teacher_id = $user->classes()->firstOrFail()->user_id;    //teacher
                    $users = $users->whereIn('user_type', [UserTypeEnum::STAFF])->orWhere('id', $homeroom_teacher_id)->orWhere('admin_flg', 1);
                }
                if ($user->user_type == UserTypeEnum::TEACHER) { //if is teacher
                    if (!is_null($user->class_id))
                        $users = $users->whereIn('user_type', [UserTypeEnum::STAFF, UserTypeEnum::STUDENT])->where('class_id', $user->class_id);
                    else
                        $users = $users->whereIn('user_type', [UserTypeEnum::STAFF]);
                }
            }
        }
        try{
            DB::beginTransaction();
            foreach ($users->get() as $userCreate) {
                $dataConversition['user1_id'] = $user->id;
                $dataConversition['user2_id'] = $userCreate->id;
                $conversation = Conversation::whereIn('user1_id', $dataConversition)->whereIn('user2_id', $dataConversition)->first();
                if (!$conversation)
                    $conversation[] = Conversation::create($dataConversition);
            }
            DB::commit();
        }catch(Exception $e){
            DB::rollBack();
            throw $e;
        }
        
        $conversation = Conversation::where('user1_id', $user->id)->orWhere('user2_id', $user->id)->orderBy('updated_at', 'DESC');
        $conversation = $conversation->latest()->paginate($perPage);
        return $this->formatJson(ListConversationCollection::class, $conversation);
    }
    public function list(Request $request, $id)
    {
        $perPage = $request->get('per_page');
        $perPage = $perPage ? $perPage : config('settings.countPaginate');
        $messages = Message::where('conversation_id', $id)->orderBy('id', 'DESC')->paginate($perPage);
        return $this->formatJson(MessageCollection::class, $messages);
    }
    public function store(CreateConversationRequest $request)
    {
        $data = $request->getData();
        $user = auth('api')->user();
        $dataUser = [$data['user2_id'], $user->id];
        $receiver = User::findOrFail($data['user2_id']);
        $conversasion = Conversation::whereIn('user1_id', $dataUser)->whereIn('user2_id', $dataUser)->first();
        if (!is_null($data['text']))
            $content = $data['text'];
        else $content = "";
        if ($conversasion) {
            $data['sender_id'] = $user->id;
            $data['conversation_id'] = $conversasion->id;
            $createMessage = Message::create($data);
            $this->pushSocketListConversation($conversasion->user1_id, $conversasion);
            $this->pushSocketListConversation($conversasion->user2_id, $conversasion);
            $title_notification = '' . $user->name . ' đã gửi tin nhắn cho bạn.';
            $this->notifcationSend($title_notification, $content, $user, $receiver);
        } else {
            $dataConversition['user1_id'] = $user->id;
            $dataConversition['user2_id'] = $data['user2_id'];
            $conversasion = Conversation::create($dataConversition);
            $data['sender_id'] = $user->id;
            $data['conversation_id'] = $conversasion->id;
            $createMessage = Message::create($data);
            $this->pushSocketListConversation($dataConversition['user1_id'], $conversasion);
            $this->pushSocketListConversation($dataConversition['user2_id'], $conversasion);

            $title_notification = '' . $user->name . ' đã gửi tin nhắn cho bạn.';
            $this->notifcationSend($title_notification, $content, $user, $receiver);
        }
        $conversasion->update(["updated_at" => Carbon::now()]);
        $this->pushSocket($conversasion->id, $createMessage);
        return $this->formatJson(MessageResource::class, $createMessage);
    }

    public function createConversation(CreateNewConversationRequest $request)
    {
        $data = $request->getData();
        $user = auth('api')->user();
        $dataUser = [$data['user2_id'], $user->id];
        $receiver = User::findOrFail($data['user2_id']);
        $converson = Conversation::whereIn('user1_id', $dataUser)->whereIn('user2_id', $dataUser)->first();
        if (!$converson) {
            $dataConversition['user1_id'] = $user->id;
            $dataConversition['user2_id'] = $data['user2_id'];
            $converson = Conversation::create($dataConversition);
        }
        return $this->formatJson(CreateNewConversationResource::class, $converson, $receiver);
    }

    public function update(UpdateConversationRequest $request, $id)
    {
        $data = $request->getData();
        $message = Message::findOrFail($id);
        $message->update($data);
        $this->pushSocket($id, $message);
        return $this->formatJson(MessageResource::class, $message);
    }
    public function delete($id)
    {
        $message = Message::findOrFail($id);
        $message->delete();
        return $this->sendMessage('Xóa tin nhắn thành công.', 200);
    }
    public function deleteConversion($id)
    {
        $conversation = Conversation::findOrFail($id);
        $conversation->delete();
        return $this->sendMessage('Xóa hội thoại thành công.', 200);
    }

    protected function pushSocket($id, $message)
    {
        $redis = LRedis::connection();
        $redis->publish('conversation-' . $id, json_encode(new MessageResource($message)));
    }

    protected function pushSocketListConversation($id, $conversation)
    {
        $redis = LRedis::connection();
        $redis->publish('list-conversation-' . $id, json_encode(new ConversionResource($conversation)));
    }
}
