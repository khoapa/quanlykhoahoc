<div class="form-group">
    <div class="form-row mb-2">
        <div class="col-md-12">
            <label for="firstname">Câu trả lời</label>
            <div>
                <textarea value="{{ old('answer', optional(optional($answer))->answer) }}"
                    class="form-control" id="total_point_choice" rows="6" name="answer" required>{{ old('answer', optional(optional($answer))->answer) }}</textarea>
                {!! $errors->first('answer', '<p class="invalid-feedback">:message</p>') !!}
            </div>
        </div>
    </div>
</div>
<div class="form-group">
    @php
    $correct = old('correct', optional(optional($answer))->correct);
    @endphp
    <label>Đáp án đúng</label>
    <label class="customcheck">Đáp án đúng
        <input type="hidden" name="correct" value="0">
        <input class="form-check-input" {!! Helper::setChecked($correct, 1) !!} name="correct" type="checkbox" value="1"
            id="defaultCheck1">
        <span class="checkmark"></span>
    </label>
</div>
</div>