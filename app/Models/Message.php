<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Message extends Model
{
    protected $table = 'messages';
    protected $fillable = [
        'id','sender_id','conversation_id','text','images','created_at','updated_at'
    ];
    public function sender(){
        return $this->belongsTo(User::class,'sender_id','id');
    }
}
