<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryMapUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=> 'required|max:255',
            'icon' => 'nullable|mimes:jpeg,png,jpg,gif,svg|max:5012',
        ];
    }
    public function messages(){
        $messages = [
            'name.required' => 'Thể loại điểm map là trường bắt buộc',
            'icon.mimes'=>'Icon phải đúng định dạng',
            'icon.max' => 'Icon không vượt quá 5Mb',
            'name.max' => 'Thể loại tin tức không vượt quá 255 kí tự',
        ];
        return $messages;
        
    }
    public function getData()
    {
        $data = $this->only(['name','icon']);
        return $data;
    }
}
