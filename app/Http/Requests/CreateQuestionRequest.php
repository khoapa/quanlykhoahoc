<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateQuestionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        return [
            'course_id'  => 'required',
            'type' => 'required',
            'subject' =>'required',
        ];
    }
    public function messages()
    {
        $messages = [ 
            'course_id.required' => 'Khóa học là trường bắt buộc', 
            'type.required' => 'Loại câu hỏi là trường bắt buộc', 
            'point.required' => 'Số điểm là trường bắt buộc', 
            'subject.required' => 'Tên câu hỏi là trường bắt buộc',  
        ];
        return $messages;
    }
    public function getData()
    {
        $data = $this->only([ 'course_id','type','subject','multiselect','point','rediret_answer']);
        return $data;
    }
}
