<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use App\Http\Requests\ClassCreateRequest;
use App\Http\Requests\ClassUpdateRequest;
use App\Models\Classes;
use App\Models\Grade;
use App\Models\School;
use App\User;
use Illuminate\Http\Request;
use App\Services\UploadService;
use App\Enums\UserTypeEnum;
use App\Helpers\Helper;
use App\Http\Requests\StudentRequest;
use App\Http\Requests\StudentUpdateRequest;
use App\Models\Role;
use Illuminate\Support\Facades\Hash;

class ClassController extends Controller
{
    public function index(Request $request)
    {
        $name = $request->get('search');
        $class = Classes::orderBy('id', 'ASC');
        $schoolID = $request->get('school_id');
        $gradeID = $request->get('grade_id');
        $user = auth()->user();
        if ($user->user_type == UserTypeEnum::TEACHER || $user->user_type == UserTypeEnum::STAFF || $user->admin_flg == 1) {
            $grades = [];
            $class = Classes::orderBy('id', 'ASC');

            if (!is_null($name)) {
                $class = $class->where('name', 'LIKE', '%' . $name . '%');
            }
            $schools = [];
            $grades = [];
            if($user->user_type != UserTypeEnum::TEACHER){
            if (!is_null($schoolID)) {
                $class = $class->where('school_id', '=', $schoolID);
                $gradesIds = School::findOrFail($schoolID)->grade_ids;
                $gradesArray = json_decode($gradesIds);
                if (!is_null($gradesArray)) {
                    foreach ($gradesArray as $key => $value) {
                        $grades[$key]['id'] = $value;
                        $grades[$key]['name'] = Grade::findOrFail($value)->name;
                    };
                }
            }
            $schools = School::get();
            if (!is_null($gradeID)) {
                $class = $class->where('grade_id', $gradeID);
            }}
            if ($user->user_type == UserTypeEnum::STAFF) {
                $school_id = $user->school_id;
                $class = $class->where('school_id', $school_id);
            }
            if ($user->user_type == UserTypeEnum::TEACHER && $user->class_id) {
                $class_id = $user->class_id;
                $class = $class->where('id', $class_id);
            }
            if (!is_null($name)) {
                $class = $class->where('name', 'LIKE', '%' . $name . '%');
            }
            $classes = $class->paginate(config('settings.countPaginate'));
            return view('admins.classes.index', compact('classes', 'schools', 'grades'));
        }
        return view('errors.403');
    }
    public function create()
    {
        $schools = School::get();
        $grades = Grade::get();
        $teachers = User::where('user_type', UserTypeEnum::TEACHER)->get();
        return view('admins.classes.create', compact('schools', 'grades', 'teachers'));
    }
    public function store(ClassCreateRequest $request)
    {
        $data = $request->getData();
        if ($request->hasFile('thumbnail')) {
            $file = $request->file('thumbnail');
            $data_image = UploadService::uploadImage('class', $file);
            $data['thumbnail'] = $data_image;
        }
        $insertData = Classes::create($data);
        if ($insertData) {
            return redirect()->route('class_index')->with('message', 'Tạo lớp học thành công');
        } else {
            return redirect()->back()->with('message', 'Tạo lớp học thất bại');
        }
    }
    public function delete($id)
    {
        $class = Classes::findOrFail($id);
        $class->delete();
        return redirect()->route('class_index')->with('message', 'Xóa lớp học thành công');
    }
    public function edit($id)
    {
        $class = Classes::find($id);
        $idSchoolOfClass = $class->school->id;
        $gradesIds = School::findOrFail($idSchoolOfClass)->grade_ids;
        $gradesArray = json_decode($gradesIds);
        $gradeOfSchool = [];
        if (!is_null($gradesArray)) {
            foreach ($gradesArray as $key => $value) {
                $gradeOfSchool[$key]['id'] = $value;
                $gradeOfSchool[$key]['name'] = Grade::findOrFail($value)->name;
            };
        }
        $schools = School::get();
        $teachers = User::join('model_has_roles', function ($join) {
            $join->on('users.id', '=', 'model_has_roles.model_id');
        })
            ->where('school_id', $class->school_id)->where('user_type', UserTypeEnum::TEACHER)
            ->where('model_has_roles.role_id', 3)->get();
        return view('admins.classes.edit', compact('class', 'schools', 'gradeOfSchool', 'teachers'));
    }
    public function update(ClassUpdateRequest $request, $id)
    {
        $class = Classes::find($id);
        $data = $request->getData();
        $data_image = $class->thumbnail;
        if ($request->hasFile('thumbnail')) {
            $file = $request->file('thumbnail');
            $data_image = UploadService::uploadImage('class', $file);
        }
        $data['thumbnail'] = $data_image;
        $update = $class->update($data);
        if ($update) {
            return redirect()->route('class_index')->with('message', 'Đã cập nhật lớp học thành công.');
        } else {
            return redirect()->route('class_index')->with('message', 'Đã có lỗi xảy ra trong quá trình cập nhật. Vui lòng thử lại.');
        }
    }
    public function listClass($id)
    {
        $classes = Classes::where('school_id', '=', $id)->paginate(config('settings.countPaginate'));
        $school = School::find($id);
        return view('admins.schools.list_class', compact('classes', 'school'));
    }
    public function listStudent($id, $idSchool = null)
    {
        $query = User::where('class_id', '=', $id);
        if (!is_null($idSchool)) {
            $query = $query->where('school_id', $idSchool);
        }
        $students = $query->paginate(config('settings.countPaginate'));
        $class = Classes::find($id);
        return view('admins.classes.list_students', compact('students', 'class'));
    }
    public function createStudent($id)
    {
        $class = Classes::find($id);
        $schools = School::get();
        $classes = Classes::get();
        $roles = Role::where('name', '!=', 'Admin')->get();
        return view('admins.classes.create_students', compact('schools', 'classes', 'roles', 'class'));
    }
    public function storeStudent(StudentRequest $request)
    {
        $data = $request->getData();
        $data['birthday'] = Helper::formatSqlDate($data['birthday']);
        $data['password'] = Hash::make(config('settings.password'));
        $data['user_type'] = UserTypeEnum::STUDENT;
        $data_image = config('settings.image');
        if ($request->hasFile('avatar')) {
            $file = $request->file('avatar');
            $data_image = UploadService::uploadImage('student', $file);
        }
        $data['avatar'] = $data_image;
        try {
            $userCreate = User::create($data);
            $roleId = $data['role_id'];
            if (!is_null($roleId)) {
                $userCreate->syncRoles([$roleId]);
            }
            return redirect()->route('list_student', [$userCreate->class_id, $userCreate->school_id])->with('message',  'Thêm mới học sinh thành công');
        } catch (\Exception $e) {
            return redirect()->back()->with('message', 'Thêm mới học sinh không thành công');
        }
    }
    public function editStudent($id)
    {
        $student = User::findOrFail($id);
        $schools = School::get();
        $classes = Classes::get();
        $roles = Role::where('name', '!=', 'Admin')->get();
        return view('admins.classes.edit_student', compact('student', 'schools', 'classes', 'roles'));
    }
    public function updateStudent(StudentUpdateRequest $request, $id)
    {

        $student = User::findOrFail($id);
        $classId =  $student->class_id;
        $schoolId =  $student->school_id;
        $data = $request->getData();
        $data['birthday'] = Helper::formatSqlDate($data['birthday']);
        $data['user_type'] = UserTypeEnum::STUDENT;
        if ($request->has('avatar')) {
            $file = $request->file('avatar');
            $data_image = UploadService::uploadImage('student', $file);
            $data['avatar'] = $data_image;
        } else {
            $data['avatar'] = $student->avatar;
        }
        $roleId = $data['role_id'];
        $roleOld = $student->role()->first();
        if (!is_null($roleId)) {
            $student->syncRoles([$roleId]);
        } else {
            if ($roleOld) {
                $student->removeRole($roleOld->role_id);
            }
        }
        try {
            $student->update($data);
            return redirect()->route('list_student', [$classId, $schoolId])->with('message', 'Đã cập nhật học sinh thành công.');
        } catch (\Exception $e) {
            return redirect()->back()->with('message', 'Đã có lỗi xảy ra trong quá trình cập nhật. Vui lòng thử lại.');
        }
    }
    public function classSchool($id)
    {
        $class = Classes::where('school_id', $id)->get();
        return response()->json($class);
    }
}
