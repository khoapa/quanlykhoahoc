<div class="app-sidebar__inner">
    <ul class="vertical-nav-menu">
        <li class="app-sidebar__heading">Dashboards</li>
        <li>
            <a href="{{route('home.index')}}">
                <i class="metismenu-icon pe-7s-culture"></i>
                HOME
            </a>
        </li>
        <li class="app-sidebar__heading">Quản lý user</li>
        <li>
            <a class="" href="{{route('user.index')}}">
                <i class="metismenu-icon fa fa-user-circle">
                </i>Tất cả User
            </a>
        </li>
        <li>
            <a class="" href="{{route('student.index')}}">
                <i class="metismenu-icon fa fa-user-circle">
                </i>Học sinh
            </a>
        </li>
        <li>
            <a class="" href="{{route('teacher.index')}}">
                <i class="metismenu-icon fa fa-user">
                </i>Giáo viên
            </a>
        </li>
        <li>
            <a class="" href="{{route('tracking.tracking.index')}}">
                <i class="metismenu-icon pe-7s-note2">
                </i>Tracking học sinh
            </a>
        </li>
        <li class="app-sidebar__heading">Quản lý hệ thống</li>
        <li>
            <a class="" href="{{route('class_index')}}">
                <i class="metismenu-icon fa fa-list-ul">
                </i>Lớp
            </a>
        </li>
        <li>
            <a class="" href="{{route('grade_index')}}">
                <i class="metismenu-icon pe-7s-note2">
                </i>Khối
            </a>
        </li>
        <li>
            <a class="" href="{{route('school_index')}}">
                <i class="metismenu-icon pe-7s-note2">
                </i>Trường
            </a>
        </li>
        <li class="app-sidebar__heading">Khóa học</li>
        <li>
            <a href="{{route('category_course.index')}}">
                <i class="metismenu-icon fa fa-barcode">
                </i>Danh mục khóa học
            </a>
        </li>
        <li>
            <a href="{{route('course.index')}}">
                <i class="metismenu-icon fa fa-window-restore">
                </i>Khóa học
            </a>
        </li>
        <li>
            <a href="{{route('school_course.index')}}">
                <i class="metismenu-icon fa fa-window-restore">
                </i>Chương trình học
            </a>
        </li>
        <li>
            <a href="{{route('transcript.index')}}">
                <i class="metismenu-icon fa fa-window-restore">
                </i>Kết quả học tập
            </a>
        </li>
        <li class="app-sidebar__heading">Tin Tức</li>
        <li>
            <a href="{{route('news_category_index')}}">
                <i class="metismenu-icon fa fa-folder-open "></i>Danh mục tin tức
            </a>
        </li>
        <li>
            <a href="{{route('news_index')}}">
                <i class="metismenu-icon pe-7s-note2 "></i>Tin Tức
            </a>
        </li>
        <li class="app-sidebar__heading">Hỗ Trợ</li>
        <li>
            <a href="{{route('supports.index')}}">
                <i class="metismenu-icon fa fa-folder-open"></i>Danh mục hỗ trợ
            </a>
        </li>
        <li class="app-sidebar__heading">Quản Lý Map</li>
        <li>
            <a href="{{route('map_category_index')}}">
                <i class="metismenu-icon fa fa-folder-open "></i>Danh mục điểm map
            </a>
        </li>
        <li>
            <a href="{{route('map_index')}}">
                <i class="metismenu-icon fa fa-map-marker  "></i>Điểm Map
            </a>
        </li>
        <li class="app-sidebar__heading">Quản Lý Khác</li>
        <!-- <li>
            <a href="{{route('map_index')}}">
                <i class="metismenu-icon fa fa-map"></i>Bản đồ
            </a>
        </li> -->
        <li>
            <a href="{{route('refer.index')}}">
                <i class="metismenu-icon fa fa-folder-open"></i>Danh mục tham khảo
            </a>
        </li>
        <li>
            <a href="{{route('service.edit')}}">
                <i class="metismenu-icon fa fa-folder-open"></i>Web service
            </a>
        </li>
        <li>
            <a href="{{route('chat.chat.index')}}">
                <i class="metismenu-icon fa fa-folder-open"></i>Chat
            </a>
        </li>
        <li>
            <a href="#">
                <i class="metismenu-icon fa fa-folder-open"></i>
                Settings
                <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
            </a>
            <ul>
                <li>
                    <a href="{{route('permission.permission.index')}}">
                        <i class="metismenu-icon fa fa-folder-open"></i>Phân quyền
                    </a>
                </li>
            </ul>
        </li>

    </ul>
</div>