<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Enums\UserTypeEnum;
use App\Models\School;
use App\Models\Classes;

class TrackingController extends Controller
{
    public function index(Request $request){
        $schoolId = $request->get('school_id');
        $classId = $request->get('class_id');
        $name = $request->get('name');
        $query = User::where('user_type',UserTypeEnum::STUDENT);
        $schools = School::get();
        $classes = Classes::get(); 
        if(!is_null($name)){
            $query = $query->where('name','LIKE','%'.$name.'%');
        }
        if(!is_null($classId)){
            $query = $query->where('school_id','=',$schoolId);
        }
        if(!is_null($classId)){
            $query = $query->where('class_id','=',$classId);
        }
        $students = $query->get();
        $arr = [];
        foreach($students as $student){
            $data = [
                'name' => $student->name,
                'school' => $student->school->name.'-'.$student->school->name,
                'long' => $student->longitude,
                'lat' => $student->latitude
            ];
            array_push($arr,$data);
        }
        $arrs = json_encode($arr);
        return view('admins.tracking.index',compact('arrs','schools','classes'));
    }
}
