@extends('admin')
@section('content')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-car icon-gradient bg-mean-fruit">
                    </i>
                </div>
                <div>
                    DANH SÁCH LỚP HỌC CỦA TRƯỜNG {{$school->name}}
                </div>
            </div>
            <div class="page-title-actions">
                <a href="{{route('school_create.class',['id'=>$school->id])}}" class="mb-2 mr-2 btn-icon btn btn-success" href=""><i class="mr-1 pe-7s-plus btn-icon-wrapper">
                    </i>Thêm mới lớp học</a>
            </div>
        </div>
    </div>
    <div class="card mb-3">
        <div class="main-card card">
            <div class="panel-body panel-body-with-table">
                <div class="table-responsive">
                    <table class="table table-striped ">
                        <thead>
                            <tr role="row">
                                <th>STT</th>
                                <th>Hình ảnh</th>
                                <th>Tên lớp</th>
                                <th>Khối</th>
                                <th>Trường</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($classes as $row)
                            <tr role="row" class="odd">
                                <td>{!! Helper::indexPaginate($classes,$loop) !!}</td>
                                <td class="image-cate">
                                    <img class="rounded-circle" src="{{$row->thumbnail}}" alt="">
                                </td>
                                <td>{{$row->name}}</td>
                                <td>{{optional($row->grade)->name}}</td>
                                <td>{{optional($row->school)->name}}</td>
                                <td>
                                    <div class=" w-100">
                                        <a href="{{route('list_student',[$row->id,$row->school->id])}}" class="mb-2 mr-2 btn-icon btn-pill btn btn-outline-success">DS Học Sinh</a>
                                        <a type="submit" class="pull-right mb-2 mr-2 btn-icon btn-icon-only btn-shadow btn-dashed btn btn-outline-warning " href="{{route('class_delete',['id'=>$row->id])}}" title="Xóa" onclick="return confirm('Bạn muốn xóa ?')">
                                            <span class="pe-7s-trash" aria-hidden="true"></span>
                                        </a>
                                        <a href="{{route('school_edit.class',['id'=>$row->id])}}" class="pull-right mb-2 mr-2 btn-icon btn-icon-only btn-shadow btn-dashed btn btn-outline-primary" title="Sửa">
                                            <span class="pe-7s-tools" aria-hidden="true"></span>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                    </table>
                </div>
            </div>
        </div>
        <div class="panel-footer">
            <div class="col-12">
                <div class="pagination pull-right">
                    {{ $classes->links()}}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection