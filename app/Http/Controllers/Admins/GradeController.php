<?php

namespace App\Http\Controllers\Admins;

use App\Enums\UserTypeEnum;
use App\Http\Controllers\Controller;
use App\Http\Requests\GradeCreateRequest;
use App\Http\Requests\GradeUpdateRequest;
use App\Models\Grade;
use App\Models\School;
use Illuminate\Http\Request;
use App\Services\UploadService;

class GradeController extends Controller
{
    public function index(Request $request)
    {
        $name = $request->get('search');
        $user = auth()->user();
        if ($user->user_type == UserTypeEnum::STAFF || $user->admin_flg == 1) {
            $grade = Grade::orderBy('id', 'ASC');
            if ($user->user_type == UserTypeEnum::STAFF) {
                $school_id = $user->school_id;
                $school = School::findOrFail($school_id);
                $grade_ids = $school->grade_ids;
                $grade = $grade->whereIn('id', json_decode($grade_ids));
            }
            if (!is_null($name)) {
                $grade = $grade->where('name', 'LIKE', '%' . $name . '%');
            }
            $grades = $grade->paginate(config('settings.countPaginate'));
            return view('admins.grades.index', compact('grades'));
        }
        return view('errors.403');
    }
    public function create()
    {
        return view('admins.grades.create');
    }

    public function store(GradeCreateRequest $request)
    {
        $data = $request->getData();
        if ($request->hasFile('thumbnail')) {
            $file = $request->file('thumbnail');
            $data_image = UploadService::uploadImage('grade', $file);
            $data['thumbnail'] = $data_image;
        }
        $insertData = Grade::create($data);
        if ($insertData) {
            return redirect()->route('grade_index')->with('message', 'Tạo khối học thành công');
        } else {
            return redirect()->back()->with('message', 'Tạo khối học thất bại');
        }
    }
    public function delete($id)
    {
        $grade = Grade::findOrFail($id);
        $grade->delete();
        return redirect()->route('grade_index')->with('message', 'Xóa khối học thành công');
    }
    public function edit($id)
    {
        $grade = Grade::find($id);
        return view('admins.grades.edit', compact('grade'));
    }
    public function update(GradeUpdateRequest $request, $id)
    {
        $grade = Grade::find($id);
        $data = $request->all();
        $thumbnailCurrent = $grade->thumbnail;

        if ($request->hasFile('thumbnail')) {
            $thumbnail = $request->file('thumbnail');
            $getThumbnail = time() . '_' . $thumbnail->getClientOriginalName();
            $destinationPath = public_path('uploads/thumbnail');
            $thumbnail->move($destinationPath, $getThumbnail);
            $url = 'uploads/thumbnail/' . $getThumbnail;
            $data['thumbnail'] = $url;
        } else {
            $data['thumbnail'] = $thumbnailCurrent;
        }
        $grade->update($data);
        if ($grade->update($data)) {
            return redirect()->route('grade_index')->with('message', 'Đã cập nhật khối học thành công.');
        } else {
            return redirect()->route('grade_index')->with('message', 'Đã có lỗi xảy ra trong quá trình cập nhật. Vui lòng thử lại.');
        }
    }

    public function listGradeOfSchool($id)
    {
        $gradesIds = School::findOrFail($id)->grade_ids;
        $gradesArray = json_decode($gradesIds);
        $grade = [];
        if (!is_null($gradesArray)) {
            foreach ($gradesArray as $key => $value) {
                $grade[$key]['id'] = $value;
                $grade[$key]['name'] = Grade::findOrFail($value)->name;
            };
        }
        return response()->json($grade);
    }
}
