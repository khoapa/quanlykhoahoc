<div class="form-row">
    <div class="col mb-6">
        <div class="form-group {{ $errors->has('name') ? 'was-validated-custom' : 'was-validated-custom-valid' }}">
            <label for="name">Thể loại tin tức</label>
            <input type="text" class="form-control" id="name" name="name" value="{{ old('name', optional(optional($category))->name) }}" placeholder="Thể loại tin tức" required>
            {!! $errors->first('name', '<p class="invalid-feedback">:message</p>') !!}
        </div>
    </div>
    <div class="col mb-6">
    </div>
</div>