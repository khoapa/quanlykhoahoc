$(document).on('change', '#type', function() {
    var value = document.getElementById("type").value;
    if (value == 2) {
        $("#form-select-multi").css("display", "none");
        $("#rediret-answer").css("display", "none");
        document.getElementById("multiselect").value = 0;
    } else {
        $("#form-select-multi").css("display", "block");
        $("#rediret-answer").css("display", "-webkit-inline-box");
    }
});


$(document).ready(function() {
    var value = document.getElementById("type").value;
    if (value == 2) {
        $("#form-select-multi").css("display", "none");
        $("#rediret-answer").css("display", "none");
        document.getElementById("multiselect").value = 0;
    }
});
$(document).on('click', '.add-answer', function() {
    let preview = '<div class="form-group"><label for="description" class="control-label">Câu trả lời</label>' +
        '<textarea rows="4" class="form-control" rows="6" name="answer[]" required></textarea></div>' +
        '<div class="form-group"><label>Đáp án đúng</label><label class="customcheck">Đáp án đúng' +
        '<input type="hidden" name="correct[]" type="checkbox" value="0">' +
        '<input class="form-check-input" name="correct[]" type="checkbox" value="1">' +
        '<span class="checkmark"></span> </label></div>';
    $('#add-answer').append(preview);

    return false;

});
$(document).ready(function() {
    $('#rediret-answer').on('click', function() {
        $('#inp-rediret-answer').val(1);
        $('form').trigger('submit');
        return false;
    });
});

$(document).on('change', '#course_name', function() {
    $('#form-search').submit();
});
$('#course_id_disable').on('mousedown', function(e) {
    e.preventDefault();
    this.blur();
    window.focus();
});
$('#total_point_choice').on('change', function(e) {
    let pointChoose = document.getElementById('total_point_choice').value;
    if (pointChoose > 100 || pointChoose < 0) {
        pointChoose = 100;
    }
    document.getElementById("total_point_essay").value = 100 - parseInt(pointChoose);
});

function checkAll(id) {
    console.log($('#all-' + id).val())
    if ($('#all-' + id).val() == 0) {
        $('.all-' + id).prop('checked', true);
        $('#all-' + id).prop('checked', true);
        $('#all-' + id).val(1);
    } else {
        $('.all-' + id).prop('checked', false);
        $('#all-' + id).prop('checked', false);
        $('#all-' + id).val(0);
    }
    // var checkboxes = document.querySelectorAll('.permi-' + id);
    // $this = $(this)
    // for (var i = 0; i < checkboxes.length; i++) {
    //     console.log($this);
    //     if (checkboxes[i] != $this)
    //         checkboxes[i].checked = $this.checked;
    // }

}
$('.select-all').click(function() {
    console.log(this);
    var $this = $(this),
        checked = !$this.data('checked');

    $('.' + $this.data('class')).not(':disabled').prop('checked', checked);
    $this.data('checked', checked)
});
$(document).on('change', '#class_search', function() {
    $('#form-search').submit();
});
$("#school_search").change(function() {
    var idchool = document.getElementById('school_search').value;
    var url = 'admin/school/search-class/' + idchool;
    $.ajax({
        url: url,
        type: 'get',
        dataType: 'json',
        success: function(data) {
            $("#class_search").html("<option value=''>--Lớp--</option>");

            $.each(data, function(key, value) {
                $("#class_search").append(
                    "<option value=" + value.id + ">" + value.name + "</option>"
                );
            });
        }
    });
});