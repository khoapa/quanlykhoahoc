<?php
return [
    'create_success' => 'Thêm khóa học thành công',
    'delete_success' => 'Xóa khóa học thành công.',
    'update_success' => 'Cập nhập khóa học thành công.',
    'create_fail' => 'Thêm khóa học thất bại',
    'delete_fail' => 'Xóa khóa học thất bại.',
    'update_fail' => 'Cập nhập khóa học thất bại.',
];