<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SchoolCourseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'course_id'  =>  'required',
          
        ];
    }
    public function messages()
    {
        return [
            'required' =>  __(':attribute không được để trống.'),
        ];
    }
    public function attributes()
    {
        return [
            'course_id' =>  __('khóa học'),
        ];
    }
}
