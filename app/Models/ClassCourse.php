<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClassCourse extends Model
{
    protected $table = 'class_course';
    protected $fillable = [
        'classes_id','course_id',
    ];
    public $timestamps = true;
}
