@extends('admin')
@section('content')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-car icon-gradient bg-mean-fruit">
                    </i>
                </div>
                <div>
                    DANH SÁCH HỌC SINH CỦA LỚP {{$class->name}}
                </div>
            </div>
            <div class="page-title-actions">
                <a href="{{route('class_create.student',['id'=>$class->id])}}" class="mb-2 mr-2 btn-icon btn btn-success" href=""><i class="mr-1 pe-7s-plus btn-icon-wrapper">
                    </i>Thêm mới học sinh</a>
            </div>
        </div>
    </div>
    <div class="main-card mb-3 card">
            <div id="example_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12">
                        <table class="table table-striped">
                            <thead>
                                <tr role="row">
                                    <th>STT</th>             
                                    <th>Ảnh</th>
                                    <th>Họ Tên</th>                                
                                    <th>Trường</th>
                                    <th>Lớp</th>
                                    <th>Trạng thái</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                 @foreach($students as $valueStudent)
                                @php
                                $date=date_create($valueStudent->birthday);
                                @endphp
                                <tr role="row" class="even">
                                <td>{!! Helper::indexPaginate($students,$loop) !!}</td>
                                    <td class="image-cate">
                                        <img class="rounded-circle" src="{{$valueStudent->avatar}}" alt="">
                                    </td>
                                    <td class="sorting_1 dtr-control">{{$valueStudent->name}}</td>
                                    <td>{{!is_null($valueStudent->school) ? $valueStudent->school->name : ''}}</td>
                                    <td>{{!is_null($valueStudent->classes) ? $valueStudent->classes->name : ''}}</td>
                                    <td>
                                        <label class="pd-toggle-switch">
                                            @if($valueStudent->status == 1)
                                            <input type="checkbox" checked onclick="addStatusUserFunction('2','{{$valueStudent->id}}')" value="{{$valueStudent->id}}">
                                            @else
                                            <input type="checkbox" onclick="addStatusUserFunction('1','{{$valueStudent->id}}')" value="{{$valueStudent->id}}">
                                            @endif
                                        </label>
                                    </td>
                                
                                    <td>
                                        <div class=" w-100">
                                            <a class="pull-right mb-2 mr-2 btn-icon btn-icon-only btn-shadow btn-dashed btn btn-outline-warning " href="('student.destroy',['id'=>$valueStudent->id])}}" title="Xóa" onclick="return confirm('Bạn muốn xóa ?')">
                                                <span class="pe-7s-trash" aria-hidden="true"></span>
                                            </a>
                                            <a href="{{route('class_edit.student',['id'=>$valueStudent->id])}}" class="pull-right mb-2 mr-2 btn-icon btn-icon-only btn-shadow btn-dashed btn btn-outline-primary" title="Sửa">
                                                <span class="pe-7s-tools" aria-hidden="true"></span>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="panel-footer">
                    <div class="col-12">
                        <div class="pagination pull-right">
                            {{ $students->links()}}
                        </div>
                    </div>
                </div>
            </div>
       
    </div>
</div>
@endsection