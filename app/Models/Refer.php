<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Refer extends Model
{
    protected $table = 'refer';
    protected $fillable = [
        'icon','title','link'
    ];
    public $timestamps = true;
}
