<!doctype html>
<html lang="en">

<head>
    <base href={{asset('')}}>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>ControlPanel</title>
    <meta name="viewport"
        content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
    <meta name="description" content="This is an example dashboard created using build-in elements and components.">
    <meta name="msapplication-tap-highlight" content="no">

    <link href="{{ asset('admins/main.css') }}" rel="stylesheet">
    <link href="{{ asset('admins/assets/css/secondary.css') }}" rel="stylesheet">
    <link href="{{ asset('admins/assets/css/map.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css">
    <script src="https://code.jquery.com/jquery-1.12.3.min.js"
        integrity="sha256-aaODHAgvwQW1bFOGXMeX+pC4PZIPsvn2h1sArYOhgXQ=" crossorigin="anonymous"></script>

    <!-- file new -->
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/3.5.0/select2.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/3.5.0/select2-bootstrap.min.css" />
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
</head>

<body>
    <div class="app-container fixed-header ">
        <div class="app-header header-shadow header-custom">
            <div class="">
                <img class="image-view" src="{{ asset('admins/assets/images/iconKNS.svg') }}">
                <span class="app-sidebar__heading">LIFE SKILL</span>
            </div>
        </div>
        <div class="contaner content-support">
            <div class="title text-center">{{ $support->title }}</div>
            <div class="detail-support">
                {!! $support->content !!}
            </div>
        </div>
    </div>

</body>

</html>