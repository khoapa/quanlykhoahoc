<div class="form-row">
    <div class="col mb-6">
        <div class="form-group">
            <label for="name">Tên khối học</label>
            <input type="text" value="{{ old('name', optional(optional($grade))->name) }}" class="form-control" maxlength="255" id="name" name="name" placeholder="Tên khối học" required>
            {!! $errors->first('name', '<p class="invalid-feedback">:message</p>') !!}
        </div>
    </div>
    <div class="col mb-6">
    </div>
</div>
<div class="form-group {{ $errors->has('thumbnail') ? 'was-validated-custom' : 'was-validated-custom-valid'}}">
    <label for="thumbnail">Ảnh khối học</label>
    <div class="col-md-3 block-single-upload">
        {!! Helper::uploadSingleImage( old('thumbnail', optional($grade)->thumbnail), 'thumbnail') !!}
    </div>
    {!! $errors->first('thumbnail', '<p class="invalid-feedback">:message</p>') !!}
</div>
<br>