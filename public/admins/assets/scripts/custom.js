$(document).on('click', '.block-image-upload', function() {
    var tag = event.target.nodeName.toLowerCase();
    if (tag == 'button' || tag == 'img' || tag == 'i') {
        return;
    } else {
        $('#image-upload').click();
    }
})


$(document).on('change', '#upload', function() {
    let formData = new FormData();
    formData.append('file', $('#image-upload')[0].files[0]);
    let url = $(this).attr('url');
    let images = $('#images').val();
    images = (images) ? JSON.parse(images) : [];
    $.ajax({
        url: url,
        type: 'POST',
        data: formData,
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        processData: false,
        contentType: false,
        success: function(data) {
            images = (images) ? images : [];
            images.push(data.path);
            let preview = '<div class="preview-image"><img src="' + data.path + '" />';

            preview += '<button type="button" class="delete-preview-image mb-2 mr-2 btn-icon btn-icon-only btn-shadow btn-dashed btn btn-outline-warning"><i class="pe-7s-trash btn-icon-wrapper"> </i></button></div>';

            $('.block-image-upload').append(preview);
            $('#images').val(JSON.stringify(images));
        }
    });
});
$(document).on('click', '.delete-preview-image', function() {
    let path = $(this).parent().find("img").attr('src');
    let images = $('#images').val();
    images = (images) ? JSON.parse(images) : [];
    if (images.includes(path)) {
        images = images.filter(function(ele) {
            return ele != path;
        });
        images = (images.length) ? JSON.stringify(images) : '';
        $('#images').val(images);
        $(this).parent().remove();
    }
});

$(document).on('click', '.btn-upload', function() {
    $('#image-upload').click();
});

$(document).on('click', '.btn-photo-image', function() {
    $('#upload-single-image').click();
});
$(document).on('change', '#upload-single-image', function() {
    readURL(this, 'img-sigle-preview');
});

function readURL(input, idPreview) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('#' + idPreview)
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

function showPreview(event) {
    if (event.target.files.length > 0) {
        var src = URL.createObjectURL((event.target.files[0]));
        var preview = document.getElementById('upload-img-single-show');
        preview.src = src;
        preview.style.display = "block";
    }
}


$(document).on('click', '#btn-upload_image', function() {
    $('#upload_image').click();
});

function readURLNew(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            imgElem = document.getElementById('upload_image');
            data = e.target.result.base64;
            console.log(data);
            $('#preview-image').attr('src', e.target.result);
            $(".image-box").css("display", "block");

        }
        reader.readAsDataURL(input.files[0]);
    }
}

$("#upload_image").change(function() {
    readURLNew(this);
});

function removeImage() {
    $(".image-box").css("display", "none");
    $('#upload_image').removeAttr('value');
}