@extends('admin')
@section('content')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-car icon-gradient bg-mean-fruit">
                    </i>
                </div>
                <div>Danh Sách Giáo Viên</div>
            </div>
            <div class="page-title-actions">
                <a href="{{ route('teacher.create') }}" class="mb-2 mr-2 btn-icon btn btn-success" href=""><i class="mr-1 pe-7s-plus btn-icon-wrapper">
                    </i>Thêm mới giáo viên</a>
            </div>
        </div>
    </div>
    @if(Session::has('message'))
    <div class="alert alert-success">
        <span class="glyphicon glyphicon-ok"></span>
        {!! session('message') !!}
        <button type="button" class="close" data-dismiss="alert" aria-label="close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    <form action="" method="GET" id="form-search">
        <div class="row">
            <div class="col-md-3">
                <input type="search" value="<?php echo request()->get('search') ?>" class="form-control search-input" name="search" placeholder="Tên giáo viên" aria-controls="example">
                <button class="btn btn-info search"><i class="fa fa-search"></i></button>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <select class="form-control" id="search_school" name="school_id">
                        <option value="">--Chọn trường--</option>
                        @foreach($schools as $school)
                        <option @if($school->id == request()->school_id)
                            selected = "selected"
                            @endif
                            value="{{$school->id}}">{{$school->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <select class="form-control" id="search_class" name="class_id">
                        <option value="">--Chọn lớp--</option>
                        @if(!is_null($classes))
                        @foreach($classes as $class)
                        <option @if($class->id == request()->class_id)
                            selected = "selected"
                            @endif
                            value="{{$class->id}}">{{$class->name}}</option>
                        @endforeach
                        @endif
                    </select>
                </div>
            </div>
        </div>
    </form>
    <div class="main-card card">
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">
                <table class="table table-striped ">
                    <thead>
                        <tr role="row">
                            <th>STT</th>
                            <th>Ảnh</th>
                            <th>Họ Tên</th>
                            <th>Trường</th>
                            <th>Lớp</th>
                            <th>Trạng Thái</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($teacher as $valueTeacher)
                        @php
                        $date=date_create($valueTeacher->birthday);
                        @endphp
                        <tr role="row" class="even">
                            <td>{!! Helper::indexPaginate($teacher,$loop) !!}</td>
                            <td class="image-cate">
                                <img class="rounded-circle" src="{{$valueTeacher->avatar}}" alt="">
                            </td>
                            <td class="sorting_1 dtr-control">{{$valueTeacher->name}}</td>
                            <td>{{!is_null($valueTeacher->school) ? $valueTeacher->school->name : ''}}</td>
                            <td>{{!is_null($valueTeacher->classes) ? $valueTeacher->classes->name : ''}}</td>
                            <td>
                                <label class="pd-toggle-switch">
                                    @if($valueTeacher->status == 1)
                                    <input type="checkbox" checked onclick="addStatusUserFunction('2','{{$valueTeacher->id}}')" value="{{$valueTeacher->id}}">
                                    @else
                                    <input type="checkbox" onclick="addStatusUserFunction('1','{{$valueTeacher->id}}')" value="{{$valueTeacher->id}}">
                                    @endif
                                </label>
                            </td>
                            <td>
                                <div class=" w-100">
                                    <a href="{{route('teacher.destroy',['id'=>$valueTeacher->id])}}" type="submit" class="pull-right mb-2 mr-2 btn-icon btn-icon-only btn-shadow btn-dashed btn btn-outline-warning" title="Xóa" onclick="return confirm('Bạn muốn xóa ?')">
                                        <span class="pe-7s-trash" aria-hidden="true"></span>
                                    </a>
                                    <a href="{{route('teacher.edit',['id'=>$valueTeacher->id])}}" class="pull-right mb-2 mr-2 btn-icon btn-icon-only btn-shadow btn-dashed btn btn-outline-primary" title="Sửa">
                                        <span class="pe-7s-tools" aria-hidden="true"></span>
                                    </a>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="panel-footer">
        <div class="col-12">
            <div class="pagination pull-right">
                {{ $teacher->links()}}
            </div>
        </div>
    </div>
</div>
@endsection