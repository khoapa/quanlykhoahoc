<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group(['prefix' => 'v1', 'namespace' => 'Api\v1'], function () {
    Route::post('/user/signin', 'UserController@signIn')->name('api.user.signin');
    Route::post('/user/signup', 'UserController@signUp')->name('api.user.signup');
    Route::post('/user/forgetpasss', 'UserController@forgetPass');
    Route::post('/verify', 'UserController@actionVerifyPhone');
    Route::post('/change-pass/verify', 'UserController@changePassVerifyPhone');

    Route::group(['prefix' => 'list'], function () {
        Route::post('/school', 'SchoolController@index');
        Route::post('/class', 'ClassController@index');
    });

    Route::group(['prefix' => 'support'], function () {
        Route::post('/', 'SupportController@index');
        Route::get('/{id}', 'SupportController@show');
    });
    Route::group(['prefix' => 'chat'], function () {
        Route::post('/create', 'ChatController@store');
       // Route::get('/{id}', 'SupportController@show');
    });

});
Route::group(['prefix' => 'v1', 'namespace' => 'Api\v1','middleware' =>'api.auth'], function () {
    Route::post('/images/upload', 'ImagesController@uploadImage');
    Route::group(['prefix' => 'user'], function () {
        Route::get('/me', 'UserController@me');
        Route::post('/changepass', 'UserController@changePass');
        Route::post('/update', 'UserController@update');
        Route::post('/device-token', 'UserController@addDeviceToken');
        Route::post('/log-out', 'UserController@logout');
    });
    Route::group(['prefix' => 'course'], function () {
        Route::get('/category', 'CategoryCourseController@index');
        Route::post('/list', 'CourseController@index');
        Route::post('/list-offer', 'CourseController@listOffer');
        Route::post('/list-new', 'CourseController@listNew');
        Route::get('/{id}', 'CourseController@show');
        Route::post('/list-all', 'CourseController@listAll');
        Route::get('/join/{id}', 'CourseController@join');
        Route::get('/favourite/{id}', 'CourseController@favourite');
        Route::post('/history', 'CourseController@history');
        Route::post('/list-favourite', 'CourseController@listFavourite');
        Route::post('/reply/{id}', 'CourseController@reply');
        Route::post('/result/{id}', 'CourseController@result');
    });
    Route::group(['prefix' => 'refer'], function () {
        Route::post('/list', 'ReferController@index');
        Route::get('/{id}', 'ReferController@show');
    });
    Route::group(['prefix' => 'news'], function () {
        Route::post('/list', 'NewsController@index');
        Route::post('/allpage', 'NewsController@allPage');
        Route::get('/{id}', 'NewsController@show');
    });
    Route::group(['prefix' => 'map'], function () {
        Route::get('/category', 'CategoryMapController@index');
        Route::post('/list', 'MapController@index');
        Route::get('/{id}', 'MapController@show');
        Route::post('/update', 'CategoryMapController@update');
        Route::post('/update-location','MapController@addTheLastPosition');
    });
    Route::group(['prefix' => 'home'], function () {
        Route::get('/', 'HomeController@getHome');
    });
    Route::group(['prefix' => 'conversion'], function () {
        Route::post('/list', 'ConversationController@index');
        Route::post('/list-message/{id}', 'ConversationController@list');
        Route::post('/create-new', 'ConversationController@createConversation');
        Route::post('/create', 'ConversationController@store');
        Route::put('/update/{id}', 'ConversationController@update');
        Route::delete('delete/{id}', 'ConversationController@delete');
        Route::delete('delete-conversion/{id}', 'ConversationController@deleteConversion');
    });
    Route::group(['prefix' => 'new'], function () {
        Route::get('/category', 'CategoryNewController@index');
    });

});
