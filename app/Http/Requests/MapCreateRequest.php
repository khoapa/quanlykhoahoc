<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MapCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=> 'required|max:255',
            'icon' => 'nullable|mimes:jpeg,png,jpg,gif,svg|max:5012',
            'map_category_id' => 'required',
            'description'=> 'required',
            'phone' => 'required',
            'location' => 'required'
        ];
    }
    public function messages(){
        $messages = [
            'map_category_id.required' => 'Thể loại là trường bắt buộc',
            'icon.mimes'=>'Icon phải đúng định dạng',
            'icon.max' => 'Icon không vượt quá 5Mb',
            'title.required' => 'Tiêu đề là trường bắt buộc',
            'description.required' => 'Mô tả là trường bắt buộc',
            'phone.required' => 'Số điện thoại là trường bắt buộc',   
            'location.required' => 'Vị trí là trường bắt buộc',
            'title.max' => 'Tiêu đề không vượt quá 255 kí tự',    
        ];
        return $messages;
    }
    public function getData()
    {
        $data = $this->only(['title','map_category_id', 'icon','description','phone','hotline','location','latitude','longitude']);
        return $data;
    }
}
