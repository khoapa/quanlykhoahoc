<?php

namespace App\Http\Resources\Notification;

use App\Helpers\Helper;
use App\Http\Resources\UserResource;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class NotificationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $this->load(['sender', 'receiver']);
        return [
          'id' => $this->id,
          'sender' => new UserResource($this->sender),
          'receiver' => new UserResource($this->receiver),
          'status' => $this->read_status,
          'title' => $this->title,
          'content' => $this->content,
          'created_at' => Helper::formatDate($this->created_at),
          'moment_string' => ' '.$this->momentDate($this->created_at),
          'click_action' => "NOTIFICATION_CLICK",
        ];
    }

    // Protected fucntion
    protected function momentDate($date){
      Carbon::setLocale('vi');
      $dt = new Carbon($date);
      return $dt->diffForHumans();
    }
}
