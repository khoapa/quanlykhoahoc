@extends('admin')
@section('content')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-car icon-gradient bg-mean-fruit">
                    </i>
                </div>
                <div>Chương Trình Học <br>
                    <h8 class="text">Danh sách khóa học đang hiển thị trên ứng dụng</h8>
                </div>
            </div>
            @if(!is_null($school_id) && is_null($classes))
            <div class="page-title-actions">
                <a href="{{route('school_course.create',['id'=>$school_id])}}" class="mb-2 mr-2 btn-icon btn btn-success" href=""><i class="mr-1 pe-7s-plus btn-icon-wrapper">
                    </i>Thêm Khóa Học Trường</a>
            </div>
            @endif
            @if(!is_null($class_id))
            <div class="page-title-actions">
                <a href="{{route('school_course.createClass',['id'=>$class_id])}}" class="mb-2 mr-2 btn-icon btn btn-success" href=""><i class="mr-1 pe-7s-plus btn-icon-wrapper">
                    </i>Thêm Khóa Học Lớp</a>
            </div>
            @endif
        </div>
    </div>
    @if(Session::has('message') )
    <div class="alert alert-success">
        <span class="glyphicon glyphicon-ok"></span>
        {!! session('message') !!}
        <button type="button" class="close" data-dismiss="alert" aria-label="close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    <div class="col-md-12 pl-0">
        <div class="mb-3 card">
            <div class="card-body">
                <div class="tab-content">
                    <form enctype="multipart/form-data" method="get" novalidate="" action="{{route('school_course.index') }}" accept-charset="UTF-8" id="create_exhibition_form" name="create_exhibition_form">
                        {{ csrf_field() }}
                        <div class="form-row">
                            <div class="col-mb-3">
                                <h6 class="timeline-title">Chọn Trường:</h6>
                            </div>
                            <div class="col-mb-3">
                                <div class="form-group">
                                    <select name="school_id" id="school_id1" class="form-control">
                                        <option value="">--Trường--</option>
                                        @foreach($listSchools as $school)
                                        @if($school_id == $school->id )
                                        <option value="{{$school->id}}" selected>{{$school->name}}</option>
                                        @else
                                        <option value="{{$school->id}}">{{$school->name}}</option>
                                        @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-mb-3">
                                <div class="form-group">
                                    <select name="class_id" id="class_id" class="form-control classes">
                                        <option value="">--Lớp--</option>
                                        @if(!is_null($listClasses))
                                        @foreach($listClasses as $value)
                                        @if($class_id == $value->id)
                                        <option selected value="{{$value->id}}">{{$value->name}}</option>
                                        @else
                                        <option value="{{$value->id}}">{{$value->name}}</option>
                                        @endif
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-mb-3">
                                <button class="btn btn-outline-success">OK</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div id="example_wrapper" class="dataTables_wrapper dt-bootstrap4">
            <div class="main-card card">
                <div class="panel-body panel-body-with-table">
                    <div class="table-responsive">

                        <table class="table table-striped ">

                            <thead>
                                <tr role="row">
                                    <th>Tên khóa học</th>
                                    <th>Danh mục</th>
                                    <th>Số học sinh đang tham gia</th>
                                    <!-- <th>Tất cả lớp</th> -->
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(!is_null($schools) && is_null($classes))
                                @foreach($schools as $valuesChools)
                                @foreach($valuesChools->course as $valueCourse)
                                <tr role="row" class="even">
                                    <td>{{$valueCourse->name}}</td>
                                    <td>{{$valueCourse->category->name}}</td>
                                    @php
                                    $sum = 0;
                                    foreach($userCourses as $valueUserCourses){
                                    if($valueUserCourses->course_id == $valueCourse->id){
                                    $sum++;
                                    }
                                    }
                                    @endphp
                                    <td>{{$sum}}</td>
                                    <!-- <td><input type="checkbox"></td> -->
                                    <td>
                                        <div class=" w-100">
                                            <a href="{{route('school_course.destroy',['id'=>$valueCourse->pivot->id])}}" type="submit" class="pull-right mb-2 mr-2 btn-icon btn-icon-only btn-shadow btn-dashed btn btn-outline-warning" title="Xóa" onclick="return confirm('Bạn muốn xóa ?')">
                                                <span class="pe-7s-trash" aria-hidden="true"></span>
                                            </a>
                                            <!-- <a href="{{route('school_course.edit',['id'=>$valueCourse->pivot->id])}}" class="pull-right mb-2 mr-2 btn-icon btn-icon-only btn-shadow btn-dashed btn btn-outline-primary" title="Sửa">
                                                <span class="pe-7s-tools" aria-hidden="true"></span>
                                            </a> -->
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                                @endforeach
                                @endif
                                <!--  -->
                                @if(!is_null($classes))
                                @foreach($classes as $valuesClasses)
                                @foreach($valuesClasses->course as $valueCourse)
                                <tr role="row" class="even">
                                    <td>{{$valueCourse->name}}</td>
                                    <td>{{$valueCourse->category->name}}</td>
                                    @php
                                    $sum = 0;
                                    foreach($userCourses as $valueUserCourses){
                                    if($valueUserCourses->course_id == $valueCourse->id){
                                    $sum++;
                                    }
                                    }
                                    @endphp
                                    <td>{{$sum}}</td>
                                    <td>
                                        <div class="w-100">
                                            <a class="pull-right mb-2 mr-2 btn-icon btn-icon-only btn-shadow btn-dashed btn btn-outline-warning " href="{{route('school_course.destroyClass',['id'=>$valueCourse->pivot->id])}}" title="ẩn" onclick="return confirm('Bạn muốn xóa ?')">
                                                <span class="pe-7s-trash" aria-hidden="true"></span>
                                            </a>
                                            <form method="GET" action="{{route('transcript.user')}}" accept-charset="UTF-8">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="class" value="{{$valueCourse->pivot->classes_id}}">
                                                <input type="hidden" name="course" value="{{$valueCourse->pivot->course_id}}">
                                                <div class="col mb-3">
                                                    <button class="pull-right badge badge-success label-detail-answer"><i class="pe-7s-check btn-icon-wrapper"> </i>Xem Chi Tiết</button>
                                                </div>
                                            </form>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-footer">
            <div class="col-12">
                <div class="pagination pull-right">
                </div>
            </div>
        </div>
    </div>
</div>
@endsection