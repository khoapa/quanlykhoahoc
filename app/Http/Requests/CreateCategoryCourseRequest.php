<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateCategoryCourseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  =>  'required|max:255',
            'thumbnail'  =>'required|mimes:jpeg,png,jpg,gif,svg|max:5120',
        ];
    }
    public function messages()
    {
        $messages = [ 
            'name.max' => 'Tên danh mục không vượt quá 255 kí tự', 
            'name.required' => 'Tên danh mục là trường bắt buộc', 
            'thumbnail.required' => 'Hình ảnh là trường bắt buộc', 
            'thumbnail.mimes' => 'Hình ảnh không đúng định dạng', 
            'thumbnail.max' => 'Hình ảnh không được quá 5 MB', 
        ];
        return $messages;
    }
    public function getData()
    {
        $data = $this->only(['name','thumbnail']);
        return $data;
    }
}
