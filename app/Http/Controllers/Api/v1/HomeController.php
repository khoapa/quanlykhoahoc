<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\ApiController;
use App\Http\Resources\Home\HomeResource;
use App\Models\Course;
use App\Models\CourseCategory;
use App\Models\News;
use Illuminate\Support\Str;

class HomeController extends ApiController
{
    public function getHome()
    {
        $queryCourseCategory = CourseCategory::select("id", 'name', 'thumbnail')->orderBy('id', 'DESC')->get();
        $queryNews = News::select("id", 'title', 'thumbnail', 'news_category_id', 'content', 'priority', 'link', 'status')
            ->where("status", 1)->orderBy('id', 'DESC')->take(5)->get();
        $queryOldNews = News::select("id", 'title', 'thumbnail', 'news_category_id', 'content', 'priority', 'link', 'status')
            ->where("status", 2)->orderBy('id', 'DESC')->take(6)->get();

        $queryNewCourse = Course::select('name', 'course_category_id', 'cover_image', 'start_date', 'longtime', 'status', 'new', 'offer', 'total_point', 'total_point_choice')
            ->where("status", 1)->where("new", 1)->with('category:id,name')->orderBy('id', 'DESC')->take(5)->get();
            
        $queryOfferCourse = Course::select('name', 'course_category_id', 'cover_image', 'start_date', 'longtime', 'status', 'new', 'offer', 'total_point', 'total_point_choice')
            ->where("status", 1)->where("offer", 1)->with('category:id,name')->orderBy('id', 'DESC')->take(5)->get();
        foreach($queryNews as $key => $news){
            $data['news'][$key] = $news->setAttribute('description',Str::limit(preg_replace( "/\r|\n/", "", strip_tags(html_entity_decode($news->content))),150));
        }
        foreach($queryOldNews as $key => $news){
            $data['old_news'][$key] = $news->setAttribute('description',Str::limit(preg_replace( "/\r|\n/", "", strip_tags(html_entity_decode($news->content))),150));
        }
        $user = auth()->user();
        $data['course_categories'] = $queryCourseCategory;
        $data['new_courses'] = $queryNewCourse;
        $data['offer_courses'] = $queryOfferCourse;
        $data['user']["id"] = $user->id;
        $data['user']["name"] = $user->name;
        return $data;
    }
}
