@extends('admin')
@section('content')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-car icon-gradient bg-mean-fruit">
                    </i>
                </div>
                <div>
                    CHI TIẾT PHÂN QUYỀN ({{$role->description}})
                </div>
            </div>
        </div>
    </div>
    @if(Session::has('message'))
    <div class="alert alert-success">
        <span class="glyphicon glyphicon-ok"></span>
        {!! session('message') !!}
        <button type="button" class="close" data-dismiss="alert" aria-label="close">
            <span aria-hidden="true">&times;</span>
        </button>

    </div>
    @endif
    <div class="main-card card">
        <form enctype="multipart/form-data" method="POST" novalidate=""
            action="{{route('permission.permission.update',$id)}}" accept-charset="UTF-8" id="create_exhibition_form"
            name="create_exhibition_form" class="form-horizontal {{ $errors->any() ? 'was-validated' : '' }}">
            {{csrf_field()}}
            <div class="form-row">
                <div class="col-lg-12 mt-3 ml-3">
                    <div class="form-group project-role">
                        <label>Quyền người dùng</label>
                        <div>
                            <input type="checkbox" value="0" id="allcheck" onClick="toggle(this)" />
                            <label class="ml-2">Tất cả</label>
                        </div>
                    </div>
                </div>
                <?php $i = 1; ?>
                @foreach($permissions as $key=>$permission)
                      <?php $i++; ?>
                <div class="col-md-3 ml-3 mt-2">
                    <p class="text-role">{{$key}}</p>
                    <input type="checkbox" class="allcheck" value="0" id="all-{{$i}}" onclick="checkAll(<?php echo $i ?>)" />
                    <label class="ml-2">Tất cả</label>
                    @foreach($permission as $p)
                    <div>
                        <input multiple @foreach($roles as $role) @if($role->pivot->permission_id == $p->id)
                        checked
                        @endif
                        @endforeach
                        class="all-{{$i}} allcheck"
                        id="{{$p->id}}"
                        type="checkbox" name="permision[]" value="{{$p->id}}"/>

                        <label class="ml-2 mt-auto description-permission">{{$p->description}}</label>
                        
                    </div>
                    @endforeach
                </div>
                @endforeach
            </div>
            <div class="form-group mt-2">
                <div class="col-md-offset-2 col-md-10">
                    <a href="{{ route('permission.permission.index') }}" class="mb-2 mr-2 btn-icon btn-pill btn btn-outline-secondary"><i
                            class="pe-7s-back btn-icon-wrapper"> </i>Trở lại</a>
                    <button class="mb-2 mr-2 btn-icon btn-pill btn btn-outline-success"><i
                            class="pe-7s-check btn-icon-wrapper"> </i>Cập nhập</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
<script language="JavaScript">
function toggle(source) {
    if ($('#allcheck').val() == 0) {
        $('.allcheck').prop('checked', true);
        $('#allcheck').prop('checked', true);
        $('#allcheck').val(1);
    } else {
        $('.allcheck').prop('checked', false);
        $('#allcheck').prop('checked', false);
        $('#allcheck').val(0);
    }
}
</script>