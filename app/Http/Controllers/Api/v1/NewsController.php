<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\ApiController;
use App\Http\Controllers\Controller;
use App\Http\Resources\CourseCollection;
use App\Http\Resources\News\AllPageNewsCollection;
use App\Http\Resources\News\NewsCollection;
use App\Http\Resources\News\NewsResource;
use App\Http\Resources\Refer\ReferCollection;
use App\Models\CategoryNews;
use App\Models\News;
use App\Models\Refer;
use App\Models\Tag;
use Illuminate\Http\Request;

class NewsController extends ApiController
{
    public function index(Request $request)
    {
        $perPage = $request->get('per_page');
        $newsCategoryId = $request->get('news_category_id');
        $searchText = $request->get('search_text');
        $query = News::orderBy('id', 'DESC');
        $query_all = News::orderBy('id', 'DESC');
        if (!is_null($newsCategoryId))
            $query->where('news_category_id', $newsCategoryId);
        else $query->where('status', 1);
        $data = [];
        if (!is_null($searchText)) {
            $query_tags = Tag::where('name', 'LIKE', '%' . $searchText . '%')->pluck('id')->toArray();
            foreach ($query_all->get() as $news) {
                foreach (!is_null($news->tag_ids) ? json_decode($news->tag_ids) : [] as $news_tag_id) {
                    $value = in_array($news_tag_id, $query_tags);
                    if ($value) {
                        $data[] = $news->id;
                        break;
                    }
                }
            }
            $query = $query->whereIn('id', $data)->orWhere('title', 'LIKE', '%' . $searchText . '%');
        }
        $perPage = $perPage ? $perPage : config('settings.countPaginate');
        $query = $query->latest()->paginate($perPage);
        return $this->formatJson(NewsCollection::class, $query);
    }

    public function allPage(Request $request)
    {
        $perPage = $request->get('per_page');
        $query = CategoryNews::orderBy('id', 'DESC');
        $perPage = $perPage ? $perPage : config('settings.countPaginate');
        $query = $query->latest()->paginate($perPage);
        return $this->formatJson(AllPageNewsCollection::class, $query);
    }
    public function show($id)
    {
        $news = News::findOrFail($id);
        return $this->formatJson(NewsResource::class, $news);
    }
}
