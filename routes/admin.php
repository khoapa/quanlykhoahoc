<?php
Route::get('admin/login','Auth\AuthController@getLogin')->name('login');
Route::post('admin/login','Auth\AuthController@postLogin')->name('admins.login');
Route::get('admin/logout','Auth\AuthController@logout')->name('logout');
Route::get('/support/{id}', 'Admins\SupportsController@view')->name('support.support.view'); 
// Admin route 
Route::group(['prefix' => 'admin','middleware' =>'auth'], function () {
    Route::get('/', 'Admins\DashboardController@index')
         ->name('admins.dashboard.index');
    Route::group([  
    'prefix' => 'category_course',
    'namespace' => 'Admins',
    ], function () {
        Route::get('/', 'CourseCategoryController@index')->name('category_course.index')->middleware('permission:view_category_course');
        Route::get('/create', 'CourseCategoryController@create')->name('category_course.create')->middleware('permission:create_category_course');
        Route::post('/store', 'CourseCategoryController@store')->name('category_course.store');
        Route::get('/edit/{id}', 'CourseCategoryController@edit')->name('category_course.edit')->middleware('permission:edit_category_course');
        Route::post('/update/{id}', 'CourseCategoryController@update')->name('category_course.update');
        Route::delete('/delete/{id}', 'CourseCategoryController@destroy')->name('category_course.destroy')->middleware('permission:delete_category_course');
    }); 
    Route::group([  
        'prefix' => 'course',
        'namespace' => 'Admins',
    ], function () {
        Route::get('/', 'CourseController@index')->name('course.index')->middleware('permission:view_course');
        Route::get('/create', 'CourseController@create')->name('course.create')->middleware('permission:create_course');
        Route::post('/store', 'CourseController@store')->name('course.store');
        Route::get('/edit/{id}', 'CourseController@edit')->name('course.edit')->middleware('permission:edit_course');
        Route::post('/update/{id}', 'CourseController@update')->name('course.update');
        Route::get('/delete/{id}', 'CourseController@destroy')->name('course.destroy')->middleware('permission:delete_course');
    });
    Route::group([  
        'prefix' => 'position',
        'namespace' => 'Admins',
    ], function () {
        Route::get('/', 'PositionController@index')->name('position.position.index');
    }); 
    Route::group([  
        'prefix' => 'question',
        'namespace' => 'Admins',
    ], function () {
        Route::get('/{id?}', 'QuestionController@index')->name('question.question.index');
        Route::get('/create/question/{id?}', 'QuestionController@create')->name('question.question.create');
        Route::post('/store', 'QuestionController@store')->name('question.question.store');
        Route::get('/edit/{id}', 'QuestionController@edit')->name('question.question.edit');
        Route::post('/update/{id}', 'QuestionController@update')->name('question.question.update');
        Route::delete('/delete/{id}', 'QuestionController@destroy')->name('question.question.destroy');
    }); 
    Route::group([  
        'prefix' => 'answer',
        'namespace' => 'Admins',
    ], function () {
        Route::get('/{id}', 'AnswerController@index')->name('answer.answer.index');
        Route::get('/create/{id}', 'AnswerController@create')->name('answer.answer.create');
        Route::post('/store/{id}', 'AnswerController@store')->name('answer.answer.store');
        Route::get('/edit/{id}', 'AnswerController@edit')->name('answer.answer.edit');
        Route::post('/update/{id}', 'AnswerController@update')->name('answer.answer.update');
        Route::delete('/delete/{id}', 'AnswerController@destroy')->name('answer.answer.destroy');
    });    
    Route::group([  
        'prefix' => 'point',
        'namespace' => 'Admins',
    ], function () {
        Route::get('/', 'PointController@index')->name('point.point.index');

    }); 
    Route::group([  
        'prefix' => 'permission',
        'namespace' => 'Admins',
    ], function () {
        Route::get('/', 'PermisionController@index')->name('permission.permission.index')->middleware('permission:view_permission');
        Route::get('/detail/{id}', 'PermisionController@show')->name('permission.permission.show')->middleware('permission:edit_permission');
        Route::post('/update/{id}', 'PermisionController@update')->name('permission.permission.update');
    });    
    Route::group([  
        'prefix' => 'chat',
        'namespace' => 'Admins',
    ], function () {
        Route::get('/{id?}', 'ChatController@index')->name('chat.chat.index');
        Route::post('/create', 'ChatController@store')->name('chat.chat.store');
        Route::post('/upload', 'ChatController@upload');
        Route::get('/conversion/delete/{id}', 'ChatController@deleteConversion')->name('chat.chat.delete_conversion');
        Route::get('/message/delete/{id}', 'ChatController@deleteMessage')->name('chat.chat.delete_message');
    });
    Route::group([  
        'prefix' => 'tracking',
        'namespace' => 'Admins',
    ], function () {
        Route::get('/', 'TrackingController@index')->name('tracking.tracking.index');
    });
    Route::get('/school/search-class/{id}', 'Admins\ClassController@classSchool');
});

