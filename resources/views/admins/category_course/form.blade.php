<div class="form-group {{ $errors->has('name') ? 'was-validated-custom' : 'was-validated-custom-valid' }}">
    <div class="form-row mb-2">
        <div class="col-md-6">
            <label for="firstname">Tên Danh Mục</label>
            <div>
                <input type="text" value="{{ old('name', optional(optional($categoryCourse))->name) }}"
                    class="form-control" id="name" name="name" required>
                    {!! $errors->first('name', '<p class="invalid-feedback">:message</p>') !!}
            </div>
        </div>
    </div>
</div>
<div class="form-group">
    <label for="avatar" class="control-label">Hình ảnh(*)</label>
    <div class="col-md-3 block-single-upload">
        {!! Helper::uploadSingleImage( old('thumbnail', optional($categoryCourse)->thumbnail), 'thumbnail') !!}
        {!! $errors->first('thumbnail', '<p class="invalid-feedback">:message</p>') !!}
    </div>
</div>