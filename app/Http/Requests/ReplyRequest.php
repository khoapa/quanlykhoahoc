<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReplyRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            '*.id_question' => 'required',
            '*.answer' => 'required',
            '*.type' => 'required',
        ];
    }
    public function messages()
    {
        $messages = [ 
            '*.id_question.required' => 'Câu hỏi là trường bắt buộc', 
            '*.answer.required' => 'Câu trả lời là trường bắt buộc', 
            '*.type.required' => 'Loại câu hỏi là trường bắt buộc',

        ];
        return $messages;
    }
}
