<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClassUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=> 'required',
            'school_id' => 'required',
            'grade_id' => 'required',
            'thumbnail' => 'nullable|mimes:jpeg,png,jpg,gif,svg|max:5012'
        ];
    }
    public function messages(){
        $messages = [
            'thumbnail.mimes'=>'Ảnh không đúng định dạng',
            'thumbnail.max' => 'Ảnh không vượt quá 5Mb',
            'name.required' => 'Tên lớp là trường bắt buộc',
            'school_id.required' => 'Trường học là trường bắt buộc',
            'grade_id.required' => 'Khối là trường bắt buộc'
        ];
        return $messages;
    }
    public function getData()
    {
        $data = $this->only(['name','thumbnail','school_id','grade_id','user_id']);
        return $data;
    }
}
