<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Models\Map::class, function (Faker $faker) {
    return [
        'title'=>$faker->name,
        'map_category_id'=>rand(1,10),
        'phone'=>rand(1000000000,9999999999),
        'hotline'=>rand(1000000000,9999999999),
        'location'=>$faker->address,
        'latitude'=>rand(10,5000)/100,
        'longitude'=>rand(10,5000)/100,
    ];
});
