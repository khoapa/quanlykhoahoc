<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Enums\StatusCourseEnum;

class CourseAllResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    public function toArray($request)
    {
        $limit = request()->get('limit');
        return [
            'id' => $this->id,
            'name' => $this->name,
            'coursers' => CourseResource::collection($this->course->where('status',StatusCourseEnum::SHOW)->take($limit)),
        ];
    }
}
