<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StudentSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $limit = 20;
        for ($i = 0; $i < $limit; $i++) {
            DB::table('users')->insert([
                [
                    'name' => $faker->name,
                    'email' => $faker->unique()->email,
                    'avatar' => 'https://picsum.photos/200/300',
                    'phone' => rand(1000000000, 9999999999),
                    'password' => Hash::make(12345678),
                    'birthday' => $faker->dateTimeBetween('1990-01-01', '2012-12-31')->format('Y/m/d'),
                    'address' => $faker->address,
                    'gender' => rand(1, 2),
                    'school_id' => rand(1, 4),
                    'class_id' => rand(1, 4),
                    'position_id' => rand(1, 4),
                    'user_type' => rand(1, 2),
                ],
            ]);
        }
    }
}
