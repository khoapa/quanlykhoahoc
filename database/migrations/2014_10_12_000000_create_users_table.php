<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name',255);
            $table->string('email',255)->unique();
            $table->string('password');
            $table->string('avatar',255);
            $table->timestamp('email_verified_at')->nullable();
            $table->string('phone',20);
            $table->dateTime('birthday');
            $table->string('address',255);
            $table->tinyInteger('gender')->nullable()->comment = '1:nam, 2:nữ';
            $table->integer('school_id')->nullable();
            $table->integer('class_id')->nullable();
            $table->integer('position_id')->nullable();
            $table->integer('user_type')->nullable()->comment = '1.học sinh, 2: Giao Viên, 3. Nhân viên quản lý';
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
