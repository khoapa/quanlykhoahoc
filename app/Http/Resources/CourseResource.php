<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Helpers\Helper;
use App\Enums\StatusUserCourseEnum;

class CourseResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $idUser = auth()->user()->id;
        $statusUserCourse = $this->userCourse()->where('user_id',$idUser)->first();
        $statusFavourite = $this->userFavourite()->where('user_id',$idUser)->first();
        return [
            'id' => $this->id,
            'name' => $this->name,
            'category' => $this->category->name,
            'cover_image' => $this->cover_image,
            'start_date' => Helper::formatDate($this->start_date),
            'longtime' => $this->longtime,
            'status_user' => is_null($statusUserCourse) ? null : $statusUserCourse->pivot->status,
            'status_user_lable' => is_null($statusUserCourse) ? 'Tham gia' : StatusUserCourseEnum::getDescription($statusUserCourse->pivot->status),
            'status_favourite' => is_null($statusFavourite) ? 0 : 1,
        ];
    }
}
