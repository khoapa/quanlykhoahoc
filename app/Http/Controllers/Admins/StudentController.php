<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Http\Requests\StudentRequest;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\StudentUpdateRequest;
use App\Models\School;
use App\Models\Classes;
use App\Services\UploadService;
use App\Helpers\Helper;
use App\Enums\UserTypeEnum;
use App\Models\Role;

class StudentController extends Controller
{
    public function index(Request $request)
    {  
        $name = $request->get('search');
        $schoolID = $request->get('school_id');
        $classID = $request->get('class_id');
        $query=User::where('user_type',UserTypeEnum::STUDENT)->orderBy('id', 'DESC');
        $classes = null;
        if (!is_null($schoolID)) {
            $query = $query->where('school_id',$schoolID);
            $classes = Classes::where('school_id',$schoolID)->get();   
        }
        if (!is_null($classID)) {
            $query = $query->where('class_id',$classID);
        }
        if (!is_null($name)) {
            $query = $query->where('name', 'LIKE', '%' . $name . '%');
        }
        $student = $query->paginate(config('settings.countPaginate'));
        $schools = School::get();
        return view('admins.student.index', compact('student','schools','classes'));
    }
    public function create()
    {
        $schools = School::get();
        $classes = Classes::get();
        $roles = Role::where('name','!=','Admin')->get();
        return view('admins.student.create', compact('schools', 'classes','roles'));
    }
    public function store(StudentRequest $request)
    {
        $data = $request->getData();
        $data['birthday'] = Helper::formatSqlDate($data['birthday']);
        $data['password'] = Hash::make(config('settings.password'));
        $data['user_type'] = UserTypeEnum::STUDENT;
        $data['email'] = str_replace(' ', '', $data['email']);
        $data_image = config('settings.image');
        if ($request->hasFile('avatar')) {
            $file = $request->file('avatar');
            $data_image = UploadService::uploadImage('student', $file);
        }
        $data['avatar'] = $data_image; 
        try {
            $userCreate = User::create($data);
            $roleId = $data['role_id'];
            if(!is_null($roleId)){
                $userCreate->syncRoles([$roleId]);
            }
            return redirect()->route('student.index')->with('message',  'Thêm mới học sinh thành công');
        } catch (\Exception $e) {
            return redirect()->back()->with('message', 'Thêm mới học sinh không thành công');
        }
    }
    public function edit($id)
    {
        $student = User::findOrFail($id);
        $schools = School::get();
        $classes = Classes::get();
        $roles = Role::where('name','!=','Admin')->get();
        return view('admins.student.update', compact('student', 'schools', 'classes','roles'));
    }
    public function update(StudentUpdateRequest $request, $id)
    {

        $student = User::findOrFail($id);
        $data = $request->getData();
        $data['birthday'] = Helper::formatSqlDate($data['birthday']);
        $data['user_type'] = UserTypeEnum::STUDENT;
        $data['email'] = str_replace(' ', '', $data['email']);
        if ($request->has('avatar')) {
            $file = $request->file('avatar');
            $data_image = UploadService::uploadImage('student', $file);
            $data['avatar'] = $data_image;
        } else {
            $data['avatar'] = $student->avatar;
        }
        $roleId = $data['role_id'];
        $roleOld = $student->role()->first();
        if(!is_null($roleId)){
            $student->syncRoles([$roleId]);
        }else{
            if($roleOld){
                $student->removeRole($roleOld->role_id);   
            }
        }
        try {
            $student->update($data);
            return redirect()->route('student.index')->with('message', 'Đã cập nhật học sinh thành công.');
        } catch (\Exception $e) {
            return redirect()->back()->with('message', 'Đã có lỗi xảy ra trong quá trình cập nhật. Vui lòng thử lại.');
        }
    }
    public function searchClassAjax(Request $request)
    {
        if ($request->ajax()) {
            $classes = Classes::where('school_id', '=', $request->id_School)->get();

            return response()->json($classes);
        }
    }
    public function updateStatusAjax(Request $request)
    {
        if ($request->ajax()) {
            $student = User::findOrFail($request->id);
            $student->status = $request->status;
            if ($student->save()) {
                $message = 'chuyển trạng thái thành công';
            } else {
                $message = 'chuyển trạng thái thất bại';
            }
            return response()->json($message);
        }
    }
    public function destroy($id)
    {
        $student = User::findOrFail($id);
        try {
            $student->delete();
            return redirect()->route('student.index')->with('message', 'Đã xóa học sinh thành công');
        } catch (\Exception $e) {
            return redirect()->back()->with('message', 'Đã có lỗi xảy ra trong quá trình xóa. Vui lòng thử lại.');
        }
    }
}
