<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public  $mtoken;
    public function __construct($token)
    {
        $this->mtoken = $token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $pass = $this->mtoken;
        return $this->view('emails.forgot_password',compact('pass'))->subject('Mail Đổi Mật Khẩu');

    }
}
