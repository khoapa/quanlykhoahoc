<div class="form-row">
    <div class="col-md-6 mb-6">
        <div class="form-group">
            <label for="name">Tên lớp</label>
                <input type="text" value="{{ old('name', optional(optional($class))->name) }}" maxlength="255" class="form-control" id="name" name="name" placeholder="Tên lớp"  required>
            {!! $errors->first('name', '<p class="invalid-feedback">:message</p>') !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('school_id') ? 'was-validated ' : '' }}">
            <label for="school">Chọn trường:</label>
            <select required class="custom-select {{ $errors->has('school_id') ? 'is-invalid' : '' }}" id="school" name="school_id">
                <option value="">--Chọn trường--</option>
                @if(!isset($school))
                    @foreach($schools as $valueSchool)
                    <option @if($valueSchool->id == old('school_id', optional(optional($class))->school_id))
                        selected = "selected"
                        @endif
                        value="{{$valueSchool->id}}">{{$valueSchool->name}}</option>
                    @endforeach
                @else
                    @foreach($schools as $valueSchool)
                    <option @if($valueSchool->id == old('school_id', optional(optional($school))->id))
                        selected = "selected"
                        @endif
                        value="{{$valueSchool->id}}">{{$valueSchool->name}}</option>
                    @endforeach
                @endif
            </select>
            {!! $errors->first('school_id', '<p class="invalid-feedback">:message</p>') !!}
        </div>
    </div>
</div>
<div class="form-row">
    <div class="col-md-6 mb-6">
        <div class="form-group {{ $errors->has('grade_id') ? 'was-validated ' : '' }}">
            <label for="grade">Chọn khối:</label>
            <select required class="custom-select {{ $errors->has('grade_id') ? 'is-invalid' : '' }}" id="grade_id" name="grade_id">
                <option value="">--Chọn khối--</option>
                @if(!is_null($edit))
                <?php
                for ($i = 0; $i < count($gradeOfSchool); $i++) {
                ?>
                    <option <?php if ($gradeOfSchool[$i]['id'] == optional(optional($class))->grade_id) { ?> selected="selected"<?php } ?> value="<?php echo $gradeOfSchool[$i]['id'] ?>">{{$gradeOfSchool[$i]['name']}}</option>
                <?php
                }
                ?>
                @endif
            </select>
            {!! $errors->first('grade_id', '<p class="invalid-feedback">:message</p>') !!}
        </div>
    </div>
    <div class="col-md-6 mb-6">
        <div class="form-group {{ $errors->has('user_id') ? 'was-validated ' : '' }}">
            <label for="grade">Giáo viên chủ nhiệm:</label>
            <select required class="custom-select {{ $errors->has('user_id') ? 'is-invalid' : '' }}" id="user_id" name="user_id">
                <option value="">--Chọn giáo viên--</option>
                @if(!is_null($class))
                @foreach($teachers as $teacher)         
                <option @if($teacher->id == old('user_id', optional(optional($class))->user_id))
                    selected = "selected"
                    @endif
                    value="{{$teacher->id}}">{{$teacher->name}}</option>
                @endforeach
                @endif
            </select>
            {!! $errors->first('user_id', '<p class="invalid-feedback">:message</p>') !!}
        </div>
    </div>
</div>
<div class="form-group {{ $errors->has('thumbnail') ? 'was-validated-custom' : 'was-validated-custom-valid'}}">
    <label>Hình Ảnh</label>
    <div class="col-md-3 block-single-upload">
        {!! Helper::uploadSingleImage( old('thumbnail', optional($class)->thumbnail), 'thumbnail') !!}
    </div>
    {!! $errors->first('thumbnail', '<p class="invalid-feedback">:message</p>') !!}
</div>