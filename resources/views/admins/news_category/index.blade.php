@extends('admin')
@section('content')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-car icon-gradient bg-mean-fruit">
                    </i>
                </div>
                <div>
                    DANH SÁCH THỂ LOẠI TIN TỨC
                </div>
            </div>
            <div class="page-title-actions">
                <a href="{{route('news_category_create')}}" class="mb-2 mr-2 btn-icon btn btn-success" href=""><i class="mr-1 pe-7s-plus btn-icon-wrapper">
                    </i>Thêm mới thể loại</a>
            </div>
        </div>
    </div>
    @if(Session::has('message'))
    <div class="alert alert-success">
        <span class="glyphicon glyphicon-ok"></span>
        {!! session('message') !!}
        <button type="button" class="close" data-dismiss="alert" aria-label="close">
            <span aria-hidden="true">&times;</span>
        </button>

    </div>
    @endif
    <form action="" method="GET" id="form-search">
        <div class="col mb-6">
            <div class="row">
                <div class="form-group">
                    <input type="search" value="<?php echo request()->get('search') ?>" class="form-control" name="search" placeholder="Tên thể loại tin tức" aria-controls="example">
                </div>
                <div class="form-group ml-2">
                    <button class="btn btn-info"><i class="fa fa-search"></i></button>
                </div>
            </div>
        </div>
    </form>
    <div class="card mb-3">
    <div class="main-card card">
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">
                <table class="table table-striped ">
                    <thead>
                        <tr role="row">
                            <th>STT</th>
                            <th>Thể loại tin tức</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($categories as $category)
                        <tr role="row" class="odd">
                        <td>{!! Helper::indexPaginate($categories,$loop) !!}</td>
                            <td>{{$category->name}}</td>
                            <td>
                                <div class=" w-100">
                                    <a class="pull-right mb-2 mr-2 btn-icon btn-icon-only btn-shadow btn-dashed btn btn-outline-warning " href="{{route('news_category_delete',['id'=>$category->id])}}" title="Xóa" onclick="return confirm('Bạn muốn xóa ?')">
                                        <span class="pe-7s-trash" aria-hidden="true"></span>
                                    </a>
                                    <a href="{{route('news_category_edit',['id'=>$category->id])}}" class="pull-right mb-2 mr-2 btn-icon btn-icon-only btn-shadow btn-dashed btn btn-outline-primary" title="Sửa">
                                        <span class="pe-7s-tools" aria-hidden="true"></span>
                                    </a>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                </table>
                </div>
        </div>
        <div class="panel-footer">
            <div class="col-12">
                <div class="pagination pull-right">
                    {{ $categories->links()}}
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
    @endsection