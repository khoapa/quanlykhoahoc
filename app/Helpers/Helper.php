<?php

namespace App\Helpers;

use Carbon\Carbon;
use Request;

class Helper
{
    public static function formatDate($date)
    {
        $dt = is_null($date) ? Carbon::now() : new Carbon($date);
        return  $dt->format('d/m/Y');
    }
    public static function formatDate1($date)
    {
        $dt = is_null($date) ? Carbon::now() : new Carbon($date);
        return  $dt->format('d-m-Y');
    }
    public static function setSelected($val, $origin)
    {
        $active = $val == $origin ? 'selected' : '';
        return $active;
    }
    public static function formatSqlDate($date)
    {
        if (is_null($date)) {
            return null;
        }
        $dt = new Carbon($date);
        return $dt->format('Y-m-d');
    }
    public static function indexPaginate($object, $loop)
    {
        $index = ($object->currentPage() - 1) * $object->perPage() + $loop->iteration;
        return $index;
    }
    public static function uploadSingleImage($oldImage, $name)
    {
        $image = $oldImage ? asset($oldImage) : asset('admins/assets/images/photo.png');
        return <<<HTML
        <div class="preview-upload-image">
            <img id="img-sigle-preview" src="$image"/>
        </div>
        <button  type="button" class="btn-photo-image mb-2 mr-2 btn-icon btn-icon-only btn btn-light"><i class="pe-7s-camera btn-icon-wrapper"> </i></button>
        <input type="file" class="form-control upload-single-image" id="upload-single-image" name="$name" style="width: 0" required/>
HTML;
    }
    public static function setChecked($val, $origin)
    {
        $active = $val == $origin ? 'checked' : '';
        return $active;
    }

    public static function postApi($body)
    {
        $client = new \GuzzleHttp\Client();
        $response = $client->request("POST", 'http://svc3.netplus.vn/WSSendSMS.asmx', [
            'headers' => [
                'Content-Type' => 'text/xml; charset=UTF8',
            ],
            'body' => '<?xml version="1.0" encoding="utf-8"?>
            <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
              <soap:Body>
                ' . $body . '
              </soap:Body>
            </soap:Envelope>'
        ]);
        return $response;
    }
    public static function formatDateChat($date){
        $dt = is_null($date) ? Carbon::now() : new Carbon($date);
        return  $dt->format('d-m-Y H:i');
    }
    public static function formatDateDiffForHumans($date){
        $now = Carbon::now();
        $dt = is_null($date) ? Carbon::now() : new Carbon($date);
        return  $dt->diffForHumans($now);
    }
}
