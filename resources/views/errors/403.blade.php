<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Unauthorized!</title>
  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:100,500,600&display=swap&subset=vietnamese" rel="stylesheet">
  <style>
    html,
    body {
      background-color: #fff;
      color: #636b6f;
      font-family: 'Montserrat', sans-serif;
      font-weight: 100;
      height: 100vh;
      margin: 0;
    }

    .full-height {
      height: 100vh;
      flex-direction: column;
    }

    .flex-center {
      align-items: center;
      display: flex;
      justify-content: center;
    }

    .content {
      text-align: center;
    }

    .title {
      font-size: 45px;
      text-align: center;
      text-transform: uppercase;
    }

    .m-b-md {
      margin-bottom: 30px;
    }

    .btn-outline {
      border: 1px solid #e55c5c;
      color: #e55c5c;
      padding: 4px 20px;
      border-radius: 15px;
      font-size: 15px;
      font-weight: 500;
      text-decoration: none;
      display: block;
      width: 80px;
      margin-top: 20px;
      transition: all 0.3s;
      text-transform: none;
    }
    .btn-outline:hover{
      border-color: #ffd966;
      transition: all 0.3s;
    }
    .btn-outline svg{
      vertical-align: middle;
    }
    .btn-outline span{
      vertical-align: middle;
    }
  </style>
</head>

<body>
  <div class="title flex-center full-height">
    Không có quyền thực hiện!
    <div>
      <a href="{{ route('home.index') }}" class="btn btn-outline"><svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
          width="23" height="23" viewBox="0 0 172 172" style=" fill:#000000;">
          <g transform="translate(4.73,4.73) scale(0.945,0.945)">
            <g fill="none" fill-rule="nonzero" stroke="none" stroke-width="none" stroke-linecap="butt"
              stroke-linejoin="none" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none"
              font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal">
              <g fill="#e55c5c" stroke="#e55c5c" stroke-width="10" stroke-linejoin="round">
                <path
                  d="M75.27688,48.72438c1.15562,2.64719 0.5375,5.71094 -1.53188,7.71312l-22.4675,22.6825h86.3225c2.48594,-0.04031 4.78375,1.26313 6.03344,3.41313c1.26313,2.13656 1.26313,4.79719 0,6.93375c-1.24969,2.15 -3.5475,3.45344 -6.03344,3.41312h-86.3225l22.4675,22.6825c2.67406,2.72781 2.62031,7.10844 -0.1075,9.7825c-2.72781,2.67406 -7.10844,2.62031 -9.7825,-0.1075l-33.97,-34.4l-4.73,-4.8375l4.73,-4.8375l33.97,-34.4c1.23625,-1.30344 2.92938,-2.08281 4.73,-2.15c2.87563,-0.16125 5.53625,1.47813 6.69188,4.11188z">
                </path>
              </g>
              <path d="M0,172v-172h172v172z" fill="none" stroke="none" stroke-width="1" stroke-linejoin="miter"></path>
              <g fill="#e55c5c" stroke="none" stroke-width="1" stroke-linejoin="miter">
                <path
                  d="M68.585,44.6125c-1.80062,0.06719 -3.49375,0.84656 -4.73,2.15l-33.97,34.4l-4.73,4.8375l4.73,4.8375l33.97,34.4c2.67406,2.72781 7.05469,2.78156 9.7825,0.1075c2.72781,-2.67406 2.78156,-7.05469 0.1075,-9.7825l-22.4675,-22.6825h86.3225c2.48594,0.04031 4.78375,-1.26312 6.03344,-3.41312c1.26313,-2.13656 1.26313,-4.79719 0,-6.93375c-1.24969,-2.15 -3.5475,-3.45344 -6.03344,-3.41313h-86.3225l22.4675,-22.6825c2.06938,-2.00219 2.6875,-5.06594 1.53188,-7.71312c-1.15563,-2.63375 -3.81625,-4.27313 -6.69188,-4.11188z">
                </path>
              </g>
              <path d="" fill="none" stroke="none" stroke-width="1" stroke-linejoin="miter"></path>
              <path d="" fill="none" stroke="none" stroke-width="1" stroke-linejoin="miter"></path>
            </g>
          </g>
        </svg>
        <span>Trở về</span>
      </a>
    </div>
  </div>
</body>

</html>
