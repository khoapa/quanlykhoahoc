<?php 
return [
    'create_success' => 'Thêm danh mục khóa học thành công',
    'delete_success' => 'Xóa danh mục khóa học thành công.',
    'update_success' => 'Cập nhập danh mục khóa học thành công.',
    'create_fail' => 'Thêm danh mục khóa học thất bại',
    'delete_fail' => 'Xóa danh mục khóa học thất bại.',
    'update_fail' => 'Cập nhập danh mục khóa học thất bại.',
];