<?php

namespace App\Http\Resources\News;

use App\Http\Resources\CollectionResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class AllPageNewsCollection extends CollectionResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public $collects = AllPageNewsResource::class;
    public function toArray($request)
    {
        return $this->collection;
    }
}
