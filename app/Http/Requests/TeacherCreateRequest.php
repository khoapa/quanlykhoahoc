<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TeacherCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  =>  'required',
            'email' => 'required|email|unique:users', 
            'birthday'  => 'required|before:today',
            'avatar'  =>  'mimes:jpeg,png,jpg,gif,svg|max:5012',
            'phone'  =>  'required',
            'address'  =>  'required',
            'gender'  =>  'required',
            'school_id'  =>  'required',
          
        ];
    }
    public function messages()
    {
        return [
            'required'            =>  __(':attribute không được để trống.'),
            'unique'              =>  __(':attribute ' . '"' . $this->email . '"' . ' đã tồn tại trong hệ thống, vui lòng nhập :attribute khác.'),
            'min'            =>  __(':attribute tối thiểu  ký tự.'),
            'mimes'            =>  __('đây không phải file :attribute'),
            'max'            =>  __(':attribute quá dung lượng quy định.'),
            'not_in'            =>  __(':attribute Bạn chưa chọn :attribute.'),
            'email'            =>  __('Đây không phải :attribute.'),
            'before'            =>  __('Đây không phải :attribute.'),
        ];
    }
    public function attributes()
    {
        return [
            'name'     =>  __('Họ Tên'),
            'email'     =>  __('email'),
            'birthday'     =>  __('Ngày Sinh'),
            'avatar'     =>  __('Hình'),
            'phone'     =>  __('Số Điện Thoại'),
            'address'     =>  __('Địa Chỉ'),
            'gender'     =>  __('Giới tính'),
            'school_id'     =>  __('Trường'),        ];
    }
    public function getData()
    {
        $data = $this->only(['role_id','name','email','avatar','birthday','phone','address','gender','class_id','school_id']);
        return $data;
    }
}
