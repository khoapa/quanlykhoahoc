<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Position;
class PositionController extends Controller
{
    public function index(){
        $positions = Position::orderBy('id','DESC')->paginate(config('settings.countPaginate'));
        return view('admins.position.index',compact('positions'));
    }
}
