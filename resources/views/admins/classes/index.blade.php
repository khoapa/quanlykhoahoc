@extends('admin')
@section('content')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-car icon-gradient bg-mean-fruit">
                    </i>
                </div>
                <div>
                    DANH SÁCH LỚP HỌC
                </div>
            </div>
            <div class="page-title-actions">
                <a href="{{route('class_create')}}" class="mb-2 mr-2 btn-icon btn btn-success" href=""><i class="mr-1 pe-7s-plus btn-icon-wrapper">
                    </i>Thêm mới lớp học</a>
            </div>
        </div>
    </div>
    @if(Session::has('message'))
    <div class="alert alert-success">
        <span class="glyphicon glyphicon-ok"></span>
        {!! session('message') !!}
        <button type="button" class="close" data-dismiss="alert" aria-label="close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    <form action="" method="GET" id="form-search">
        <div class="row">
            <div class="col-md-3">
                <input type="search" value="<?php echo request()->get('search') ?>" class="form-control search-input" name="search" placeholder="Tên lớp học" aria-controls="example">
                <button class="btn btn-info search"><i class="fa fa-search"></i></button>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <select class="form-control" id="search_school" name="school_id">
                        <option value="">--Chọn trường--</option>
                        @foreach($schools as $school)
                        <option @if($school->id == request()->school_id)
                            selected = "selected"
                            @endif
                            value="{{$school->id}}">{{$school->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <select class="form-control" id="grade_search" name="grade_id">
                        <option value="">--Chọn khối--</option>
                        <?php
                        for ($i = 0; $i < count($grades); $i++) {
                        ?>
                            <option <?php if ($grades[$i]['id'] == request()->grade_id) { ?> selected="selected" <?php } ?> value="<?php echo $grades[$i]['id'] ?>">{{$grades[$i]['name']}}</option>
                        <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
        </div>
    </form>
    <div class="card mb-3">
        <div class="main-card card">
            <div class="panel-body panel-body-with-table">
                <div class="table-responsive">
                    <table class="table table-striped ">
                        <thead>
                            <tr>
                                <th>STT</th>
                                <th>Hình ảnh</th>
                                <th>Tên lớp</th>
                                <th>Khối</th>
                                <th>Trường</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($classes as $row)
                            <tr role="row" class="odd">
                                <td>{!! Helper::indexPaginate($classes,$loop) !!}</td>
                                <td class="image-cate">
                                    <img class="rounded-circle" src="{{$row->thumbnail}}" alt="">
                                </td>
                                <td>{{$row->name}}</td>
                                <td>{{optional(optional(optional($row))->grade)->name}}</td>
                                <td>{{optional(optional(optional($row))->school)->name}}</td>
                                <td>
                                    <div class=" w-100">
                                        <a href="{{route('list_student',[$row->id,optional($row->school)->id])}}" class="mb-2 mr-2 btn-icon btn-pill btn btn-outline-success">DS Học Sinh</a>
                                        <a type="submit" class="pull-right mb-2 mr-2 btn-icon btn-icon-only btn-shadow btn-dashed btn btn-outline-warning " href="{{route('class_delete',['id'=>$row->id])}}" title="Xóa" onclick="return confirm('Bạn muốn xóa ?')">
                                            <span class="pe-7s-trash" aria-hidden="true"></span>
                                        </a>
                                        <a href="{{route('class_edit',['id'=>$row->id])}}" class="pull-right mb-2 mr-2 btn-icon btn-icon-only btn-shadow btn-dashed btn btn-outline-primary" title="Sửa">
                                            <span class="pe-7s-tools" aria-hidden="true"></span>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="panel-footer">
                <div class="col-12">
                    <div class="pagination pull-right">
                        {{ $classes->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection