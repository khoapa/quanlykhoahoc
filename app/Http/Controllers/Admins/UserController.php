<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use App\Models\School;
use App\Models\Classes;
use App\Services\UploadService;
use App\Helpers\Helper;
use App\Enums\UserTypeEnum;
use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Models\Grade;
use App\Models\Role;
use PhpParser\Builder\Class_;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $name = $request->get('search');
        $user_type = $request->get('user_type');
        $schoolID = $request->get('school_id');
        $classID = $request->get('class_id');
        $query=User::whereIn('user_type', [UserTypeEnum::TEACHER, UserTypeEnum::STUDENT])->orderBy('id', 'DESC');
        $classes = null;
        if (!is_null($schoolID)) {
            $query = $query->where('school_id',$schoolID);
            $classes = Classes::where('school_id',$schoolID)->get();   
        }
        if (!is_null($classID)) {
            $query = $query->where('class_id',$classID);
        }
        if (!is_null($user_type)) {
            $query = $query->where('user_type', $user_type);
        } 
        if (!is_null($name)) {
            $query = $query->where('name', 'LIKE', '%' . $name . '%');
        }
        $users = $query->paginate(config('settings.countPaginate'));
        $schools = School::get();
        return view('admins.users.index', compact('users','schools','classes'));
    }
    public function create()
    {
        $schools = School::get();
        $classes = Classes::get();
        $roles = Role::where('name', '!=', 'Admin')->get();
        return view('admins.users.create', compact('schools', 'classes', 'roles'));
    }
    public function store(UserCreateRequest $request)
    {
        $data = $request->getData();
        $data['birthday'] = Helper::formatSqlDate($data['birthday']);
        $data['password'] = Hash::make(config('settings.password'));
        $data['email'] = str_replace(' ', '', $data['email']);
        $data_image = config('settings.image');
        if ($request->hasFile('avatar')) {
            $file = $request->file('avatar');
            $data_image = UploadService::uploadImage('user', $file);
        }
        $data['avatar'] = $data_image;
        try {
            $userCreate = User::create($data);
            $roleId = $data['role_id'];
            if (!is_null($roleId)) {
                $userCreate->syncRoles([$roleId]);
            }
            return redirect()->route('user.index')->with('message',  'Thêm mới user thành công');
        } catch (\Exception $e) {
            return redirect()->back()->with('message', 'Thêm mới user không thành công');
        }
    }
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $schools = School::get();
        $classes = Classes::get();
        $roles = Role::where('name', '!=', 'Admin')->get();
        return view('admins.users.update', compact('user', 'schools', 'classes', 'roles'));
    }
    public function update(UserUpdateRequest $request, $id)
    {

        $user = User::findOrFail($id);
        $data = $request->getData();
        $data['birthday'] = Helper::formatSqlDate($data['birthday']);
        $data['email'] = str_replace(' ', '', $data['email']);
        if ($request->has('avatar')) {
            $file = $request->file('avatar');
            $data_image = UploadService::uploadImage('user', $file);
            $data['avatar'] = $data_image;
        } else {
            $data['avatar'] = $user->avatar;
        }
        $roleId = $data['role_id'];
        $roleOld = $user->role()->first();
        if (!is_null($roleId)) {
            $user->syncRoles([$roleId]);
        } else {
            if ($roleOld) {
                $user->removeRole($roleOld->role_id);
            }
        }
        try {
            $user->update($data);
            return redirect()->route('user.index')->with('message', 'Đã cập nhật user thành công.');
        } catch (\Exception $e) {
            return redirect()->back()->with('message', 'Đã có lỗi xảy ra trong quá trình cập nhật. Vui lòng thử lại.');
        }
    }
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        try {
            $user->delete();
            return redirect()->route('users.index')->with('message', 'Đã xóa user thành công');
        } catch (\Exception $e) {
            return redirect()->back()->with('message', 'Đã có lỗi xảy ra trong quá trình xóa. Vui lòng thử lại.');
        }
    }
}
