<?php

namespace App\Http\Controllers\Admins;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Refer;
use App\Services\UploadService;
use App\Http\Requests\ReferRequest;
use App\Http\Requests\ReferUpdateRequest;
class ReferController extends Controller
{
    public function index(Request $request)
    {
        $title = $request->get('search');
        $query = Refer::orderBy('id','DESC');
        if(!is_null($title)){
            $query = $query->where('title','LIKE','%'.$title.'%');
        }
        $refers = $query->paginate(config('settings.countPaginate'));
        return view('admins.refer.index', compact('refers'));
    }
    public function create()
    {
        return view('admins.refer.create');
    }
    public function store(ReferRequest $request)
    {
        $data = $request->getData();
        if ($request->hasFile('icon')) {
            $file = $request->file('icon');
            $data_icon = UploadService::uploadImage('icon', $file);
            $data['icon'] = $data_icon;
        }
        $insertData = Refer::create($data);
        if ($insertData) {
            return redirect()->route('refer.index')->with('message', trans('Thêm mới tham khảo thành công'));
        } else {
            return redirect()->back()->with('message', trans('Đã có lỗi xảy ra trong quá trình thêm mới. Vui lòng thử lại.'));
        }
    }
    public function edit($id){
        $refer = Refer::findOrFail($id);
        return view('admins.refer.update',compact('refer'));
    }
    public function update(ReferUpdateRequest $request, $id)
    {   
        $refer = Refer::findOrFail($id);
        $data = $request->getData();
        $data_icon = $refer->icon;
        if ($request->hasFile('icon')) {
            $file = $request->file('icon');
            $data_icon = UploadService::uploadImage('icon', $file);
        }
        $data['icon'] = $data_icon;
        $update = $refer->update($data);
	    if ($update) {
            return redirect()->route('refer.index')->with('message',trans('Cập Nhật tham khảo thành công'));
	    }else {                        
            return redirect()->back()->with('message',trans('Đã có lỗi xảy ra trong quá trình thêm mới. Vui lòng thử lại.'));
	    }
    }
    public function destroy($id){
        $refer = Refer::findOrFail($id);
        $delRefer = $refer->delete();
        if ($delRefer) {
            return redirect()->route('refer.index')->with('message',trans('Đã xóa trang tham khảo thành công'));
	    }else {                        
            return redirect()->back()->with('message',trans('Đã có lỗi xảy ra trong quá trình thêm mới. Vui lòng thử lại.'));
	    }
    }
}
