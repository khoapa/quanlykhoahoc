# Coding convention
- [ ] Tên lớp đặt theo PascalCase, dùng danh từ tiếng Anh, ví dụ: UserClass, CategoryClass…

- [ ] Tên hàm và phương thức sử dụng camelCase, sử dụng động từ tiếng Anh ví dụ getUser, getCategory…


- [ ] Tên biến sử dụng camelCase $loginUser, $categoryList…, tên ghép tiếng Anh có nghĩa, liên quan đến thực thể lưu trữ, dùng số nhiều nếu biến là dạng mảng.


- [ ] Tên hằng số  viết hoa hết và cách nhau bởi dấu gạch dưới DISCOUNT_PERCENT, LIMIT_RATE…


- [ ] Tên bảng, tên cột trong Database sử dụng underscore và sử dụng danh từ số nhiều, ví dụ bảng users, user_profiles ...

- [ ] Comment: Dùng tiếng Anh, câu từ rõ nghĩa, comment ở đoạn code xử lý logic phức tạp, comment ở phương thức nếu nhiều param phức tạp.


- [ ] Code được format theo chuẩn: tab == 4 space, clean code

# Commit source 

- [ ] Tạo tên branch theo theo chuẩn: 
    1. Feature: feature/tên_chức_năng: vd: feature/user_login 
    2. Bug: fix/tên_bug: fix/error_login hoặc fix/bug_BUGID(BUGID là số thứ tự bug do tester bắt), fix/feedback_FEEDBACK_ID(FEEDBACK_ID là số thứ tự feedback )
    3. Bug ưu tiên: hotfix/bug_BUGIID(BUGID là số thự tự bug cần ưu tiên hotfix) 

- [ ] Đặt tên commit: Commit nên đặt tên theo chức năng liên quan đến commit: VD: Tạo data cho user...

- [ ] Mỗi commit nhỏ hơn 10 file.(không commit nhiều file 1 lần )

- [ ] Commit chỉ những file liên quan đến chức năng đang dùng, không commit những file không cần thiết lên.

# Tạo PullRequest. 

- [ ] Đặt tiêu đề PR rõ ràng, liên quan đến task đang thực hiện: VD: Tạo chức năng đăng nhập.

- [ ] Mô tả xử lý logic (nếu có những xử lý phức tạp cần review lại xử lý)

- [ ] Chụp ảnh kết quả đính kèm

- [ ] Tự review lại các file thay đổi trước khi assign cho người review


