<?php

namespace App\Http\Resources\CategoryMap;

use App\Models\UserCategoryMap;
use Illuminate\Http\Resources\Json\JsonResource;

class CategoryMapResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {   
        $userId = auth()->user()->id;
        $map = UserCategoryMap::where("category_map_id",$this->id)->where("user_id",$userId)->first();
        return [
            'id' => $this->id,
            'name' => $this->name,
            'icon' => $this->icon,
            "status" => !is_null($map) ? $map->status : 1
        ];
    }
}
