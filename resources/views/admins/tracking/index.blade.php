@extends('admin')
@section('content')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-car icon-gradient bg-mean-fruit">
                    </i>
                </div>
                <div>
                    Tracking học sinh
                </div>
            </div>
        </div>
    </div>
    @if(Session::has('message'))
    <div class="alert alert-success">
        <span class="glyphicon glyphicon-ok"></span>
        {!! session('message') !!}
        <button type="button" class="close" data-dismiss="alert" aria-label="close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    <form action="" method="GET" id="form-search">
        <div class="row">
        <div class="col-md-3">
                <input type="search" value="<?php echo request()->get('name') ?>" class="form-control search-input" name="name" placeholder="Tên học sinh" aria-controls="example">
                <button class="btn btn-info custom-search"><i class="fa fa-search"></i></button>
            </div>

            <div class="col-md-3">
                <div class="form-group">
                    <select class="form-control" id="school_search" name="school_id">
                        <option value="">--Chọn trường--</option>
                        @foreach($schools as $school)
                        <option @if($school->id == request()->school_id)
                            selected = "selected"
                            @endif
                            value="{{$school->id}}">{{$school->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <select class="form-control" id="class_search" name="class_id">
                        <option value="">--Chọn lớp--</option>
                        @foreach($classes as $class)
                        <option @if($class->id == request()->class_id)
                            selected = "selected"
                            @endif
                            value="{{$class->id}}">{{$class->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </form>
    <div class="main-card card">
    <div id="mapstudent" style="height: 400px;"></div>
     <input type="hidden" name="student" value="{{$arrs}}" id="studentsMap" />
    </div>
</div>
@endsection
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAZlHcoPe33iRtvrqFr1o_ZU50pV6JRVJs&callback=initMap&libraries=places&callback=initMap" defer></script>
<script type="text/javascript" src="{{ asset('admins/assets/scripts/tracking.js') }}"></script>
