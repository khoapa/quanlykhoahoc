importScripts('https://www.gstatic.com/firebasejs/7.10.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.10.0/firebase-messaging.js');

// Initialize the Firebase app in the service worker by passing in the
// messagingSenderId.
var config = {
  apiKey: "AIzaSyAyflWq2Px_GwHzZ1yFzbYHxdiRrV66-mo",
  authDomain: "skill-a8bfd.firebaseapp.com",
  databaseURL: "https://skill-a8bfd.firebaseio.com",
  projectId: "skill-a8bfd",
  storageBucket: "skill-a8bfd.appspot.com",
  messagingSenderId: "215861017338",
  appId: "1:215861017338:web:f8ab6de504667712132743",
  measurementId: "G-2KPE6B849V"
};

firebase.initializeApp(config);
// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function (payload) {
  console.log('[firebase-messaging-sw.js] Received background message ', payload);
  // Customize notification here
  const notificationTitle = 'Background Message Title';
  const notificationOptions = {
    body: 'Background Message body.',
    icon: '/itwonders-web-logo.png'
  };

  return self.registration.showNotification(notificationTitle,
    notificationOptions);
});
