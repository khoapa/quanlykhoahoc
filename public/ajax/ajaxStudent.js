$("#school_id1").change(function() {
    var id_School = $(this).val();
    var url = 'admin/student/search-class';
    $.ajax({
        url: url,
        type: 'get',
        dataType: 'json',
        data: {
            'id_School': id_School
        },
        success: function(data) {
            $("#class_id").html("<option value=''>--Lớp--</option>");

            $.each(data, function(key, value) {
                $("#class_id").append(
                    "<option value=" + value.id + ">" + value.name + "</option>"
                );
            });
        }
    });
});

function addStatusUserFunction(status, id) {
    var url = 'admin/student/update-status';
    $.ajax({
        url: url,
        type: 'get',
        dataType: 'json',
        data: {
            'status': status,
            'id': id
        },
        success: function(data) {
            alert(data);
        }
    });
}


$('#datepicker').datepicker({
    uiLibrary: 'bootstrap4',
    format: "dd-mm-yyyy",
});

$("#school").change(function() {
    var id = $(this).val();
    var url = 'admin/teacher/list-teacher/' + id;
    $.ajax({
        url: url,
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        type: 'get',
        dataType: 'json',
        success: function(data) {
            $("#user_id").html('');
            $.each(data, function(key, value) {
                $("#user_id").append(
                    "<option value=" + value.id + ">" + value.name + "</option>"
                );
            });
        }
    });
});
$("#school").change(function() {
    var id = $(this).val();
    var url = 'admin/grades/list-grade/' + id;
    $.ajax({
        url: url,
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        type: 'get',
        dataType: 'json',
        success: function(data) {
            $("#grade_id").html('');
            $.each(data, function(key, value) {
                $("#grade_id").append(
                    "<option value=" + value.id + ">" + value.name + "</option>"
                );
            });
        }
    });
});

function addStatusFunction(status, id) {
    var url = '/student/update-status';
    $.ajax({
        url: url,
        type: 'get',
        dataType: 'json',
        data: {
            'status': status,
            'id': id
        },
        success: function(data) {
            alert(data);
        }
    });
}

$(document).on('change', '#search_school', function() {
    $('#form-search').submit();
});
$(document).on('change', '#grade_search', function() {
    $('#form-search').submit();
});
$(document).on('change', '#search_class', function() {
    $('#form-search').submit();
});
$(document).on('change', '#user_type_search', function() {
    $('#form-search').submit();
});
$("#search_school").change(function() {
    document.getElementById('grade_search').value = '';
});
$("#search_school").change(function() {
    document.getElementById('search_class').value = '';
});