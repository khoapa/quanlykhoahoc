<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Models\CourseCategory;
use App\Http\Resources\CategoryCourseResource;

class CategoryCourseController extends ApiController
{
    public function index(){
        $categories = CourseCategory::all();
        return $this->formatCollectionJson(CategoryCourseResource::class,$categories);
    }
}
