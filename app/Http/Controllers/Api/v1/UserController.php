<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\ApiLoginRequest;
use App\Http\Resources\UserResource;
use JWTAuth;
use App\Http\Controllers\ApiController;
use App\Http\Resources\UserInforResource;
use App\Http\Requests\RegisterApiRequest;
use App\Helpers\Helper;
use App\User;
use App\Models\School;
use App\Http\Resources\ListSchoolResource;
use App\Enums\UserTypeEnum;
use App\Http\Requests\ActionVerifyRequest;
use App\Http\Resources\ListTeacherResource;
use App\Models\Classes;
use App\Http\Requests\ApiForgetPassRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\ChangePassRequest;
use App\Http\Requests\ChangePassVerifyRequest;
use App\Http\Requests\UpdateUserApiRequest;
use App\Http\Requests\User\AddDeviceTokenRequest;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use MarkWilson\XmlToJson\XmlToJsonConverter;
use SimpleXMLElement;

class UserController extends ApiController
{
    public function signIn(ApiLoginRequest $request)
    {

        $credentials = $request->only('email', 'password');
        $token = null;
        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['message' => 'Email hoặc mật khẩu không hợp lệ'], 422);
            }
        } catch (JWTAuthException $e) {
            return response()->json(['message' => 'Không thể tạo token'], 500);
        }
        $user = auth()->user();
        $user = $user->setAttribute('token', $token);
        return $this->formatJson(UserResource::class, $user);
    }
    public function me()
    {
        $user = auth('api')->user();
        return $this->formatJson(UserInforResource::class, $user);
    }
    public function signUp(RegisterApiRequest $request)
    {
        $data = $request->getData();
        if (isset($data['birthday'])) {
            $data['birthday'] = Helper::formatSqlDate($data['birthday']);
        }
        $data['password'] = bcrypt($data['password']);
        $createUser = User::create($data);
        $data['avatar'] = config('settings.image');
        if ($createUser) {
            return $this->sendMessage('Tạo tài khoản thành công.', 200);
        } else {
            return $this->sendError404('Có lỗi khi tạo tài khoản!');
        }
    }

    public function forgetPass(ApiForgetPassRequest $request)
    {
        $data_verify = $request->data_verify;
        try {
            DB::beginTransaction();
            if (filter_var($data_verify, FILTER_VALIDATE_EMAIL)) {
                $user = User::where('email', $data_verify)->first();
                if (is_null($user)) {
                    return $this->sendError404('Email không có trong hệ thống');
                } else {
                    $email_authentication_code = rand(1000, 9999);
                    Mail::send('emails.forgot_password', compact('user', 'email_authentication_code'), function ($message) use ($user) {
                        $message->to($user->email)->subject('Mã xác thực!');
                    });
                    $user->update(["email_authentication_code" => $email_authentication_code]);
                    DB::commit();
                    return $this->sendMessage("Mã xác thực đã được gửi qua email của bạn.");
                }
            } else {
                $user = User::where('phone', $data_verify)->first();
                if (is_null($user)) {
                    return $this->sendError404('Số điện thoại không có trong hệ thống.');
                } else {
                    $phone_authentication_code = rand(1000, 9999);
                    return $phone_authentication_code;
                    //send sms right now
                    $school_code = config('config_web_service.MaTruong');
                    $user_service = config('config_web_service.User');
                    $pass_service = config('config_web_service.pass');
                    $company_code = config('config_web_service.CompanyCode');
                    $sms_type = config('config_web_service.SMSType');

                    $response_login = Helper::postApi(
                        '<Login xmlns="http://tempuri.org/">
                    <maTruong>' . $school_code . '</maTruong>
                    <userName>' . $user_service . '</userName>
                    <password>' . $pass_service . '</password>
                  </Login>'
                    );
                    // return $response_login->getBody()->getContents();
                    $xml_string = $response_login->getBody()->getContents();
                    $soap = simplexml_load_string($xml_string);
                    $response = $soap->children('http://schemas.xmlsoap.org/soap/envelope/')
                        ->Body->children()
                        ->LoginResponse;

                    $customer_sent_id = (string) $response->LoginResult;
                    $phone = str_replace(0, 84, $data_verify);
                    $response_send = Helper::postApi(
                        '<SendSMS xmlns="http://tempuri.org/">
                        <aSMS_Input>
                            <SmsType>' . $sms_type . '</SmsType>
                            <IdCustomerSent>' . $customer_sent_id . '</IdCustomerSent>
                            <CompanyCode>' . $company_code . '</CompanyCode>
                            <Mobile>' . $phone . '</Mobile>
                            <SMSContent>' . $phone_authentication_code . '</SMSContent>
                        </aSMS_Input>
                        <userName>' . $user_service . '</userName>
                        <password>' . $pass_service . '</password>
                    </SendSMS>
    '
                    );
                    $user->update(["phone_authentication_code" => $phone_authentication_code]);
                    DB::commit();
                    return $this->sendMessage("Mã xác thực đã được gửi đến số điện thoại.");
                }
            }
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    public function actionVerifyPhone(ActionVerifyRequest $request)
    {
        $data_verify = $request->data_verify;
        $verify_code = $request->verify_code;
        if (filter_var($data_verify, FILTER_VALIDATE_EMAIL)) {
            $user = User::where('email', $data_verify)->first();
            if (is_null($user)) {
                return $this->sendError404('Email không có trong hệ thống');
            } else {
                if ($user->email_authentication_code === $verify_code) {
                    return $this->sendMessage("Xác thực thành công.");
                } else return $this->sendError404('Mã xác thực không đúng.');
            }
        } else {
            $user = User::where('phone', $data_verify)->first();
            if (is_null($user)) {
                return $this->sendError404('Số điện thoại không có trong hệ thống.');
            } else {
                if ($user->phone_authentication_code === $verify_code) {
                    return $this->sendMessage("Xác thực thành công.");
                } else return $this->sendError404('Mã xác thực không đúng.');
            }
        }
    }
    public function changePassVerifyPhone(ChangePassVerifyRequest $request)
    {
        $data_verify = $request->data_verify;
        $verify_code = $request->verify_code;
        $password = bcrypt($request->password);
        if (filter_var($data_verify, FILTER_VALIDATE_EMAIL)) {
            $user = User::where('email', $data_verify)->first();
            if (is_null($user)) {
                return $this->sendError404('Email không có trong hệ thống');
            } else {
                if ($user->email_authentication_code === $verify_code) {
                    $user->update(["email_authentication_code" => null, 'password' => $password]);
                    return $this->sendMessage("Thay đổi mật khẩu thành công.");
                } else return $this->sendError404('Mã xác thực không đúng.');
            }
        } else {
            $user = User::where('phone', $data_verify)->first();
            if (is_null($user)) {
                return $this->sendError404('Số điện thoại không có trong hệ thống.');
            } else {
                if ($user->phone_authentication_code === $verify_code) {
                    $user->update(["phone_authentication_code" => null, 'password' => $password]);
                    return $this->sendMessage("Thay đổi mật khẩu thành công.");
                } else return $this->sendError404('Mã xác thực không đúng.');
            }
        }
    }

    public function addDeviceToken(AddDeviceTokenRequest $request)
    {
        $user = auth()->user();
        $data = $request->getData();
        $device_token = $user->device_token;
        if (is_null($device_token)) {
            $data_tokens[] = (string) $data['device_token'];
            $device_token = json_encode($data_tokens);
            $user->device_token = $device_token;
            $user->save();
        } else {
            $device_token = json_decode($device_token);
            if (!in_array((string) $data['device_token'], $device_token)) {
                array_push($device_token, (string) $data['device_token']);
                $user->device_token = $device_token;
                $user->save();
            }
        }
        return $this->sendMessage("Thêm Device Token thành công!");
    }

    public function logout(AddDeviceTokenRequest $request)
    {
        $user = auth()->user();
        $data = $request->getData();
        $device_token = $user->device_token;
        if (!empty($device_token)) {
            $arr_token = json_decode($device_token);
            $index = array_search((string) $data['device_token'], $arr_token);
            unset($arr_token[$index]);
            $device_token = json_encode(array_values($arr_token));
            $user->update(['device_token' => $device_token]);
        }
        JWTAuth::invalidate(JWTAuth::getToken());
        return $this->sendMessage('Đăng xuất thành công!');
    }

    public function changePass(ChangePassRequest $request)
    {
        $password = bcrypt($request->get('password'));
        $user = auth('api')->user()->update([
            'password' => $password
        ]);
        return $this->sendMessage("Thay đổi mật khẩu thành công.");
    }

    public function update(UpdateUserApiRequest $request)
    {
        $user = auth()->user();
        $data = $request->getData();
        $data['birthday'] = Helper::formatSqlDate($data['birthday']);
        $update = $user->update($data);
        return $this->sendMessage('Cập nhập thông tin thành công.');
    }
}
