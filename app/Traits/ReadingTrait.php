<?php

namespace App\Traits;
use App\Models\User;
use App\Models\UserRead;
use LRedis;
trait ReadingTrait
{
    
    public function resetReadNumber($userId, $projectId, $itemType)
    {
        $item = UserRead::firstOrNew(array('user_id' =>$userId, 'item_type' => $itemType, 'project_id' => $projectId));
        $item->num_read = 0;
        $item->save();
        $this->sendNotification(true);
    }

    public function increaseNumber($projectId, $itemType)
    {
        $users = User::all();
        foreach($users as $user){
            $item = UserRead::firstOrNew(array('user_id' =>$user->id, 'item_type' => $itemType, 'project_id' => $projectId));
            $item->num_read ++;
            $item->save();
        }
        $this->sendNotification(true);
    }

    protected function sendNotification($hasNew = true)
    {
        $redis = LRedis::connection();
        $redis->publish('reading-notify', json_encode(['has_new' => $hasNew]));
    }

    public function sendTestMessage()
    {
        $redis = LRedis::connection();
        $redis->publish('user-chat-3',  json_encode(['has_new' => true]));
    }

}