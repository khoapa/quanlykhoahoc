<div id="example_wrapper" class="dataTables_wrapper dt-bootstrap4">
            <div class="main-card card">
                <div class="panel-body panel-body-with-table">
                    <div class="table-responsive">

                        <table class="table table-striped ">

                            <thead>
                                <tr role="row">
                                    <th>Tên khóa học</th>
                                    <th>Danh muc</th>
                                    <th>Chọn khóa học</th>
                                    <!-- <th>Tất cả lớp</th> -->
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                               @foreach($courses as $valueCourses)
                               <tr role="row">
                                    <td>{{$valueCourses->name}}</td>
                                    <td>{{$valueCourses->category->name}}</td>
                                    <td> 
                                    <label class="pd-toggle-switch">
                                     <input type="checkbox" name="course_id[]" value="{{$valueCourses->id}}">
                                    </label>
                                    </td>
                               </tr>
                               @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>