<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ServiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'link'  =>  'required',
            'pass'  =>  'required',
            'key'   =>  'required',
            'user' =>  'required'
        ];
    }
    public function messages()
    {
        return [
            'required'   =>  __(':attribute không được để trống.'),
        ];
    }
    public function attributes()
    {
        return [
            'link'     =>  __('Link'),
            'pass'  =>  __('Mật khẩu'),
            'key'   =>  __('Key'),
            'user' =>  __('User'),
        ];
    }
    public function getData()
    {
        $data = $this->only(['link','pass','key','user']);
        return $data;
    }
}
