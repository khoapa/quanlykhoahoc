$("#school_id").change(function() {
    var id_School = $(this).val();
    var url = 'admin/transcript/search-class';
    $.ajax({
        url: url,
        type: 'get',
        dataType: 'json',
        data: {
            'id_School': id_School
        },
        success: function(data) {
            $("#class_id").html('');
            $.each(data, function(key, value) {
                $("#class_id").append(
                    "<option value=" + value.id + ">" + value.name + "</option>"
                );
            });
        }
    });
});
$("#category_id").change(function() {
    var id_category = $(this).val();
    var url = 'admin/transcript/search-course';
    $.ajax({
        url: url,
        type: 'get',
        dataType: 'json',
        data: {
            'id_category': id_category
        },
        success: function(data) {
            $("#course_id").html('');
            $.each(data, function(key, value) {
                $("#course_id").append(
                    "<option value=" + value.id + ">" + value.name + "</option>"
                );
            });
        }
    });
});